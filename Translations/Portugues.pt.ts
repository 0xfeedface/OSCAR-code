<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR" sourcelanguage="en_US">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Diálogo</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="35"/>
        <source>&amp;About</source>
        <translation>&amp;Sobre</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="49"/>
        <location filename="../oscar/aboutdialog.cpp" line="125"/>
        <source>Release Notes</source>
        <translation>Notas da Versão</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="63"/>
        <source>Credits</source>
        <translation>Créditos</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="77"/>
        <source>GPL License</source>
        <translation>Licença GPL</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="239"/>
        <source>Close</source>
        <translation>Fechar</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="43"/>
        <source>Show data folder</source>
        <translation>Exibir pasta de dados</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="47"/>
        <source>About OSCAR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="91"/>
        <source>Sorry, could not locate About file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="104"/>
        <source>Sorry, could not locate Credits file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="116"/>
        <source>Sorry, could not locate Release Notes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="126"/>
        <source>OSCAR v%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="129"/>
        <source>Important:</source>
        <translation>Importante:</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="130"/>
        <source>As this is a pre-release version, it is recommended that you &lt;b&gt;back up your data folder manually&lt;/b&gt; before proceding, because attempting to roll back later may break things.</source>
        <translation>Como se trata de uma versão de pré-lançamento, é recomendável que você &lt;b&gt;faça um backup manual&lt;/b&gt; da sua pasta de dados antes de proceder, porque a tentativa de retroceder mais tarde pode causar problemas.</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="142"/>
        <source>To see if the license text is available in your language, see %1.</source>
        <translation>Para ver se o texto da licença está disponível no seu idioma, consulte%1.</translation>
    </message>
</context>
<context>
    <name>CMS50F37Loader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.cpp" line="873"/>
        <source>Could not find the oximeter file:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.cpp" line="879"/>
        <source>Could not open the oximeter file:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CMS50Loader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="478"/>
        <source>Could not get data transmission from oximeter.</source>
        <translation>Impossível obter dados de transmissão do oxímetro.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="478"/>
        <source>Please ensure you select &apos;upload&apos; from the oximeter devices menu.</source>
        <translation>Por favor certifique-se de selecionar &apos;enviar&apos; do menu de dispositivos do oxímetro.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="546"/>
        <source>Could not find the oximeter file:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="552"/>
        <source>Could not open the oximeter file:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Daily</name>
    <message>
        <location filename="../oscar/daily.ui" line="435"/>
        <source>Form</source>
        <translation>De</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="506"/>
        <source>Go to the previous day</source>
        <translation>Ir para o dia anterior</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="551"/>
        <source>Show or hide the calender</source>
        <translation>Exibir ou ocultar o calendário</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="614"/>
        <source>Go to the next day</source>
        <translation>Ir para o próximo dia</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="674"/>
        <source>Go to the most recent day with data records</source>
        <translation>Ir para o dia mais recente com dados registrados</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="861"/>
        <source>Events</source>
        <translation>Eventos</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="923"/>
        <source>View Size</source>
        <translation>Ver tamanho</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="968"/>
        <location filename="../oscar/daily.ui" line="1401"/>
        <source>Notes</source>
        <translation>Notas</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1022"/>
        <source>Journal</source>
        <translation>Diário</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1116"/>
        <location filename="../oscar/daily.ui" line="1126"/>
        <source>Small</source>
        <translation>Pequeno</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1131"/>
        <source>Medium</source>
        <translation>Médio</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1136"/>
        <source>Big</source>
        <translation>Grande</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1208"/>
        <source>I&apos;m feeling ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1502"/>
        <source>Flags</source>
        <translation>Marcadores</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1554"/>
        <source>Graphs</source>
        <translation>Gráficos</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1579"/>
        <source>Show/hide available graphs.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1094"/>
        <source>Color</source>
        <translation>Cor</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1050"/>
        <source> i </source>
        <translation> i </translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1081"/>
        <source>u</source>
        <translation>u</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1066"/>
        <source>B</source>
        <translation>B</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1195"/>
        <source>Zombie</source>
        <translation>Zumbi</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1224"/>
        <source>Weight</source>
        <translation>Peso</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1298"/>
        <source>Awesome</source>
        <translation>Ótimo</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1336"/>
        <source>B.M.I.</source>
        <translation>I.M.C.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1352"/>
        <source>Bookmarks</source>
        <translation>Favoritos</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1373"/>
        <source>Add Bookmark</source>
        <translation>Adicionar favorito</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1396"/>
        <source>Starts</source>
        <translation>Inícios</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1409"/>
        <source>Remove Bookmark</source>
        <translation>Remover Favorito</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="220"/>
        <source>Breakdown</source>
        <translation>Discriminação</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="220"/>
        <source>events</source>
        <translation>eventos</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="533"/>
        <source>No %1 events are recorded this day</source>
        <translation>Nenhum evento %1 registrado neste dia</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="639"/>
        <source>%1 event</source>
        <translation>evento %1</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="640"/>
        <source>%1 events</source>
        <translation>eventos %1</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="231"/>
        <source>UF1</source>
        <translation>UF1</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="232"/>
        <source>UF2</source>
        <translation>UF2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="677"/>
        <source>Session Start Times</source>
        <translation>Tempos de Início de Sessão</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="678"/>
        <source>Session End Times</source>
        <translation>Tempos de Término de Sessão</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="881"/>
        <source>Duration</source>
        <translation>Duração</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="867"/>
        <source>Position Sensor Sessions</source>
        <translation>Sessões de Sensor de Posição</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="128"/>
        <source>Details</source>
        <translation>Detalhes</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="291"/>
        <source>Time at Pressure</source>
        <translation>Tempo na Pressão</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="872"/>
        <source>Unknown Session</source>
        <translation>Sessão Desconhecida</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="899"/>
        <source>Click to %1 this session.</source>
        <translation type="unfinished">Clique para %1 essa sessão</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="899"/>
        <source>disable</source>
        <translation>ativar</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="899"/>
        <source>enable</source>
        <translation>desativar</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="915"/>
        <source>%1 Session #%2</source>
        <translation>%1 Sessão #%2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="916"/>
        <source>%1h %2m %3s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="930"/>
        <source>One or more waveform record(s) for this session had faulty source data. Some waveform overlay points may not match up correctly.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="948"/>
        <source>Machine Settings Unavailable</source>
        <translation>Configurações de Máquina Indisponíveis</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1099"/>
        <source>PAP Mode: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1145"/>
        <source>%1%2</source>
        <translation>%1%2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1232"/>
        <source>Time over leak redline</source>
        <translation>Tempo acima da linha vermelha de vazamento</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1472"/>
        <source>Event Breakdown</source>
        <translation>Discriminação de Eventos</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1485"/>
        <source>Unable to display Pie Chart on this system</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1507"/>
        <source>Sessions all off!</source>
        <translation>Sessões todas desativadas!</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1509"/>
        <source>Sessions exist for this day but are switched off.</source>
        <translation>Sessões existem para esse dia mas estão desativadas.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1512"/>
        <source>Impossibly short session</source>
        <translation>Sessão impossivelmente curta</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1513"/>
        <source>Zero hours??</source>
        <translation>Zero horas??</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1516"/>
        <source>BRICK :(</source>
        <translation>PROBLEMA :(</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1517"/>
        <source>Sorry, this machine only provides compliance data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1518"/>
        <source>Complain to your Equipment Provider!</source>
        <translation>Reclama para seu fornecedor do equipamento!</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1140"/>
        <source>Statistics</source>
        <translation>Estatísticas</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1068"/>
        <source>Oximeter Information</source>
        <translation>Informação do Oxímetro</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1072"/>
        <source>SpO2 Desaturations</source>
        <translation>Dessaturações de SpO2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1073"/>
        <source>Pulse Change events</source>
        <translation>Eventos de Mudança de Pulso</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1074"/>
        <source>SpO2 Baseline Used</source>
        <translation>Patamar SpO2 Usado</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="942"/>
        <source>Machine Settings</source>
        <translation>Configurações de Máquina</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="838"/>
        <source>Session Information</source>
        <translation>Informações da Sessão</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="858"/>
        <source>CPAP Sessions</source>
        <translation>Sessões CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="861"/>
        <source>Oximetry Sessions</source>
        <translation>Sessões de Oxímetro</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="864"/>
        <source>Sleep Stage Sessions</source>
        <translation>Sessões de Estátio de Sono</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="946"/>
        <source>&lt;b&gt;Please Note:&lt;/b&gt; All settings shown below are based on assumptions that nothing&apos;s changed since previous days.</source>
        <translation>&lt;b&gt;Por favor note:&lt;/b&gt; Todas as configurações mostradas abaixo se baseiam em presunções de que nada mudou desde os dias anteriores.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1094"/>
        <source>Model %1 - %2</source>
        <translation>Modelo %1 - %2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1103"/>
        <source>(Mode/Pressure settings are guessed on this day.)</source>
        <translation>(Configurações de modo/pressão são conjecturadas nesse dia.)</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1212"/>
        <source>This day just contains summary data, only limited information is available.</source>
        <translation>Esse dia apenas contem dados sumários, apenas informação limitada está disponível.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1222"/>
        <source>Total time in apnea</source>
        <translation>Tempo total em apneia</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1238"/>
        <source>Total ramp time</source>
        <translation>Tempo total de rampa</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1242"/>
        <source>Time outside of ramp</source>
        <translation>Tempo fora da rampa</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1283"/>
        <source>Start</source>
        <translation>Início</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1283"/>
        <source>End</source>
        <translation>Fim</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1428"/>
        <source>BRICK! :(</source>
        <translation>PROBLEMA :(</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1536"/>
        <source>&quot;Nothing&apos;s here!&quot;</source>
        <translation>&quot;Nada aqui!&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1539"/>
        <source>No data is available for this day.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1831"/>
        <source>Pick a Colour</source>
        <translation>Escolha uma Cor</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2092"/>
        <source>This bookmarked is in a currently disabled area..</source>
        <translation>Essa favorito está em uma área desativada atualmente..</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2135"/>
        <source>Bookmark at %1</source>
        <translation>Favorito em %1</translation>
    </message>
</context>
<context>
    <name>ExportCSV</name>
    <message>
        <location filename="../oscar/exportcsv.ui" line="14"/>
        <source>Export as CSV</source>
        <translation>Exportar como CSV</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="24"/>
        <source>Dates:</source>
        <translation>Datas:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="37"/>
        <source>Resolution:</source>
        <translation>Resolução:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="46"/>
        <source>Details</source>
        <translation>Detalhes</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="53"/>
        <source>Sessions</source>
        <translation>Sessões</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="60"/>
        <source>Daily</source>
        <translation>Diariamente</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="85"/>
        <source>Filename:</source>
        <translation>Arquivo:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="107"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="114"/>
        <source>Export</source>
        <translation>Exportar</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="131"/>
        <source>Start:</source>
        <translation>Início:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="154"/>
        <source>End:</source>
        <translation>Fim:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="192"/>
        <source>Quick Range:</source>
        <translation>Intervalo Rápido:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="200"/>
        <location filename="../oscar/exportcsv.cpp" line="60"/>
        <location filename="../oscar/exportcsv.cpp" line="122"/>
        <source>Most Recent Day</source>
        <translation>Dira Mais Recente</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="205"/>
        <location filename="../oscar/exportcsv.cpp" line="125"/>
        <source>Last Week</source>
        <translation>Última Semana</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="210"/>
        <location filename="../oscar/exportcsv.cpp" line="128"/>
        <source>Last Fortnight</source>
        <translation>Última Quinzena</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="215"/>
        <location filename="../oscar/exportcsv.cpp" line="131"/>
        <source>Last Month</source>
        <translation>Último Mês</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="220"/>
        <location filename="../oscar/exportcsv.cpp" line="134"/>
        <source>Last 6 Months</source>
        <translation>Últimos 6 Meses</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="225"/>
        <location filename="../oscar/exportcsv.cpp" line="137"/>
        <source>Last Year</source>
        <translation>Último Ano</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="230"/>
        <location filename="../oscar/exportcsv.cpp" line="119"/>
        <source>Everything</source>
        <translation>Tudo</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="235"/>
        <location filename="../oscar/exportcsv.cpp" line="108"/>
        <source>Custom</source>
        <translation>Personalizado</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="73"/>
        <source>OSCAR_</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="76"/>
        <source>Details_</source>
        <translation>Details_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="78"/>
        <source>Sessions_</source>
        <translation>Sessions_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="80"/>
        <source>Summary_</source>
        <translation>Summary_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="87"/>
        <source>Select file to export to</source>
        <translation>Selecione arquivo para o qual exportar</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="88"/>
        <source>CSV Files (*.csv)</source>
        <translation>Arquivos CSV (*.csv)</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <source>DateTime</source>
        <translation>DataHora</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>Session</source>
        <translation>Sessão</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <source>Event</source>
        <translation>Evento</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <source>Data/Duration</source>
        <translation>Data/Duração</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <source>Session Count</source>
        <translation>Contagem de Sessão</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>Start</source>
        <translation>Início</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>End</source>
        <translation>Fim</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="207"/>
        <location filename="../oscar/exportcsv.cpp" line="210"/>
        <source>Total Time</source>
        <translation>Tempo Total</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="207"/>
        <location filename="../oscar/exportcsv.cpp" line="210"/>
        <source>AHI</source>
        <translation>IAH</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="214"/>
        <source> Count</source>
        <translation> Contagem</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="222"/>
        <source>%1% </source>
        <translation>%1% </translation>
    </message>
</context>
<context>
    <name>FPIconLoader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.cpp" line="139"/>
        <source>Import Error</source>
        <translation>Erro de Importação</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.cpp" line="140"/>
        <source>This Machine Record cannot be imported in this profile.</source>
        <translation>O relatório dessa máquina não pode ser importado nesse perfil.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.cpp" line="140"/>
        <source>The Day records overlap with already existing content.</source>
        <translation>Os registros diários sobrepõem-se com conteúdo pré-existente.</translation>
    </message>
</context>
<context>
    <name>Help</name>
    <message>
        <location filename="../oscar/help.ui" line="20"/>
        <source>Form</source>
        <translation>Forma</translation>
    </message>
    <message>
        <location filename="../oscar/help.ui" line="92"/>
        <source>Hide this message</source>
        <translation>Esconder essa mensagem</translation>
    </message>
    <message>
        <location filename="../oscar/help.ui" line="198"/>
        <source>Search Topic:</source>
        <translation>Tópico de Busca:</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="58"/>
        <source>Help Files are not yet available for %1 and will display in %2.</source>
        <translation>Arquivos de ajuda não estão disponíveis ainda para %1 e aparecerão em %2.</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="68"/>
        <source>Help files do not appear to be present.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="84"/>
        <source>HelpEngine did not set up correctly</source>
        <translation>HelpEngine não instalou corretamente</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="99"/>
        <source>HelpEngine could not register documentation correctly.</source>
        <translation>HelpEngine não conseguiu registrar a documentação corretamente.</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="110"/>
        <source>Contents</source>
        <translation>Conteúdos</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="111"/>
        <source>Index</source>
        <translation>Índice</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="115"/>
        <source>Search</source>
        <translation>Busca</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="125"/>
        <source>No documentation available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="213"/>
        <source>Please wait a bit.. Indexing still in progress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="239"/>
        <source>No</source>
        <translation>Não</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="240"/>
        <source>%1 result(s) for &quot;%2&quot;</source>
        <translation>%1 resultado(s) para &quot;%2&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="241"/>
        <source>clear</source>
        <translation>limpar</translation>
    </message>
</context>
<context>
    <name>MD300W1Loader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.cpp" line="165"/>
        <source>Could not find the oximeter file:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.cpp" line="171"/>
        <source>Could not open the oximeter file:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../oscar/mainwindow.ui" line="942"/>
        <source>&amp;Statistics</source>
        <translation>E&amp;statísticas</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="996"/>
        <source>Report Mode</source>
        <translation>Modo Relatório</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1003"/>
        <source>Standard</source>
        <translation>Padrão</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1013"/>
        <source>Monthly</source>
        <translation>Mensal</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1020"/>
        <source>Date Range</source>
        <translation>Faixa de Data</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1391"/>
        <source>Statistics</source>
        <translation>Estatísticas</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1441"/>
        <source>Daily</source>
        <translation>Diariamente</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1485"/>
        <source>Overview</source>
        <translation>Visão Geral</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1529"/>
        <location filename="../oscar/mainwindow.cpp" line="1112"/>
        <source>Oximetry</source>
        <translation>Oximetria</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1579"/>
        <source>Import</source>
        <translation>Importar</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1629"/>
        <source>Help</source>
        <translation>Ajuda</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2805"/>
        <source>&amp;File</source>
        <translation>&amp;Arquivo</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2830"/>
        <source>&amp;View</source>
        <translation>&amp;Exibir</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2848"/>
        <source>&amp;Help</source>
        <translation>A&amp;juda</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2864"/>
        <source>&amp;Data</source>
        <translation>&amp;Dados</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2868"/>
        <source>&amp;Advanced</source>
        <translation>A&amp;vançado</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2888"/>
        <source>Rebuild CPAP Data</source>
        <translation>Recompilar Dados CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2980"/>
        <source>&amp;Maximize Toggle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3185"/>
        <source>Report an Issue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2917"/>
        <source>&amp;Preferences</source>
        <translation>&amp;Preferências</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2922"/>
        <source>&amp;Profiles</source>
        <translation>&amp;Perfis</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2972"/>
        <source>&amp;About OSCAR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3170"/>
        <source>Show Performance Information</source>
        <translation>Mostrar Informação de Desempenho</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3175"/>
        <source>CSV Export Wizard</source>
        <translation>Assistente de Exportação CSV</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3180"/>
        <source>Export for Review</source>
        <translation>Exportar para Revisão</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="103"/>
        <source>E&amp;xit</source>
        <translation>Sai&amp;r</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2927"/>
        <source>Exit</source>
        <translation>Sair</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2932"/>
        <source>View &amp;Daily</source>
        <translation>Mostrar &amp;Diariamente</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2940"/>
        <source>View &amp;Overview</source>
        <translation>Ver Visã&amp;o Geral</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2948"/>
        <source>View &amp;Welcome</source>
        <translation>Ver Boas-vi&amp;ndas</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2956"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2967"/>
        <source>Use &amp;AntiAliasing</source>
        <translation>Usar &amp;AntiAliasing</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2991"/>
        <source>Show Debug Pane</source>
        <translation>Mostrar Painel Depurador</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2999"/>
        <source>&amp;Reset Graph Layout</source>
        <translation>&amp;Redefinir Disposição de Gráficos</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3004"/>
        <source>Take &amp;Screenshot</source>
        <translation>Salvar Captura de &amp;Tela</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3012"/>
        <source>O&amp;ximetry Wizard</source>
        <translation>Assistente de O&amp;ximetria</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3020"/>
        <source>Print &amp;Report</source>
        <translation>Imprimir &amp;Relatório</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3025"/>
        <source>&amp;Edit Profile</source>
        <translation>&amp;Editar Perfil</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3154"/>
        <source>Daily Calendar</source>
        <translation>Calendário Diário</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3162"/>
        <source>Backup &amp;Journal</source>
        <translation>Fazer Backu&amp;p do Diário</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3030"/>
        <source>Online Users &amp;Guide</source>
        <translation>&amp;Guia Online do Utilizador</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="441"/>
        <source>OSCAR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3035"/>
        <source>&amp;Frequently Asked Questions</source>
        <translation>Perguntas &amp;Frequentes</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3040"/>
        <source>&amp;Automatic Oximetry Cleanup</source>
        <translation>Limpeza &amp;Automática de Oximetria</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3121"/>
        <source>Toggle &amp;Line Cursor</source>
        <translation>Alternar &amp;Linha do Cursor</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3045"/>
        <source>Change &amp;User</source>
        <translation>Trocar &amp;Utilizador</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3050"/>
        <source>Purge &amp;Current Selected Day</source>
        <translation>Remover Dia S&amp;elecionado</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3061"/>
        <source>Right &amp;Sidebar</source>
        <translation>Barra Lateral Di&amp;reita</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3140"/>
        <source>Daily Sidebar</source>
        <translation>Barra Lateral Diária</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3069"/>
        <source>View S&amp;tatistics</source>
        <translation>Ver E&amp;statísticas</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1320"/>
        <source>Navigation</source>
        <translation>Navegação</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1751"/>
        <source>Bookmarks</source>
        <translation>Favoritos</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2746"/>
        <source>Records</source>
        <translation>Registros</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2878"/>
        <source>Purge ALL CPAP Data</source>
        <translation>Remover TODOS Dados CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2809"/>
        <source>Exp&amp;ort Data</source>
        <translation>Exp&amp;ortar Dados</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1347"/>
        <source>Profiles</source>
        <translation>Perfis</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2872"/>
        <source>Purge Oximetry Data</source>
        <translation>Remover Dados Oximétricos</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2909"/>
        <source>&amp;Import SDcard Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3072"/>
        <location filename="../oscar/mainwindow.ui" line="3075"/>
        <source>View Statistics</source>
        <translation>Ver Estatísticas</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3083"/>
        <source>Import &amp;ZEO Data</source>
        <translation>Importar Dados &amp;ZEO</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3088"/>
        <source>Import RemStar &amp;MSeries Data</source>
        <translation>Importar Dados RemStar &amp;MSeries</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3093"/>
        <source>Sleep Disorder Terms &amp;Glossary</source>
        <translation>G&amp;lossário de Termos de Desordens do Sono</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3098"/>
        <source>Change &amp;Language</source>
        <translation>A&amp;lterar Idioma</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3103"/>
        <source>Change &amp;Data Folder</source>
        <translation>Alterar Pasta de &amp;Dados</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3108"/>
        <source>Import &amp;Somnopose Data</source>
        <translation>Importar Dados &amp;Somnopose</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3113"/>
        <source>Current Days</source>
        <translation>Dias Atuais</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="522"/>
        <source>Profile</source>
        <translation>Perfil</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="501"/>
        <location filename="../oscar/mainwindow.cpp" line="2237"/>
        <source>Welcome</source>
        <translation>Bem-Vindo</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="100"/>
        <source>&amp;About</source>
        <translation>So&amp;bre</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="877"/>
        <source>Access to Import has been blocked while recalculations are in progress.</source>
        <translation>Acesso para importar foi bloqueado enquanto cálculos estão em progresso.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1030"/>
        <source>Importing Data</source>
        <translation>Importando Dados</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="703"/>
        <location filename="../oscar/mainwindow.cpp" line="1932"/>
        <source>Please wait, importing from backup folder(s)...</source>
        <translation>Por favor aguarde, importando da(s) pasta(s) de backup...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="648"/>
        <source>Import Problem</source>
        <translation>Importar Problema</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="312"/>
        <source>%1 %2</source>
        <translation>%1 %2</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="648"/>
        <source>Couldn&apos;t find any valid Machine Data at

%1</source>
        <translation>Impossível encontrar qualquer Dado de Máquina valido em

%1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="804"/>
        <source>Please insert your CPAP data card...</source>
        <translation>Por favor insira seu cartão de dados do CPAP...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="881"/>
        <source>Import is already running in the background.</source>
        <translation>Importação já em execução em segundo plano.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="916"/>
        <source>CPAP Data Located</source>
        <translation>Dados do CPAP Localizados</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="945"/>
        <source>Please remember to point the importer at the root folder or drive letter of your data-card, and not a subfolder.</source>
        <translation>Por favor lembre-se de apontar o importador para a pasta raíz ou letra de drive do seu cartão de memória e não uma subpasta.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="946"/>
        <source>Import Reminder</source>
        <translation>Lembrete de Importação</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1009"/>
        <source>Processing import list...</source>
        <translation>Processando lista de importação...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1099"/>
        <source>This software has been created to assist you in reviewing the data produced by CPAP Machines, used in the treatment of various Sleep Disorders.</source>
        <translation>Esse software foi criado para ajudá-lo na revisão dos dados produzidos por máquinas CPAP, usadas no tratamento de várias desordesn do sono.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1103"/>
        <source>This is a beta release, some features may not yet behave as expected.</source>
        <translation>Essa é uma versão beta, alguns recursos podem ainda não funcionar como é esperado.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1106"/>
        <source>Currenly supported machines:</source>
        <translation>Máquinas atualmente suportadas:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1107"/>
        <source>CPAP</source>
        <translation>CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1109"/>
        <source>ResMed S9 models (CPAP, Auto, VPAP)</source>
        <translation>Modelos ResMed S9 (CPAP, Auto, VPAP)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1110"/>
        <source>DeVilbiss Intellipap (Auto)</source>
        <translation>DeVilbiss Intellipap (Auto)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1111"/>
        <source>Fisher &amp; Paykel ICON (CPAP, Auto)</source>
        <translation>Fisher &amp; Paykel ICON (CPAP, Auto)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1113"/>
        <source>Contec CMS50D+, CMS50E and CMS50F (not 50FW) Oximeters</source>
        <translation>Contec CMS50D+, CMS50E e CMS50F (não 50FW) Oxímetros</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1114"/>
        <source>ResMed S9 Oximeter Attachment</source>
        <translation>Oxímetro Adicional ResMed S9</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1115"/>
        <source>Online Help Resources</source>
        <translation>Recursos Online de Ajuda</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1116"/>
        <source>Note:</source>
        <translation>Nota:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1117"/>
        <source>I don&apos;t recommend using this built in web browser to do any major surfing in, it will work, but it&apos;s mainly meant as a help browser.</source>
        <translation>Eu não recomendo usar esse navegador integrado para maiores navegações, ele funciona, mas é fornecido como um navegador de ajuda.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1119"/>
        <source>(It doesn&apos;t support SSL encryption, so it&apos;s not a good idea to type your passwords or personal details anywhere.)</source>
        <translation>(Ele não suporta criptografia SSL, então não é uma boa ideia digitar suas senhas ou detalhes pessoais nele)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1132"/>
        <source>Further Information</source>
        <translation>Informação Adicional</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1136"/>
        <source>Plus a few &lt;a href=&apos;qrc:/docs/usage.html&apos;&gt;usage notes&lt;/a&gt;, and some important information for Mac users.</source>
        <translation>Além de algumas &lt;a href=&apos;qrc:/docs/usage.html&apos;&gt;notas de uso&lt;/a&gt;, e alguma informação importante para utilizadores Mac.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1138"/>
        <source>About &lt;a href=&apos;http://en.wikipedia.org/wiki/Sleep_apnea&apos;&gt;Sleep Apnea&lt;/a&gt; on Wikipedia</source>
        <translation>Sobre &lt;a href=&apos;http://pt.wikipedia.org/wiki/Apneia_do_sono&apos;&gt;Apneia do Sono&lt;/a&gt; na Wikipédia</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1141"/>
        <source>Friendly forums to talk and learn about Sleep Apnea:</source>
        <translation>Fóruns amigáveis para falar e aprender sobre Apneia do Sono:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1142"/>
        <source>&lt;a href=&apos;http://www.cpaptalk.com&apos;&gt;CPAPTalk Forum&lt;/a&gt;,</source>
        <translation>&lt;a href=&apos;http://www.cpaptalk.com&apos;&gt;CPAPTalk Forum (em inglês)&lt;/a&gt;,</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1151"/>
        <source>Copyright:</source>
        <translation>Direitos autorais:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1153"/>
        <source>License:</source>
        <translation>Licença:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1157"/>
        <source>DISCLAIMER:</source>
        <translation>AVISO LEGAL:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1159"/>
        <source>This is &lt;font color=&apos;red&apos;&gt;&lt;u&gt;NOT&lt;/u&gt;&lt;/font&gt; medical software. This application is merely a data viewer, and no guarantee is made regarding accuracy or correctness of any calculations or data displayed.</source>
        <translation>Esse &lt;font color=&apos;red&apos;&gt;&lt;u&gt;NÃO&lt;/u&gt;&lt;/font&gt; é um software médico. Esse aplicativo é apenas um visualizador de dados, e nenhuma garantia é feita com relação a sua precisão ou exatidão de quaisquer cálculos ou dados mostrados.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1163"/>
        <source>Your doctor should always be your first and best source of guidance regarding the important matter of managing your health.</source>
        <translation>Seu médico deve sempre ser sua primeira e melhor fonte de orientações sobre a matéria importante de gerenciar sua saúde.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1165"/>
        <source>*** &lt;u&gt;Use at your own risk&lt;/u&gt; ***</source>
        <translation>*** &lt;u&gt;Use por sua conta e risco&lt;/u&gt; ***</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1291"/>
        <source>Please open a profile first.</source>
        <translation>Por favor abra um perfil primeiro.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1910"/>
        <source>Please note, that this could result in loss of data if OSCAR&apos;s backups have been disabled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1972"/>
        <source>You are about to &lt;font size=+2&gt;obliterate&lt;/font&gt; OSCAR&apos;s machine database for the following machine:&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2520"/>
        <source>%1&apos;s Journal</source>
        <translation>Diário de %1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2522"/>
        <source>Choose where to save journal</source>
        <translation>Escolha onde salvar o diário</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2522"/>
        <source>XML Files (*.xml)</source>
        <translation>Arquivos XML (*.xml)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1296"/>
        <source>Access to Preferences has been blocked until recalculation completes.</source>
        <translation>Acesso às preferêcias foi bloqueado até que os cálculos terminem.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2459"/>
        <source>Are you sure you want to delete oximetry data for %1</source>
        <translation>Você tem certeza de que deseja deletar dados oximétricos para %1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2461"/>
        <source>&lt;b&gt;Please be aware you can not undo this operation!&lt;/b&gt;</source>
        <translation>&lt;b&gt;Por favor esteja ciente de que você não pode desfazer a operação!&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2482"/>
        <source>Select the day with valid oximetry data in daily view first.</source>
        <translation>Selecione o dia com dados oxímetros válidos na visualização diária primeiro.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1108"/>
        <source>Philips Respironics System One (CPAP Pro, Auto, BiPAP &amp; ASV models)</source>
        <translation>Philips Respironics System One (modelos CPAP Pro, Auto, BiPAP e ASV)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="238"/>
        <source>Help Browser</source>
        <translation>Navegador de Ajuda</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="483"/>
        <source>Loading profile &quot;%1&quot;</source>
        <translation>Carregando perfil &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="811"/>
        <source>Choose a folder</source>
        <translation>Escolha uma pasta</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="909"/>
        <source>A %1 file structure for a %2 was located at:</source>
        <translation>Uma estrutura de arquivos %1 para %2 foi localizada em:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="911"/>
        <source>A %1 file structure was located at:</source>
        <translation>Uma estrutura de arquivos %1 foi localizada em:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="917"/>
        <source>Would you like to import from this location?</source>
        <translation>Você gostaria de importar dessa localização?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="920"/>
        <source>Specify</source>
        <translation>Especifique</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1143"/>
        <source>&lt;a href=&apos;http://www.apneaboard.com/forums/&apos;&gt;Apnea Board&lt;/a&gt;</source>
        <translation>&lt;a href=&apos;http://www.apneaboard.com/forums/&apos;&gt;Apnea Board&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1393"/>
        <source>There was an error saving screenshot to file &quot;%1&quot;</source>
        <translation>Erro ao salvar a captura de tela para o arquivo &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1395"/>
        <source>Screenshot saved to file &quot;%1&quot;</source>
        <translation>Captura de tela salva para o arquivo &quot;%1&apos;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1766"/>
        <location filename="../oscar/mainwindow.cpp" line="1793"/>
        <source>Gah!</source>
        <translation>Gah!</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1919"/>
        <source>Provided you have made &lt;i&gt;your &lt;b&gt;own&lt;/b&gt; backups for ALL of your CPAP data&lt;/i&gt;, you can still complete this operation, but you will have to restore from your backups manually.</source>
        <translation>Contanto que você possua &lt;i&gt;seus&lt;b&gt;próprios&lt;/b&gt; backups para TODOS os seus dados de CPAP&lt;/i&gt;, você ainda pode completar essa operação, mas você precisará restaurar manualmente a partir dos seus backups.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1920"/>
        <source>Are you really sure you want to do this?</source>
        <translation>Tem certeza de que deseja fazer isso?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1935"/>
        <source>Because there are no internal backups to rebuild from, you will have to restore from your own.</source>
        <translation>Por não existir backups internos a partir dos quais recompilar, você terá que restaurar a partir dos seus próprios.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="644"/>
        <source>Imported %1 CPAP session(s) from

%2</source>
        <translation>Importadas %1 sessão(ões) CPAP de

%2</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="644"/>
        <source>Import Success</source>
        <translation>Sucesso na Importação</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="646"/>
        <source>Already up to date with CPAP data at

%1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="646"/>
        <source>Up to date</source>
        <translation>Atualizado</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="873"/>
        <source>No profile has been selected for Import.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1095"/>
        <source>Welcome to OSCAR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1098"/>
        <source>About OSCAR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1101"/>
        <source>OSCAR has been designed by a software developer with personal experience with a sleep disorder, and shaped by the feedback of many other willing testers dealing with similar conditions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1104"/>
        <source>Please report any bugs you find to the OSCAR developer&apos;s group.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1134"/>
        <source>The release notes for this version can be found in the About OSCAR menu item.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1154"/>
        <source>This software is released freely under the &lt;a href=&quot;qrc:/COPYING&quot;&gt;GNU Public License version 3&lt;/a&gt;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1161"/>
        <source>The authors will NOT be held liable by anyone who harms themselves or others by use or misuse of this software.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1333"/>
        <source>Updates are not yet implemented</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1511"/>
        <source>The User&apos;s Guide will open in your default browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1519"/>
        <source>The FAQ is not yet implemented</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1767"/>
        <location filename="../oscar/mainwindow.cpp" line="1794"/>
        <source>If you can read this, the restart command didn&apos;t work. You will have to do it yourself manually.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1907"/>
        <source>Are you sure you want to rebuild all CPAP data for the following machine:

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1917"/>
        <source>For some reason, OSCAR does not have any backups for the following machine:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1936"/>
        <source>Would you like to import from your own backups now? (you will have no data visible for this machine until you do)</source>
        <translation>Gostaria de importar de seus próprios backups agora? (você não terá quaisquer dados visíveis para essa máquina até fazê-lo)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2310"/>
        <source>The Glossary will open in your default browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2542"/>
        <source>Export review is not yet implemented</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2557"/>
        <source>Reporting issues is not yet implemented</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1974"/>
        <source>Note as a precaution, the backup folder will be left in place.</source>
        <translation>Note como precaução, a pasta de backup será mantida no mesmo lugar.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1975"/>
        <source>Are you &lt;b&gt;absolutely sure&lt;/b&gt; you want to proceed?</source>
        <translation>Você tem &lt;b&gt;absoluta certeza&lt;/b&gt; de que deseja prosseguir?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2025"/>
        <source>A file permission error casued the purge process to fail; you will have to delete the following folder manually:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2074"/>
        <source>No help is available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2269"/>
        <source>There was a problem opening ZEO File: </source>
        <translation>Houve um problema ao abrir o arquivo ZEO: </translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2273"/>
        <source>Zeo CSV Import complete</source>
        <translation>Importação do CSV Zeo completa</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2295"/>
        <source>There was a problem opening MSeries block File: </source>
        <translation>Houve um problema abrindo o arquivo de bloco MSeries: </translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2299"/>
        <source>MSeries Import complete</source>
        <translation>Importação de MSeries completa</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2318"/>
        <source>Donations are not implemented</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2356"/>
        <source>There was a problem opening Somnopose Data File: </source>
        <translation>Houve um problema abrindo o arquivo de dados Somnopose: </translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2360"/>
        <source>Somnopause Data Import complete</source>
        <translation>Importação de dados Somnopose completa</translation>
    </message>
</context>
<context>
    <name>MinMaxWidget</name>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1964"/>
        <source>Auto-Fit</source>
        <translation>Auto-Ajuste</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1965"/>
        <source>Defaults</source>
        <translation>Padrões</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1966"/>
        <source>Override</source>
        <translation>Substituir</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1967"/>
        <source>The Y-Axis scaling mode, &apos;Auto-Fit&apos; for automatic scaling, &apos;Defaults&apos; for settings according to manufacturer, and &apos;Override&apos; to choose your own.</source>
        <translation>O modo de escala do eixo-Y, &apos;Auto-Ajuste&apos; para auto-escala, &apos;Padrões&apos; para configurar de acordo com o fabricante, e &apos;Substituir&apos; para escolher o seu próprio.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1973"/>
        <source>The Minimum Y-Axis value.. Note this can be a negative number if you wish.</source>
        <translation>O valor Mínimo de eixo-Y.. Note que esse pode ser um número negativo se você quiser.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1974"/>
        <source>The Maximum Y-Axis value.. Must be greater than Minimum to work.</source>
        <translation>O valor Máximo de eixo-Y.. Deve ser maior do que o mínimo para funcionar.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2009"/>
        <source>Scaling Mode</source>
        <translation>Modo de Escala</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2031"/>
        <source>This button resets the Min and Max to match the Auto-Fit</source>
        <translation>Esse botão redefine o Min e Máx para combinar com a Auto-Escala</translation>
    </message>
</context>
<context>
    <name>NewProfile</name>
    <message>
        <location filename="../oscar/newprofile.ui" line="14"/>
        <source>Edit User Profile</source>
        <translation>Editar Perfil de Utilizador</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="70"/>
        <source>I agree to all the conditions above.</source>
        <translation>Eu concordo com todas as condições acima.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="111"/>
        <source>User Information</source>
        <translation>Informação de Utilizador</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="137"/>
        <source>User Name</source>
        <translation>Nome de Utilizador</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="152"/>
        <source>Keep the kids out.. Nothing more.. This isn&apos;t meant to be uber security.</source>
        <translation>Mantenha as crianças fora.. Nada mais.. Isso não foi feito para alta segurança.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="155"/>
        <source>Password Protect Profile</source>
        <translation>Proteger Perfil com Senha</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="182"/>
        <source>Password</source>
        <translation>Senha</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="196"/>
        <source>...twice...</source>
        <translation>...duas vezes...</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="213"/>
        <source>Locale Settings</source>
        <translation>Configurações de Localidade</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="285"/>
        <source>Country</source>
        <translation>País</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="253"/>
        <source>TimeZone</source>
        <translation>Zona Horária</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="54"/>
        <source>about:blank</source>
        <translation>about:blank</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="240"/>
        <source>DST Zone</source>
        <translation>Horário de Verão</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="320"/>
        <source>Personal Information (for reports)</source>
        <translation>Informação Pessoal (para relatórios)</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="344"/>
        <source>First Name</source>
        <translation>Primeiro Nome</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="354"/>
        <source>Last Name</source>
        <translation>Sobrenome</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="364"/>
        <source>It&apos;s totally ok to fib or skip this, but your rough age is needed to enhance accuracy of certain calculations.</source>
        <translation>Não há problema em mentir ou pular isso, mas sua idade aproximada é necessária para melhorar a qualidade de certos cálculos.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="367"/>
        <source>D.O.B.</source>
        <translation>Data de Nasc.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="383"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Biological (birth) gender is sometimes needed to enhance the accuracy of a few calculations, feel free to leave this blank and skip any of them.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Sexo biológico (nascença) é necessário algumas vezes para melhorar a precisão de alguns cálculos, sinta-se livre para deixar isso em branco e pular perguntas.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="386"/>
        <source>Gender</source>
        <translation>Sexo</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="399"/>
        <source>Male</source>
        <translation>Masculino</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="404"/>
        <source>Female</source>
        <translation>Feminino</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="420"/>
        <source>Height</source>
        <translation>Altura</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="459"/>
        <source>metric</source>
        <translation>métrico</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="464"/>
        <source>archiac</source>
        <translation>arcaico</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="477"/>
        <source>Contact Information</source>
        <translation>Informações de Contato</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="504"/>
        <location filename="../oscar/newprofile.ui" line="779"/>
        <source>Address</source>
        <translation>Endereço</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="521"/>
        <location filename="../oscar/newprofile.ui" line="810"/>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="531"/>
        <location filename="../oscar/newprofile.ui" line="800"/>
        <source>Phone</source>
        <translation>Telefone</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="576"/>
        <source>CPAP Treatment Information</source>
        <translation>Informação de Tratamento CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="603"/>
        <source>Date Diagnosed</source>
        <translation>Data do Diagnóstico</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="617"/>
        <source>Untreated AHI</source>
        <translation>IAH Não Tratado</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="631"/>
        <source>CPAP Mode</source>
        <translation>Modo CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="639"/>
        <source>CPAP</source>
        <translation>CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="644"/>
        <source>APAP</source>
        <translation>APAP</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="649"/>
        <source>Bi-Level</source>
        <translation>Bi-Level</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="654"/>
        <source>ASV</source>
        <translation>ASV</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="662"/>
        <source>RX Pressure</source>
        <translation>Pressão RX</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="708"/>
        <source>Doctors / Clinic Information</source>
        <translation>Informação do Médico / Clínica</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="735"/>
        <source>Doctors Name</source>
        <translation>Nome do Médico</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="752"/>
        <source>Practice Name</source>
        <translation>Nome da Clínica</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="762"/>
        <source>Patient ID</source>
        <translation>RG do Paciente</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="877"/>
        <source>OSCAR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="955"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancel</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="971"/>
        <source>&amp;Back</source>
        <translation>&amp;Voltar</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="987"/>
        <location filename="../oscar/newprofile.cpp" line="274"/>
        <location filename="../oscar/newprofile.cpp" line="283"/>
        <source>&amp;Next</source>
        <translation>&amp;Próximo</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="61"/>
        <source>Select Country</source>
        <translation>Selecione o País</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="111"/>
        <source>This software is being designed to assist you in reviewing the data produced by your CPAP machines and related equipment.</source>
        <translation>Esse software está sendo projetado para ajudá-lo a revisar os dados produzidos por máquinas CPAP e equipamento relacionado.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="116"/>
        <source>PLEASE READ CAREFULLY</source>
        <translation>POR FAVOR LEIA COM ATENÇÃO</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="120"/>
        <source>Accuracy of any data displayed is not and can not be guaranteed.</source>
        <translation>Precisão de qualquer dado mostrado não é não pode ser garantida.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="122"/>
        <source>Any reports generated are for PERSONAL USE ONLY, and NOT IN ANY WAY fit for compliance or medical diagnostic purposes.</source>
        <translation>Quaisquer relatórios gerados são para USO PESSOAL SOMENTE, e DE NENHUM MODO servem para propósitos de observância ou diagnóstico médico.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="129"/>
        <source>Use of this software is entirely at your own risk.</source>
        <translation>Uso desse software é interamente por sua conta e risco.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="109"/>
        <source>Welcome to the Open Source CPAP Analysis Reporter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="114"/>
        <source>OSCAR has been released freely under the &lt;a href=&apos;qrc:/COPYING&apos;&gt;GNU Public License v3&lt;/a&gt;, and comes with no warranty, and without ANY claims to fitness for any purpose.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="117"/>
        <source>OSCAR is intended merely as a data viewer, and definitely not a substitute for competent medical guidance from your Doctor.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="125"/>
        <source>The authors will not be held liable for &lt;u&gt;anything&lt;/u&gt; related to the use or misuse of this software.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="132"/>
        <source>OSCAR is copyright &amp;copy;2011-2018 Mark Watkins and portions &amp;copy;2019 Nightowl Software</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="155"/>
        <source>Please provide a username for this profile</source>
        <translation>Por favor forneça um nome de utilizador para esse perfil</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="165"/>
        <source>Passwords don&apos;t match</source>
        <translation>Senhas não correspondem</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="195"/>
        <source>Profile Changes</source>
        <translation>Mudanças de Perfil</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="195"/>
        <source>Accept and save this information?</source>
        <translation>Aceitar e salvar essa informação?</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="272"/>
        <source>&amp;Finish</source>
        <translation>&amp;Finalizar</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="448"/>
        <source>&amp;Close this window</source>
        <translation>Fe&amp;char essa janela</translation>
    </message>
</context>
<context>
    <name>Overview</name>
    <message>
        <location filename="../oscar/overview.ui" line="14"/>
        <source>Form</source>
        <translation>De</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="68"/>
        <source>Range:</source>
        <translation>Intervalo:</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="82"/>
        <source>Last Week</source>
        <translation>Última Semana</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="87"/>
        <source>Last Two Weeks</source>
        <translation>Últimas Duas Semanas</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="92"/>
        <source>Last Month</source>
        <translation>Último Mês</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="97"/>
        <source>Last Two Months</source>
        <translation>Últimos Dois Meses</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="102"/>
        <source>Last Three Months</source>
        <translation>Últimos Três Meses</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="107"/>
        <source>Last 6 Months</source>
        <translation>Últimos 6 Meses</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="112"/>
        <source>Last Year</source>
        <translation>Último Ano</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="117"/>
        <source>Everything</source>
        <translation>Tudo</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="122"/>
        <source>Custom</source>
        <translation>Personalizado</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="130"/>
        <source>Start:</source>
        <translation>Início:</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="153"/>
        <source>End:</source>
        <translation>Fim:</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="176"/>
        <source>Reset view to selected date range</source>
        <translation>Redefinir visualização para a faixa de data selecionada</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="195"/>
        <location filename="../oscar/overview.ui" line="244"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="225"/>
        <source>Toggle Graph Visibility</source>
        <translation>Alternar Visibilidade do Gráfico</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="260"/>
        <source>Drop down to see list of graphs to switch on/off.</source>
        <translation>Desça para ver a lista de gráficos para des/ativar.</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="264"/>
        <source>Graphs</source>
        <translation>Gráficos</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="158"/>
        <source>Respiratory
Disturbance
Index</source>
        <translation>Índice
Distúrbio
Respiratório</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="160"/>
        <source>Apnea
Hypopnea
Index</source>
        <translation>Índice
Hipoapneia
Apneia</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="166"/>
        <source>Usage</source>
        <translation>Uso</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="166"/>
        <source>Usage
(hours)</source>
        <translation>Uso
(horas)</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="169"/>
        <source>Session Times</source>
        <translation>Tempos de Sessão</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="177"/>
        <source>Total Time in Apnea</source>
        <translation>Tempo Total em Apneia</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="177"/>
        <source>Total Time in Apnea
(Minutes)</source>
        <translation>Tempo Total em Apneia
(Minutos)</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="211"/>
        <source>Body
Mass
Index</source>
        <translation>Índice
Massa
Corporal</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="212"/>
        <source>How you felt
(0-10)</source>
        <translation>Como se sentiu
(0-10)</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="502"/>
        <source>Show all graphs</source>
        <translation>Mostrar todos os gráficos</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="515"/>
        <source>Hide all graphs</source>
        <translation>Esconder todos os gráficos</translation>
    </message>
</context>
<context>
    <name>OximeterImport</name>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="14"/>
        <source>Dialog</source>
        <translation>Diálogo</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="61"/>
        <location filename="../oscar/oximeterimport.cpp" line="37"/>
        <source>Oximeter Import Wizard</source>
        <translation>Assistente de Importação do Oxímetro</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="768"/>
        <source>Skip this page next time.</source>
        <translation>Pular essa página da próxima vez.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="866"/>
        <source>Where would you like to import from?</source>
        <translation>De onde você gostaria de importar?</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="967"/>
        <source>CMS50E/F users, when importing directly, please don&apos;t select upload on your device until OSCAR prompts you to.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1004"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If enabled, OSCAR will automatically reset your CMS50&apos;s internal clock using your computers current time.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1077"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option will erase the imported session from your oximeter after import has completed. &lt;/p&gt;&lt;p&gt;Use with caution,  becauseif something goes wrong before OSCAR saves your session, you won&apos;t get it back.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1106"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option allows you to import (via cable) from your oximeters internal recordings.&lt;/p&gt;&lt;p&gt;After selecting on this option, some oximeters will require you to do something in the devices menu to initiate the upload.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Essa opção permite que você importe (via cabo) das gravações internas do seu oxímetro.&lt;/p&gt;&lt;p&gt;Após selecionar nesse opção, alguns oxímetros exigirão que você realize alguma ação no menu do aparelho para iniciar o envio.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1148"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If you don&apos;t mind a being attached to a running computer overnight, this option provide a useful plethysomogram graph, which gives an indication of heart rhythm, on top of the normal oximetry readings.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Se você não se importa de ficar conectado a um computador ligado a noite toda, essa opção fornece um gráfico pletismográfico útil, que fornece uma indicação de rítmo cardíaco, em adição às leituras oximétricas normais.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1154"/>
        <source>Record attached to computer overnight (provides plethysomogram)</source>
        <translation>Registrar conectado ao computador durante a noite (fornece pletismograma)</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1187"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option allows you to import from data files created by software that came with your Pulse Oximeter, such as SpO2Review.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Essa opção permite que você importe arquivos de dado criados pelo software que acompanha o seu Oxímetro de Pulso, como o SpO2Review.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1193"/>
        <source>Import from a datafile saved by another program, like SpO2Review</source>
        <translation>Importar de um arquivo de dado salvo por outro programa, como SpO2Review</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1272"/>
        <location filename="../oscar/oximeterimport.ui" line="1319"/>
        <source>Please connect your oximeter device</source>
        <translation>Por favor conecte seu aparelho oxímetro</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1290"/>
        <source>If you can read this, you likely have your oximeter type set wrong in preferences.</source>
        <translation>Se você pode ler isso, provavelmente você configurou o tipo de oxímetro errado nas preferências.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1345"/>
        <source>Press Start to commence recording</source>
        <translation>Pressione Início para começar o registro</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1391"/>
        <source>Show Live Graphs</source>
        <translation>Mostrar Gráficos em Tempo Real</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1422"/>
        <location filename="../oscar/oximeterimport.ui" line="1676"/>
        <source>Duration</source>
        <translation>Duração</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1458"/>
        <source>SpO2 %</source>
        <translation>SpO2 %</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1540"/>
        <source>Pulse Rate</source>
        <translation>Taxa de Pulso</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1632"/>
        <source>Multiple Sessions Detected</source>
        <translation>Múltiplas Sessões Detectadas</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1671"/>
        <source>Start Time</source>
        <translation>Tempo de Início</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1681"/>
        <source>Details</source>
        <translation>Detalhes</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1698"/>
        <source>Import Completed. When did the recording start?</source>
        <translation>Importação Completada. Quando a gravação começou?</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1722"/>
        <source>Day recording (normally would of) started</source>
        <translation>Gravação diária (normalmente teria) começado</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1768"/>
        <source>Oximeter Starting time</source>
        <translation>Tempo de Início do Oxímetro</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1780"/>
        <source>I want to use the time reported by my oximeter&apos;s built in clock.</source>
        <translation>Eu gostaria de usar o tempo relatado pelo relógio interno do meu oxímetro.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1796"/>
        <source>I started this oximeter recording at (or near) the same time as a session on my CPAP machine.</source>
        <translation>Eu comecei essa gravação de oxímetro ao mesmo (ou próximo) tempo que minha máquina CPAP.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1857"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Note: Syncing to CPAP session starting time will always be more accurate.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Nota: Sincronizar para o tempo de início do CPAP será mais preciso sempre.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1878"/>
        <source>Choose CPAP session to sync to:</source>
        <translation>Escolha uma sessão CPAP com a qual sincronizar:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1917"/>
        <location filename="../oscar/oximeterimport.ui" line="1956"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1998"/>
        <source>You can manually adjust the time here if required:</source>
        <translation>Você pode ajustar manualmente o tempo aqui se necessário:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2019"/>
        <source>HH:mm:ssap</source>
        <translation>HH:mm:ss</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2116"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2097"/>
        <source>&amp;Information Page</source>
        <translation>Página de &amp;Informação</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="823"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Please note: &lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Make sure your correct oximeter type is selected otherwise import will fail.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Por favor note: &lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Certifique-se de que o tipo de oxímetro correto está selecionado do contrário a importação falhará.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="910"/>
        <source>Select Oximeter Type:</source>
        <translation>Selecione o Tipo de Oxímetro:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="921"/>
        <source>CMS50Fv3.7+/H/I, Pulox PO-400/500</source>
        <translation>CMS50Fv3.7+/H/I, Pulox PO-400/500</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="926"/>
        <source>CMS50D+/E/F, Pulox PO-200/300</source>
        <translation>CMS50D+/E/F, Pulox PO-200/300</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="931"/>
        <source>ChoiceMMed MD300W1</source>
        <translation>ChoiceMMed MD300W1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1007"/>
        <source>Set device date/time</source>
        <translation>Definir data/hora do aparelho</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1014"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Check to enable updating the device identifier next import, which is useful for those who have multiple oximeters lying around.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Marque para ativar a atualização do identificador do aparelho na próxima importação, o que é útil para aqueles que possuem múltiplos oxímeros largados por perto.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1017"/>
        <source>Set device identifier</source>
        <translation>Definir identificador do aparelho</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1036"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Here you can enter a 7 character pet name for this oximeter.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Aqui você pode definir um nome de estimação de 7 caractéres para esse oxímetro.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1080"/>
        <source>Erase session after successful upload</source>
        <translation>Apagar sessão após envio concluído</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1112"/>
        <source>Import directly from a recording on a device</source>
        <translation>Importar diretamente de uma gravação num aparelho</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1237"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Reminder for CPAP users: &lt;/span&gt;&lt;span style=&quot; color:#fb0000;&quot;&gt;Did you remember to import your CPAP sessions first?&lt;br/&gt;&lt;/span&gt;If you forget, you won&apos;t have a valid time to sync this oximetry session to.&lt;br/&gt;To a ensure good sync between devices, always try to start both at the same time.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Lembrete para utilizadores de CPAP: &lt;/span&gt;&lt;span style=&quot; color:#fb0000;&quot;&gt;Você lembrou de importar as sessões do CPAP primeiro?&lt;br/&gt;&lt;/span&gt;Se você esqueceu, você não terá um tempo válido para o qual sincronizar essa sessão de oximetria.&lt;br/&gt;Para garantir uma boa sincronia entre aparelhos, sempre tente iniciar ambos ao mesmo tempo.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1648"/>
        <source>Please choose which one you want to import into OSCAR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1825"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;OSCAR needs a starting time to know where to save this oximetry session to.&lt;/p&gt;&lt;p&gt;Choose one of the following options:&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2135"/>
        <source>&amp;Retry</source>
        <translation>Tenta&amp;r Novamente</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2154"/>
        <source>&amp;Choose Session</source>
        <translation>Es&amp;colher Sessão</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2173"/>
        <source>&amp;End Recording</source>
        <translation>T&amp;erminar Gravação</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2192"/>
        <source>&amp;Sync and Save</source>
        <translation>&amp;Sincronizar e Salvar</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2211"/>
        <source>&amp;Save and Finish</source>
        <translation>&amp;Salvar e Terminar</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2230"/>
        <source>&amp;Start</source>
        <translation>Ini&amp;ciar</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="188"/>
        <source>Scanning for compatible oximeters</source>
        <translation>Buscando por oxímetros compatíveis</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="220"/>
        <source>Could not detect any connected oximeter devices.</source>
        <translation>Nenhum oxímetro conectado foi detectado.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="228"/>
        <source>Connecting to %1 Oximeter</source>
        <translation>Conectando ao Oxímetro %1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="256"/>
        <source>Renaming this oximeter from &apos;%1&apos; to &apos;%2&apos;</source>
        <translation>Renomeando esse oxímetro de &apos;%1&apos; para &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="259"/>
        <source>Oximeter name is different.. If you only have one and are sharing it between profiles, set the name to the same on both profiles.</source>
        <translation>O nome do oxímetro é diferente.. Se você possui apenas um e está compartilhando-o entre perfis, defina o mesmo nome em todos os perfis.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="299"/>
        <source>&quot;%1&quot;, session %2</source>
        <translation>&quot;%1&quot;, sessão %2</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="321"/>
        <source>Nothing to import</source>
        <translation>Nada para importar</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="323"/>
        <source>Your oximeter did not have any valid sessions.</source>
        <translation>Seu oxímetro não tinha quaisquer sessões válidas.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="324"/>
        <source>Close</source>
        <translation>Fechar</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="327"/>
        <source>Waiting for %1 to start</source>
        <translation>Aguardando %1 para começar</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="328"/>
        <source>Waiting for the device to start the upload process...</source>
        <translation>Aguardando pelo dispositivo para iniciar o processo de envio...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="330"/>
        <source>Select upload option on %1</source>
        <translation>Selecione a opção upload em %1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="331"/>
        <source>You need to tell your oximeter to begin sending data to the computer.</source>
        <translation>Você precisa dizer ao oxímetro para começar a enviar dados para o computador.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="333"/>
        <source>Please connect your oximeter, enter it&apos;s menu and select upload to commence data transfer...</source>
        <translation>Por favor conecte seu oxímetro, entre no menu e selecione upload para começar a transferência de dados...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="364"/>
        <source>%1 device is uploading data...</source>
        <translation>Dispositivo %1 está enviando dados...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="365"/>
        <source>Please wait until oximeter upload process completes. Do not unplug your oximeter.</source>
        <translation>Por favor aguarde até o processo de envio de dados do oxímetro terminar. Não desconecte seu oxímetro.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="384"/>
        <source>Oximeter import completed..</source>
        <translation>Importação do oxímetro completada..</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="408"/>
        <source>Select a valid oximetry data file</source>
        <translation>Selecione um arquivo de dados válido de oxímetro</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="408"/>
        <source>Oximetry Files (*.spo *.spor *.spo2 *.SpO2 *.dat)</source>
        <translation>Arquivos de Oximetria (*.so *.spor *.spo2 *.SpO2 *.dat)</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="430"/>
        <source>No Oximetry module could parse the given file:</source>
        <translation>Nenhum módulo de oximetria pode interpretar o arquivo fornecido:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="475"/>
        <source>Live Oximetry Mode</source>
        <translation>Módo de Oximetria em Tempo Real</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="527"/>
        <source>Live Oximetry Stopped</source>
        <translation>Oximetria em Tempo Real Parada</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="528"/>
        <source>Live Oximetry import has been stopped</source>
        <translation>A importação de oximetria em tempo real foi parada</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1072"/>
        <source>Oximeter Session %1</source>
        <translation>Sessão de Oxímetro %1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1117"/>
        <source>OSCAR gives you the ability to track Oximetry data alongside CPAP session data, which can give valuable insight into the effectiveness of CPAP treatment. It will also work standalone with your Pulse Oximeter, allowing you to store, track and review your recorded data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1119"/>
        <source>OSCAR is currently compatible with Contec CMS50D+, CMS50E, CMS50F and CMS50I serial oximeters.&lt;br/&gt;(Note: Direct importing from bluetooth models is &lt;span style=&quot; font-weight:600;&quot;&gt;probably not&lt;/span&gt; possible yet)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1128"/>
        <source>If you are trying to sync oximetry and CPAP data, please make sure you imported your CPAP sessions first before proceeding!</source>
        <translation>Se você está tentando sincronizar dados de oximetria e CPAP, por favor certifique de que você importou sua sessão de CPAP antes de prosseguir!</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1131"/>
        <source>For OSCAR to be able to locate and read directly from your Oximeter device, you need to ensure the correct device drivers (eg. USB to Serial UART) have been installed on your computer. For more information about this, %1click here%2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="457"/>
        <source>Oximeter not detected</source>
        <translation>Oxímetro não detectado</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="464"/>
        <source>Couldn&apos;t access oximeter</source>
        <translation>Impossível acessar oxímetro</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="478"/>
        <source>Starting up...</source>
        <translation>Inicializando...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="479"/>
        <source>If you can still read this after a few seconds, cancel and try again</source>
        <translation>Se você ainda pode ler isso após alguns segundos, cancele e tente novamente</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="526"/>
        <source>Live Import Stopped</source>
        <translation>Importação em Tempo Real Parada</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="578"/>
        <source>%1 session(s) on %2, starting at %3</source>
        <translation>%1 sessão(ões( em %2, começando em %3</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="582"/>
        <source>No CPAP data available on %1</source>
        <translation>Nenhum dado de CPAP disponível em %1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="588"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="710"/>
        <source>Recording...</source>
        <translation>Gravando...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="717"/>
        <source>Finger not detected</source>
        <translation>Dedo não detectado</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="817"/>
        <source>I want to use the time my computer recorded for this live oximetry session.</source>
        <translation>Eu quero usar a hora que meu computador registrou para essa sessão de oximetria em tempo real.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="820"/>
        <source>I need to set the time manually, because my oximeter doesn&apos;t have an internal clock.</source>
        <translation>Eu preciso definir o tempo manualmente, porque meu oxímetro não possui relógio interno.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="832"/>
        <source>Something went wrong getting session data</source>
        <translation>Algo deu errado ao obter os dados da sessão</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1113"/>
        <source>Welcome to the Oximeter Import Wizard</source>
        <translation>Bem-vindo ao Assistente de Importação de Oxímetro</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1115"/>
        <source>Pulse Oximeters are medical devices used to measure blood oxygen saturation. During extended Apnea events and abnormal breathing patterns, blood oxygen saturation levels can drop significantly, and can indicate issues that need medical attention.</source>
        <translation>Oxímetros de pulso são dispositivos médicos usados para medir a saturação de oxigênio no sangue. Durante eventos extensos de apenai e padrões anormais de respiração, os níveis de saturação de oxigênio no sangue podem cair significativamente, e podem indicar problemas que precisam de atenção médica.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1121"/>
        <source>You may wish to note, other companies, such as Pulox, simply rebadge Contec CMS50&apos;s under new names, such as the Pulox PO-200, PO-300, PO-400. These should also work.</source>
        <translation>Você pode desejar notar, outras companhias, como a Pulox, simplesmente rotulam o Contec CMS50 sob nomes novos, como o Pulox PO-200, PO-300, PO-400. Estes devem funcionar também.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1124"/>
        <source>It also can read from ChoiceMMed MD300W1 oximeter .dat files.</source>
        <translation>Ele também pode ler dos arquivos .dat do ChoiceMMed MD300W1.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1126"/>
        <source>Please remember:</source>
        <translation>Por favor lembre:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1130"/>
        <source>Important Notes:</source>
        <translation>Notas importantes:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1133"/>
        <source>Contec CMS50D+ devices do not have an internal clock, and do not record a starting time. If you do not have a CPAP session to link a recording to, you will have to enter the start time manually after the import process is completed.</source>
        <translation>Aparelhos CMS50D+ não possuem relógio interno e não registram um tempo de inínio, se você não possui uma sessão de CPAP para sincronizar a gravação, você terá de especificar manualmente o tempo de início após completar o processo de importação.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1135"/>
        <source>Even for devices with an internal clock, it is still recommended to get into the habit of starting oximeter records at the same time as CPAP sessions, because CPAP internal clocks tend to drift over time, and not all can be reset easily.</source>
        <translation>Mesmo para aparelhos com um relógio interno, ainda é recomendado desenvolver o hábito de iniciar as gravações de oxímetro ao mesmo tempo que as sessões de CPAP, porque os relógios internos dos CPAPs tendem a desviar com o tempo, e nem todos podem ser redefinidos facilmente.</translation>
    </message>
</context>
<context>
    <name>Oximetry</name>
    <message>
        <location filename="../oscar/oximetry.ui" line="14"/>
        <source>Form</source>
        <translation>Forma</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="89"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="102"/>
        <source>d/MM/yy h:mm:ss AP</source>
        <translation>d/MM/yy HH:mm:ss</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="131"/>
        <source>R&amp;eset</source>
        <translation>R&amp;edefinir</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="160"/>
        <source>SpO2</source>
        <translation>SpO2</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="245"/>
        <source>Pulse</source>
        <translation>Pulso</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="346"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="366"/>
        <source>&amp;Open .spo/R File</source>
        <translation>Abrir arquivo .sp&amp;o/R</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="385"/>
        <source>Serial &amp;Import</source>
        <translation>&amp;Importação Serial</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="398"/>
        <source>&amp;Start Live</source>
        <translation>Ini&amp;ciar em Tempo Real</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="421"/>
        <source>Serial Port</source>
        <translation>Porta Serial</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="450"/>
        <source>&amp;Rescan Ports</source>
        <translation>&amp;Rebuscar Portas</translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="23"/>
        <source>Preferences</source>
        <translation>Preferências</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="64"/>
        <source>&amp;Import</source>
        <translation>&amp;Importar</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="154"/>
        <source>Combine Close Sessions </source>
        <translation>Combinar Sessões Fechadas </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="164"/>
        <location filename="../oscar/preferencesdialog.ui" line="249"/>
        <location filename="../oscar/preferencesdialog.ui" line="731"/>
        <source>Minutes</source>
        <translation>Minutos</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="184"/>
        <source>Multiple sessions closer together than this value will be kept on the same day.
</source>
        <translation>Múltiplas sessões mais próximas do que esse valor serão mantidas no mesmo dia.
</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="239"/>
        <source>Ignore Short Sessions</source>
        <translation>Ignorar Sessões Curtas</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="266"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Sessions shorter in duration than this will not be displayed&lt;span style=&quot; font-style:italic;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-style:italic;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Sessões mais curtas do que isso em duração não serão mostradas&lt;span style=&quot; font-style:italic;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-style:italic;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="310"/>
        <source>Day Split Time</source>
        <translation>Tempo de Divisão do Dia</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="320"/>
        <source>Sessions starting before this time will go to the previous calendar day.</source>
        <translation>Sessões iniciando antes dessa hora irão para o dia anterior no calendário.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="393"/>
        <source>Session Storage Options</source>
        <translation>Opções de Armazenamento de Sessões</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="482"/>
        <source>Create SD Card Backups during Import (Turn this off at your own peril!)</source>
        <translation>Criar backups do cartão SD durante a importação (desative isso por sua conta e risco!)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="439"/>
        <source>Compress SD Card Backups (slower first import, but makes backups smaller)</source>
        <translation>Comprimir os Backups do Cartão SD (primeira importação mais lenta, mas os backups ficam mais leves)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="417"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Changing SD Backup compression options doesn&apos;t automatically recompress backup data.  &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Alterar as configurações de compressão de SD não recomprime automaticamente os dados de backup.  &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="637"/>
        <source>&amp;CPAP</source>
        <translation>&amp;CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1267"/>
        <source>Regard days with under this usage as &quot;incompliant&quot;. 4 hours is usually considered compliant.</source>
        <translation>Considere dias com uso abaixo disso &quot;inobservante&quot; 4 horas é geralmente considerado observante.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1270"/>
        <source> hours</source>
        <translation> horas</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="945"/>
        <source>Enable/disable experimental event flagging enhancements. 
It allows detecting borderline events, and some the machine missed.
This option must be enabled before import, otherwise a purge is required.</source>
        <translation>Ativar/desativar melhorias experimentais na marcação de eventos.
Elas permitem detectar eventos limítrofes, e alguns que a máquina deixa passar.
Essa opção deve ser ativada antes da importação, do contrário uma limpeza é necessária.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1023"/>
        <source>Flow Restriction</source>
        <translation>Restrição de Fluxo</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1064"/>
        <source>Percentage of restriction in airflow from the median value. 
A value of 20% works well for detecting apneas. </source>
        <translation>Porcentagem da restrição em fluxo de ar do valor médio.
Um valor de 20% funciona bem para detectar apneias. </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="987"/>
        <location filename="../oscar/preferencesdialog.ui" line="1068"/>
        <location filename="../oscar/preferencesdialog.ui" line="1545"/>
        <location filename="../oscar/preferencesdialog.ui" line="1705"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1041"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:italic;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Custom flagging is an experimental method of detecting events missed by the machine. They are &lt;span style=&quot; text-decoration: underline;&quot;&gt;not&lt;/span&gt; included in AHI.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:italic;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;As marcações personalizadas são um método experimental de detecção de eventos que a máquina deixa passar. Eles &lt;span style=&quot; text-decoration: underline;&quot;&gt;não&lt;/span&gt; são incluídos no AHI.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1085"/>
        <source>Duration of airflow restriction</source>
        <translation>Duração da restrição de fluxo de ar</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="977"/>
        <location filename="../oscar/preferencesdialog.ui" line="1088"/>
        <location filename="../oscar/preferencesdialog.ui" line="1562"/>
        <location filename="../oscar/preferencesdialog.ui" line="1650"/>
        <location filename="../oscar/preferencesdialog.ui" line="1679"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1124"/>
        <source>Event Duration</source>
        <translation>Duração de Evento</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1111"/>
        <source>Allow duplicates near machine events.</source>
        <translation>Permitir duplicados próximos de eventos da máquina.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1189"/>
        <source>Adjusts the amount of data considered for each point in the AHI/Hour graph.
Defaults to 60 minutes.. Highly recommend it&apos;s left at this value.</source>
        <translation>Ajuste a quantidade de dados considerada para cada ponto no gráfico AHI/Hora.
Padrão em 60 minutos.. Altamente recomendado manter nesse valor.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1193"/>
        <source> minutes</source>
        <translation> minutos</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1232"/>
        <source>Reset the counter to zero at beginning of each (time) window.</source>
        <translation>Redefinir o contador a zero no começo de cada janela (de tempo).</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1235"/>
        <source>Zero Reset</source>
        <translation>Redefinir a Zero</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="684"/>
        <source>CPAP Clock Drift</source>
        <translation>Deslocamento do Relógio do CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="500"/>
        <source>Do not import sessions older than:</source>
        <translation>Não importar sessões mais antigas que:</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="507"/>
        <source>Sessions older than this date will not be imported</source>
        <translation>Sessões mais antigas que essa data não serão importadas</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="533"/>
        <source>dd MMMM yyyy</source>
        <translation>dd MMMM yyyy</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1078"/>
        <source>Show in Event Breakdown Piechart</source>
        <translation>Mostrar no Gráfico Torta de Discriminação de Evento</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1101"/>
        <source>#1</source>
        <translation>#1</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1007"/>
        <source>#2</source>
        <translation>#2</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1000"/>
        <source>Resync Machine Detected Events (Experimental)</source>
        <translation>Resincronizar Eventos Detectados pela Máquina (Experimental)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1248"/>
        <source>User definable threshold considered large leak</source>
        <translation>Limitar definível pelo utilizador considerando vazamento grande</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1251"/>
        <source> L/min</source>
        <translation> L/min</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1215"/>
        <source>Whether to show the leak redline in the leak graph</source>
        <translation>Mostrar ou não a linha vermelha de vazamento no gráfico de vazamentos</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1792"/>
        <location filename="../oscar/preferencesdialog.ui" line="1864"/>
        <source>Search</source>
        <translation>Buscar</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1488"/>
        <source>&amp;Oximetry</source>
        <translation>&amp;Oximetria</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1640"/>
        <source>SPO2</source>
        <translation>SPO2</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1702"/>
        <source>Percentage drop in oxygen saturation</source>
        <translation>Queda de porcentagem na saturação de oxigênio</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1695"/>
        <source>Pulse</source>
        <translation>Pulso</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1660"/>
        <source>Sudden change in Pulse Rate of at least this amount</source>
        <translation>Mudança brusca na Taxa de Pulso de pelo menos essa intensidade</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1552"/>
        <location filename="../oscar/preferencesdialog.ui" line="1582"/>
        <location filename="../oscar/preferencesdialog.ui" line="1663"/>
        <source> bpm</source>
        <translation> bpm</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1647"/>
        <source>Minimum duration of drop in oxygen saturation</source>
        <translation>Duração mínima da queda na saturação de oxigênio</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1676"/>
        <source>Minimum duration of pulse change event.</source>
        <translation>Duração mínima do evento de mudança no pulso.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1559"/>
        <source>Small chunks of oximetry data under this amount will be discarded.</source>
        <translation>Pequenos pedaços dos dados de oximetria abaixo desse valor serão descartados.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1920"/>
        <source>&amp;General</source>
        <translation>&amp;Geral</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1949"/>
        <source>General Settings</source>
        <translation>Configurações Gerais</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2690"/>
        <source>Daily view navigation buttons will skip over days without data records</source>
        <translation>Botões de navegação na visão diária irão pular dias sem dados registrados</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2693"/>
        <source>Skip over Empty Days</source>
        <translation>Pular sobre Dias Vazios</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1970"/>
        <source>Allow use of multiple CPU cores where available to improve performance. 
Mainly affects the importer.</source>
        <translation>Permitir múltiplos núcleos de CPU quando disponíveis para melhorar desempenho.
Afeta principalmente a importação.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1974"/>
        <source>Enable Multithreading</source>
        <translation>Ativar Multithreading</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="573"/>
        <source>Bypass the login screen and load the most recent User Profile</source>
        <translation>Ignorar a tela de login e carregar o perfil de utilizador mais recente</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2680"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;These features have recently been pruned. They will come back later. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Esses recursos foram retirados recentemente. Eles retornarão no futuro. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1315"/>
        <source>Changes to the following settings needs a restart, but not a recalc.</source>
        <translation>Mudanças nas configurações seguintes exige um reinício, mas não uma recalculação.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1318"/>
        <source>Preferred Calculation Methods</source>
        <translation>Métodos de Cálculo Preferidos</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1347"/>
        <source>Middle Calculations</source>
        <translation>Calculações de Meio</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1361"/>
        <source>Upper Percentile</source>
        <translation>Percentil Superior</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1324"/>
        <source>For consistancy, ResMed users should use 95% here,
as this is the only value available on summary-only days.</source>
        <translation>Para consistência, utilizadores ResMed devem usar 95% aqui,
já que esse é o único valor disponível nos dias de apenas resumo.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1375"/>
        <source>Median is recommended for ResMed users.</source>
        <translation>Mediana é recomendada para utilizadores ResMed.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1379"/>
        <location filename="../oscar/preferencesdialog.ui" line="1442"/>
        <source>Median</source>
        <translation>Mediana</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1384"/>
        <source>Weighted Average</source>
        <translation>Média Ponderada</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1389"/>
        <source>Normal Average</source>
        <translation>Média Normal</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1413"/>
        <source>True Maximum</source>
        <translation>Máximo Verdadeiro</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1418"/>
        <source>99% Percentile</source>
        <translation>Percil de 99%</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1354"/>
        <source>Maximum Calcs</source>
        <translation>Cálcs Máximos</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="91"/>
        <source>Session Splitting Settings</source>
        <translation>Configurações de Separação de Sessão</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="359"/>
        <source>Don&apos;t Split Summary Days (Warning: read the tooltip!)</source>
        <translation>Não Separar Dias Resumidos (Aviso: leia a dica de contexto!)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="567"/>
        <source>Memory and Startup Options</source>
        <translation>Opções de Memória e Inicialização</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="609"/>
        <source>Pre-Load all summary data at startup</source>
        <translation>Pré-carregar todos os dados resumidos no início</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="596"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This setting keeps waveform and event data in memory after use to speed up revisiting days.&lt;/p&gt;&lt;p&gt;This is not really a necessary option, as your operating system caches previously used files too.&lt;/p&gt;&lt;p&gt;Recommendation is to leave it switched off, unless your computer has a ton of memory.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Essa opção mantem os dados de forma de onda e eventos na memória após o uso para acelerar a revisitação de dias.&lt;/p&gt;&lt;p&gt;Essa não é uma opção realmente necessária, já que seu sistema operativo retém arquivos previamente utilizados.&lt;/p&gt;&lt;p&gt;A recomendação é deixá-la desativada, a menos que seu computador possua muita memória livre.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="599"/>
        <source>Keep Waveform/Event data in memory</source>
        <translation>Manter dados de forma de onda/eventos na memória</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="623"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Cuts down on any unimportant confirmation dialogs during import.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Reduzir quaisquer diálogos de confirmação sem importância durante a importação.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="626"/>
        <source>Import without asking for confirmation</source>
        <translation>Importar sem pedir confirmação</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1153"/>
        <source>General CPAP and Related Settings</source>
        <translation>Configurações Gerais e Relacionadas ao CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1162"/>
        <source>Enable Unknown Events Channels</source>
        <translation>Ativar Canais de Evento Desconhecidos</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1293"/>
        <source>AHI</source>
        <extracomment>Apnea Hypopnea Index</extracomment>
        <translatorcomment>Índice de Apneia-Hipoapneia</translatorcomment>
        <translation>IAH</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1298"/>
        <source>RDI</source>
        <extracomment>Respiratory Disturbance Index</extracomment>
        <translatorcomment>Índice de Distúrbio Respiratório</translatorcomment>
        <translation>IDR</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1169"/>
        <source>AHI/Hour Graph Time Window</source>
        <translation>Janela de Tempo do Gráfico IAH/Hora</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1225"/>
        <source>Preferred major event index</source>
        <translation>Índice preferido de evento principal</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1176"/>
        <source>Compliance defined as</source>
        <translation>Observância definida como</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1218"/>
        <source>Flag leaks over threshold</source>
        <translation>Marcar vazamentos acima do limite</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="765"/>
        <source>Seconds</source>
        <translation>Segundos</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="711"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Note: This is not intended for timezone corrections! Make sure your operating system clock and timezone is set correctly.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Nota: Isso não é pretendido para correções de fuso-horário! Certifique-se de que o fuso-horário e o relógio do seu sistema estão configurados corretamente.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="758"/>
        <source>Hours</source>
        <translation>Horas</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1409"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;True maximum is the maximum of the data set.&lt;/p&gt;&lt;p&gt;99th percentile filters out the rarest outliers.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Máximo verdadeiro é o máximo do conjunto de dados.&lt;/p&gt;&lt;p&gt;99º percentil filtra os mais raros distanciamentos.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1427"/>
        <source>Combined Count divided by Total Hours</source>
        <translation>Contagem combinada divididade pelas horas totais</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1432"/>
        <source>Time Weighted average of Indice</source>
        <translation>A Média Ponderada pelo tempo do Índice</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1437"/>
        <source>Standard average of indice</source>
        <translation>Média padrão do índice</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1368"/>
        <source>Culminative Indices</source>
        <translation>Índices Culminantes</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="431"/>
        <source>Compress ResMed (EDF) backups to save disk space.
Backed up EDF files are stored in the .gz format, 
which is common on Mac &amp; Linux platforms.. 

OSCAR can import from this compressed backup directory natively.. 
To use it with ResScan will require the .gz files to be uncompressed first..</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="451"/>
        <source>The following options affect the amount of disk space OSCAR uses, and have an effect on how long import takes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="461"/>
        <source>This makes OSCAR&apos;s data take around half as much space.
But it makes import and day changing take longer.. 
If you&apos;ve got a new computer with a small solid state disk, this is a good option.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="466"/>
        <source>Compress Session Data (makes OSCAR data smaller, but day changing slower.)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="473"/>
        <source>This maintains a backup of SD-card data for ResMed machines, 

ResMed S9 series machines delete high resolution data older than 7 days, 
and graph data older than 30 days..

OSCAR can keep a copy of this data if you ever need to reinstall. 
(Highly recomended, unless your short on disk space or don&apos;t care about the graph data)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="606"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Makes starting OSCAR a bit slower, by pre-loading all the summary data in advance, which speeds up overview browsing and a few other calculations later on. &lt;/p&gt;&lt;p&gt;If you have a large amount of data, it might be worth keeping this switched off, but if you typically like to view &lt;span style=&quot; font-style:italic;&quot;&gt;everything&lt;/span&gt; in overview, all the summary data still has to be loaded anyway. &lt;/p&gt;&lt;p&gt;Note this setting doesn&apos;t affect waveform and event data, which is always demand loaded as needed.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="950"/>
        <source>Custom CPAP User Event Flagging</source>
        <translation>Marcação de Evento Personalizada pelo Utilizador de CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="997"/>
        <source>This experimental option attempts to use OSCAR&apos;s event flagging system to improve machine detected event positioning.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1749"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Syncing Oximetry and CPAP Data&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;CMS50 data imported from SpO2Review (from .spoR files) or the serial import method does &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600; text-decoration: underline;&quot;&gt;not&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt; have the correct timestamp needed to sync.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Live view mode (using a serial cable) is one way to acheive an accurate sync on CMS50 oximeters, but does not counter for CPAP clock drift.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;If you start your Oximeters recording mode at &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-style:italic;&quot;&gt;exactly &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;the same time you start your CPAP machine, you can now also achieve sync. &lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;The serial import process takes the starting time from last nights first CPAP session. (Remember to import your CPAP data first!)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1981"/>
        <source>Show Remove Card reminder notification on OSCAR shutdown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2013"/>
        <source>Automatically Check For Updates</source>
        <translation>Procurar Automaticamente Atualizações</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2030"/>
        <source>Check for new version every</source>
        <translation>Buscar uma nova versão a cada</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2037"/>
        <source>Sourceforge hosts this project for free.. Please be considerate of their resources..</source>
        <translation>SourceForge hospeda esse projeto gratuitamente.. Por favor antecioso com o recurso deles..</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2053"/>
        <source>days.</source>
        <translation>dias.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2118"/>
        <source>&amp;Check for Updates now</source>
        <translation>&amp;Bus&amp;car Atualizações Agora</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2141"/>
        <source>Last Checked For Updates: </source>
        <translation>Última Busca Por Atualizações: </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2154"/>
        <source>TextLabel</source>
        <translation>Rótulo</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2176"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If your interested in helping test new features and bugfixes early, click here.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;But please be warned this will sometimes mean breaky code..&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Se você está interessado em ajudar a testar novos recursos e correções de erros antecipadamente, clique aqui.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Mas por favor, esteja avisado que isso às vezes significa código com erros..&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2185"/>
        <source>I want to try experimental and test builds (Advanced users only please.)</source>
        <translation>Eu quero tentar compilações experimentais e de teste (apenas utilizadores avançados por favor.)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2209"/>
        <source>&amp;Appearance</source>
        <translation>&amp;Aparência</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2238"/>
        <source>Graph Settings</source>
        <translation>Configurações de Gráfico</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2254"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Which tab to open on loading a profile. (Note: It will default to Profile if OSCAR is set to not open a profile on startup)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2512"/>
        <source>Bar Tops</source>
        <translation>Topo de Barra</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2517"/>
        <source>Line Chart</source>
        <translation>Gráficos de Linha</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2607"/>
        <source>Overview Linecharts</source>
        <translation>Visão-Geral de Gráficos de Linha</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2552"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This makes scrolling when zoomed in easier on sensitive bidirectional TouchPads&lt;/p&gt;&lt;p&gt;50ms is recommended value.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Isso facilita a rolagem quando o zoom é mais fácil em TouchPads bidirecionais sensíveis.&lt;/p&gt;&lt;p&gt;50 ms é o valor recomendado.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2434"/>
        <source>Scroll Dampening</source>
        <translation>Amortecimento de rolagem</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2354"/>
        <source>Overlay Flags</source>
        <translation>Marcações Sobrepostas</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2364"/>
        <source>Line Thickness</source>
        <translation>Espessura de Linha</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2400"/>
        <source>The pixel thickness of line plots</source>
        <translation>Espessura em pixels dos traços de linha</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2670"/>
        <source>Pixmap caching is an graphics acceleration technique. May cause problems with font drawing in graph display area on your platform.</source>
        <translation>Cache de pixmap é uma técnica de aceleração gráfica. Pode causar problemas no desenho de fontes na área de mostragem de gráficos na sua plataforma.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2732"/>
        <source>Try changing this from the default setting (Desktop OpenGL) if you experience rendering problems with OSCAR&apos;s graphs.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2757"/>
        <source>Fonts (Application wide settings)</source>
        <translation>Fontes (afeta o aplicativo todo)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2380"/>
        <source>The visual method of displaying waveform overlay flags.
</source>
        <translation>O método visual de mostrar marcações de sobreposição de formas de onda.
</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2385"/>
        <source>Standard Bars</source>
        <translation>Barras Padrão</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2338"/>
        <source>Graph Height</source>
        <translation>Altura do Gráfico</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2531"/>
        <source>Default display height of graphs in pixels</source>
        <translation>Altura de exibição, em pixels, padrão dos gráficos</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2446"/>
        <source>How long you want the tooltips to stay visible.</source>
        <translation>Por quanto tempo você deseja que as dicas de contexto permaneçam visíveis.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1769"/>
        <source>Events</source>
        <translation>Eventos</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1815"/>
        <location filename="../oscar/preferencesdialog.ui" line="1894"/>
        <source>Reset &amp;Defaults</source>
        <translation>Redefinir Pa&amp;drões</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1828"/>
        <location filename="../oscar/preferencesdialog.ui" line="1907"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warning: &lt;/span&gt;Just because you can, does not mean it&apos;s good practice.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Aviso: &lt;/span&gt;Só porque você pode, não significa que é uma boa prática.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1841"/>
        <source>Waveforms</source>
        <translation>Formas de Onda</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1628"/>
        <source>Flag rapid changes in oximetry stats</source>
        <translation>Marcar mudanças bruscas nos números de oximetria</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1539"/>
        <source>Other oximetry options</source>
        <translation>Outras opções de oximetria</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1589"/>
        <source>Flag SPO2 Desaturations Below</source>
        <translation>Marcar Dessaturações SPO2 Abaixo de</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1572"/>
        <source>Discard segments under</source>
        <translation>Descartar segmentos abaixo de</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1609"/>
        <source>Flag Pulse Rate Above</source>
        <translation>Marcar Taxa de Pulso Acima de</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1599"/>
        <source>Flag Pulse Rate Below</source>
        <translation>Marcar Taxa de Pulso Abaixo de</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="356"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;This setting should be used with caution...&lt;/span&gt; Switching it off comes with consequences involving accuracy of summary only days, as certain calculations only work properly provided summary only sessions that came from individual day records are kept together. &lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;ResMed users:&lt;/span&gt; Just because it seems natural to you and I that the 12 noon session restart should be in the previous day, does not mean ResMed&apos;s data agrees with us. The STF.edf summary index format has serious weaknesses that make doing this not a good idea.&lt;/p&gt;&lt;p&gt;This option exists to pacify those who don&apos;t care and want to see this &amp;quot;fixed&amp;quot; no matter the costs, but know it comes with a cost. If you keep your SD card in every night, and import at least once a week, you won&apos;t see problems with this very often.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Essa configuração deve ser usada com cautela...&lt;/span&gt; Desativá-la traz consequências envolvendo a precisão de dias resumo-apenas, pois certos cálculos só funcionam corretamente se sessões de resumo provenientes de registros de dias individuais forem mantidas juntas. &lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Utilizadores ResMed:&lt;/span&gt; Apenas porque parece natural para você e eu que a sessão reiniciada ao meio-dia deve estar no dia anterior, não significa que os dados da ResMed concordam conosco. O formato de índice de resumo STF.edf tem sérios pontos fracos que fazem com que isso não seja uma boa idéia.&lt;/p&gt;&lt;p&gt;Esta opção existe para apaziguar aqueles que não se importam e querem ver isso &amp;quot;corrigido&amp;quot; não importando os custos, mas saiba que isso tem um custo. Se você mantiver seu cartão SD inserido todas as noites, e importar pelo menos uma vez por semana, você não verá problemas com isso com grande frequência..&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="781"/>
        <source>This calculation requires Total Leaks data to be provided by the CPAP machine. (Eg, PRS1, but not ResMed, which has these already)

The Unintentional Leak calculations used here are linear, they don&apos;t model the mask vent curve.

If you use a few different masks, pick average values instead. It should still be close enough.</source>
        <translation>Este cálculo requer dados de vazamentos totais a serem fornecidos pela máquina de CPAP. (Por exemplo, PRS1, mas não a ResMed, que já tem isso)

Os cálculos de vazamentos não intencionais usados aqui são lineares, eles não modelam a curva de ventilação da máscara.

Se você usar algumas máscaras diferentes, escolha valores médios. E deve ficar próximo o suficiente.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="788"/>
        <source>Calculate Unintentional Leaks When Not Present</source>
        <translation>Calcular Vazamentos Não Intencionais Quando Não Presente</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="797"/>
        <source>Your masks vent rate at 20cmH2O pressure</source>
        <translation>Suas máscaras ventilam na taxa de pressão 20cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="853"/>
        <source>Your masks vent rate at 4cmH2O pressure</source>
        <translation>Suas máscaras ventilam na taxa de pressão 4cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="884"/>
        <source>4 cmH2O</source>
        <translation>4 cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="894"/>
        <source>20 cmH2O</source>
        <translation>20 cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="926"/>
        <source>Note: A linear calculation method is used. Changing these values requires a recalculation.</source>
        <translation>Nota: Um método de cálculo linear é usado. Alterar esses valores requer um recálculo.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1159"/>
        <source>Show flags for machine detected events that haven&apos;t been identified yet.</source>
        <translation>Mostrar marcações para eventos detectados por máquina que ainda não foram identificados.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2424"/>
        <source>Tooltip Timeout</source>
        <translation>Tempo Limite de Dica de Contexto</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2504"/>
        <source>Graph Tooltips</source>
        <translation>Dicas de Contexto de Gráfico</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2390"/>
        <source>Top Markers</source>
        <translation>Marcadores de Topo</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="576"/>
        <source>Auto-Launch CPAP Importer after opening profile</source>
        <translation>Iniciar o importador de CPAP automaticamente depois de abrir o perfil</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="616"/>
        <source>Automatically load last used profile on start-up</source>
        <translation>Carregar automaticamente o perfil usado por último ao abrir</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1456"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Note: &lt;/span&gt;Due to summary design limitations, ResMed machines do not support changing these settings.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Note: &lt;/span&gt;Devido a limitações no projeto dos resumos, as máquinas da ResMed não suportam a alteração dessas opções.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1509"/>
        <source>Oximetry Settings</source>
        <translation>Opções de oximetria</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2244"/>
        <source>On Opening</source>
        <translation>Ao Abrir</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2257"/>
        <location filename="../oscar/preferencesdialog.ui" line="2261"/>
        <source>Profile</source>
        <translation>Perfil</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2266"/>
        <location filename="../oscar/preferencesdialog.ui" line="2305"/>
        <source>Welcome</source>
        <translation>Bem-vindo</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2271"/>
        <location filename="../oscar/preferencesdialog.ui" line="2310"/>
        <source>Daily</source>
        <translation>Diariamente</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2281"/>
        <location filename="../oscar/preferencesdialog.ui" line="2320"/>
        <source>Statistics</source>
        <translation>Estatísticas</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2289"/>
        <source>Switch Tabs</source>
        <translation>Alternar Abas</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2300"/>
        <source>No change</source>
        <translation>Nenhuma mudança</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2328"/>
        <source>After Import</source>
        <translation>Após Importação</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2626"/>
        <source>Other Visual Settings</source>
        <translation>Outras Opções Visuais</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2632"/>
        <source>Anti-Aliasing applies smoothing to graph plots.. 
Certain plots look more attractive with this on. 
This also affects printed reports.

Try it and see if you like it.</source>
        <translation>O anti-aliasing aplica suavização aos desenhos do gráfico.
Certas parcelas parecem mais atraentes com isso.
Isso também afeta os relatórios impressos.

Experimente e veja se você gosta.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2639"/>
        <source>Use Anti-Aliasing</source>
        <translation>Usar Anti-Aliasing</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2646"/>
        <source>Makes certain plots look more &quot;square waved&quot;.</source>
        <translation>Faz com que certos gráficos pareçam mais &quot;quadrados ondulados&quot;.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2649"/>
        <source>Square Wave Plots</source>
        <translation>Gráficos de Onda Quadrada</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2656"/>
        <source>Allows graphs to be &quot;screenshotted&quot; for display purposes.
The Event Breakdown PIE chart uses this method, as does
the printing code.
Unfortunately some older computers/versions of Qt can cause
this application to be unstable with this feature enabled.</source>
        <translation>Permite que os gráficos sejam &quot;capturados em tela&quot; para fins de demonstração.
O gráfico torta de Discriminação de Eventos usa esse método, assim
como o código de impressão.
Infelizmente alguns computadores / versões mais antigas do Qt podem causar
instabilidade no aplicativo com esse recurso ativado.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2663"/>
        <source>Show event breakdown pie chart</source>
        <translation>Mostrar gráfico de torta da lista de eventos discriminados</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2673"/>
        <source>Use Pixmap Caching</source>
        <translation>Usar Cache de Pixmap</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2683"/>
        <source>Animations &amp;&amp; Fancy Stuff</source>
        <translation>Animações e Coisas Chiques</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2700"/>
        <source>Whether to allow changing yAxis scales by double clicking on yAxis labels</source>
        <translation>Permitir ou não a alteração das escalas do EixoY clicando duas vezes nos rótulos do EixoY</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2703"/>
        <source>Allow YAxis Scaling</source>
        <translation>Permitir Escala do EixoY</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2726"/>
        <source>Graphics Engine (Requires Restart)</source>
        <translation>Motor Gráfico (Exige Reinício)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2792"/>
        <source>Font</source>
        <translation>Fonte</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2811"/>
        <source>Size</source>
        <translation>Tamanho</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2830"/>
        <source>Bold  </source>
        <translation>Negrito  </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2852"/>
        <source>Italic</source>
        <translation>Itálico</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2865"/>
        <source>Application</source>
        <translation>Aplicativo</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2929"/>
        <source>Graph Text</source>
        <translation>Texto do Gráfico</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2990"/>
        <source>Graph Titles</source>
        <translation>Títulos do Gráfico</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3051"/>
        <source>Big  Text</source>
        <translation>Texto  Grande</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3118"/>
        <location filename="../oscar/preferencesdialog.cpp" line="428"/>
        <location filename="../oscar/preferencesdialog.cpp" line="560"/>
        <source>Details</source>
        <translation>Detalhes</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3162"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3169"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="423"/>
        <location filename="../oscar/preferencesdialog.cpp" line="554"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="424"/>
        <location filename="../oscar/preferencesdialog.cpp" line="555"/>
        <source>Color</source>
        <translation>Cor</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="426"/>
        <source>Flag Type</source>
        <translation>Tipo de Marcação</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="427"/>
        <location filename="../oscar/preferencesdialog.cpp" line="559"/>
        <source>Label</source>
        <translation>Rótulo</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="444"/>
        <source>CPAP Events</source>
        <translation>Eventos CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="445"/>
        <source>Oximeter Events</source>
        <translation>Eventos Oxímetro</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="446"/>
        <source>Positional Events</source>
        <translation>Eventos Posicionais</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="447"/>
        <source>Sleep Stage Events</source>
        <translation>Eventos de Estágio do Sono</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="448"/>
        <source>Unknown Events</source>
        <translation>Eventos Desconhecidos</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="620"/>
        <source>Double click to change the descriptive name this channel.</source>
        <translation>Clique duas vezes para alterar o nome descritivo desse canal.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="498"/>
        <location filename="../oscar/preferencesdialog.cpp" line="627"/>
        <source>Double click to change the default color for this channel plot/flag/data.</source>
        <translation>Clique duas vezes para alterar a cor padrão para esse canal/desenho/marcação/dado.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="64"/>
        <source>&lt;p&gt;&lt;b&gt;Please Note:&lt;/b&gt; OSCAR&apos;s advanced session splitting capabilities are not possible with &lt;b&gt;ResMed&lt;/b&gt; machines due to a limitation in the way their settings and summary data is stored, and therefore they have been disabled for this profile.&lt;/p&gt;&lt;p&gt;On ResMed machines, days will &lt;b&gt;split at noon&lt;/b&gt; like in ResMed&apos;s commercial software.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="283"/>
        <location filename="../oscar/preferencesdialog.cpp" line="284"/>
        <location filename="../oscar/preferencesdialog.cpp" line="1250"/>
        <location filename="../oscar/preferencesdialog.cpp" line="1255"/>
        <source>%1 %2</source>
        <translation>%1 %2</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2276"/>
        <location filename="../oscar/preferencesdialog.ui" line="2315"/>
        <location filename="../oscar/preferencesdialog.cpp" line="425"/>
        <location filename="../oscar/preferencesdialog.cpp" line="556"/>
        <source>Overview</source>
        <translation>Visão-Geral</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="490"/>
        <source>Double click to change the descriptive name the &apos;%1&apos; channel.</source>
        <translation>Clique duass vezes para mudar o nome descritivo do canal &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="503"/>
        <source>Whether this flag has a dedicated overview chart.</source>
        <translation>Se esse sinalizador possui um gráfico de visão geral dedicado ou não.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="513"/>
        <source>Here you can change the type of flag shown for this event</source>
        <translation>Aqui você pode alterar o tipo de marcação mostrada para este evento</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="518"/>
        <location filename="../oscar/preferencesdialog.cpp" line="651"/>
        <source>This is the short-form label to indicate this channel on screen.</source>
        <translation>Este é o rótulo de forma curta para indicar este canal na tela.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="524"/>
        <location filename="../oscar/preferencesdialog.cpp" line="657"/>
        <source>This is a description of what this channel does.</source>
        <translation>Esta é uma descrição do que esse canal faz.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="557"/>
        <source>Lower</source>
        <translation>Inferior</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="558"/>
        <source>Upper</source>
        <translation>Superior</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="577"/>
        <source>CPAP Waveforms</source>
        <translation>Formas de Onda de CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="578"/>
        <source>Oximeter Waveforms</source>
        <translation>Formas de Onda de Oxímetro</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="579"/>
        <source>Positional Waveforms</source>
        <translation>Formas de Onda Posicionais</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="580"/>
        <source>Sleep Stage Waveforms</source>
        <translation>Formas de Onda de Estágio de Sono</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="636"/>
        <source>Whether a breakdown of this waveform displays in overview.</source>
        <translation>Se uma discriminação dessa forma de onda é exibida na visão geral ou não.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="641"/>
        <source>Here you can set the &lt;b&gt;lower&lt;/b&gt; threshold used for certain calculations on the %1 waveform</source>
        <translation>Aqui você pode definir o limite &lt;b&gt;inferior&lt;/b&gt; usado para determinados cálculos na forma de onda %1</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="646"/>
        <source>Here you can set the &lt;b&gt;upper&lt;/b&gt; threshold used for certain calculations on the %1 waveform</source>
        <translation>Aqui você pode definir o limite &lt;b&gt;superior&lt;/b&gt; usado para determinados cálculos na forma de onda %1</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="756"/>
        <source>Data Processing Required</source>
        <translation>Processamento de Dados Requerido</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="757"/>
        <source>A data re/decompression proceedure is required to apply these changes. This operation may take a couple of minutes to complete.

Are you sure you want to make these changes?</source>
        <translation>Um procedimento de recuperação de dados / descompactação é necessário para aplicar essas alterações. Essa operação pode levar alguns minutos para ser concluída.

Tem certeza de que deseja fazer essas alterações?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="765"/>
        <source>Data Reindex Required</source>
        <translation>Reordenação de Dados de Índice Requerida</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="766"/>
        <source>A data reindexing proceedure is required to apply these changes. This operation may take a couple of minutes to complete.

Are you sure you want to make these changes?</source>
        <translation>Um procedimento de reindexação de dados é necessário para aplicar essas alterações. Essa operação pode levar alguns minutos para ser concluída.

Tem certeza de que deseja fazer essas alterações?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="772"/>
        <source>Restart Required</source>
        <translation>Reinício Requerido</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="773"/>
        <source>One or more of the changes you have made will require this application to be restarted, in order for these changes to come into effect.

Would you like do this now?</source>
        <translation>Uma ou mais das alterações feitas exigirão que esse aplicativo seja reiniciado para que essas alterações entrem em vigor.

Você gostaria de fazer isso agora?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1132"/>
        <source> If you ever need to reimport this data again (whether in OSCAR or ResScan) this data won&apos;t come back.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1133"/>
        <source> If you need to conserve disk space, please remember to carry out manual backups.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1134"/>
        <source> Are you sure you want to disable these backups?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1178"/>
        <source>Switching off backups is not a good idea, because OSCAR needs these to rebuild the database if errors are found.

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1179"/>
        <source>Are you really sure you want to do this?</source>
        <translation>Tem certeza mesmo de que deseja fazer isso?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="48"/>
        <source>Flag</source>
        <translation>Marcação</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="49"/>
        <source>Minor Flag</source>
        <translation>Pequena Marcação</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="50"/>
        <source>Span</source>
        <translation>Período</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="51"/>
        <source>Always Minor</source>
        <translation>Sempre Pequeno</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="256"/>
        <source>Never</source>
        <translation>Nunca</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1130"/>
        <source>This may not be a good idea</source>
        <translation>Isso pode não ser uma boa ideia</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1131"/>
        <source>ResMed S9 machines routinely delete certain data from your SD card older than 7 and 30 days (depending on resolution).</source>
        <translation>As máquinas ResMed S9 apagam rotineiramente certos dados do seu cartão SD com mais de 7 e 30 dias (dependendo da resolução).</translation>
    </message>
</context>
<context>
    <name>ProfileSelect</name>
    <message>
        <location filename="../oscar/profileselect.ui" line="14"/>
        <source>Select Profile</source>
        <translation>Selecione o Perfil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="115"/>
        <source>Search:</source>
        <translation>Busca:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="191"/>
        <source>Start with the selected user profile.</source>
        <translation>Comece com o perfil de utilizador selecionado.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="215"/>
        <source>Create a new user profile.</source>
        <translation>Criar um novo perfil de utilizador.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="235"/>
        <source>Choose a different OSCAR data folder.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="274"/>
        <source>OSCAR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="310"/>
        <source>Click here if you didn&apos;t want to start OSCAR.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="383"/>
        <source>The current location of OSCAR data store.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="241"/>
        <source>&amp;Different Folder</source>
        <translation>Pasta &amp;Diferente</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="287"/>
        <source>[version]</source>
        <translation>[versão]</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="313"/>
        <source>&amp;Quit</source>
        <translation>&amp;Sair</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="370"/>
        <source>Folder:</source>
        <translation>Pasta:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="389"/>
        <source>[data directory]</source>
        <translation>[pasta de dados]</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="218"/>
        <source>New Profile</source>
        <translation>Novo Perfil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="194"/>
        <source>&amp;Select User</source>
        <translation>S&amp;elecionar Utilizador</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="92"/>
        <source>Open Profile</source>
        <translation>Abrir Perfil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="93"/>
        <source>Edit Profile</source>
        <translation>Editar Perfil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="95"/>
        <source>Delete Profile</source>
        <translation>Deletar Perfil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="139"/>
        <location filename="../oscar/profileselect.cpp" line="220"/>
        <source>Enter Password for %1</source>
        <translation>Digitar senha para %1</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="158"/>
        <location filename="../oscar/profileselect.cpp" line="342"/>
        <source>Incorrect Password</source>
        <translation>Senha incorreta</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="160"/>
        <source>You entered the password wrong too many times.</source>
        <translation>Você especificou a senha incorreta vezes demais.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="182"/>
        <source>Enter the word DELETE below to confirm.</source>
        <translation>Digite a palavra DELETE abaixo para confirmar.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="182"/>
        <source>You are about to destroy profile &apos;%1&apos;.</source>
        <translation>Você está prestes a destruir o perfil &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="201"/>
        <source>Sorry</source>
        <translation>Desculpe</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="201"/>
        <source>You need to enter DELETE in capital letters.</source>
        <translation>Você precisa digitar DELETE em letra maiúsculas.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="239"/>
        <source>You entered an incorrect password</source>
        <translation>Você digitou uma senha incorreta</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="242"/>
        <source>If you&apos;re trying to delete because you forgot the password, you need to delete it manually.</source>
        <translation>Se você está tentando deletar por ter esquecido a senha, você precisa deletar manualmente.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="255"/>
        <source>There was an error deleting the profile directory, you need to manually remove it.</source>
        <translation>Houve um erro deletando a pasta do perfil, você precisa removê-la manualmente.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="259"/>
        <source>Profile &apos;%1&apos; was succesfully deleted</source>
        <translation>Perfil &apos;%1&apos; foi deletado com sucesso</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="289"/>
        <source>Create new profile</source>
        <translation>Criar novo perfil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="326"/>
        <source>Enter Password</source>
        <translation>Digitar Senha</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="345"/>
        <source>You entered an Incorrect Password too many times. Exiting!</source>
        <translation>Você digitou uma senha incorreta vezes demais. Saindo!</translation>
    </message>
</context>
<context>
    <name>ProfileSelector</name>
    <message>
        <location filename="../oscar/profileselector.ui" line="14"/>
        <source>Form</source>
        <translation>Forma</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="26"/>
        <source>Filter:</source>
        <translation>Filtro:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="36"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="171"/>
        <source>OSCAR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="186"/>
        <source>Version</source>
        <translation>Versão</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="203"/>
        <source>&amp;Open Profile</source>
        <translation>Abr&amp;ir Perfil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="214"/>
        <source>&amp;Edit Profile</source>
        <translation>&amp;Editar Perfil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="228"/>
        <source>&amp;New Profile</source>
        <translation>&amp;Novo Perfil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="247"/>
        <source>Profile: None</source>
        <translation>Perfil: Nenhum</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="268"/>
        <source>Please select or create a profile...</source>
        <translation>Por favor selecione ou crie um perfil...</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="319"/>
        <source>Destroy Profile</source>
        <translation>Destruir Perfil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="88"/>
        <source>Profile</source>
        <translation>Perfil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="89"/>
        <source>Ventilator Brand</source>
        <translation>Marca do Ventilador</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="90"/>
        <source>Ventilator Model</source>
        <translation>Modelo do Ventilador</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="91"/>
        <source>Other Data</source>
        <translation>Outros Dados</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="92"/>
        <source>Last Imported</source>
        <translation>Importado por Último</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="93"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="133"/>
        <location filename="../oscar/profileselector.cpp" line="304"/>
        <source>%1, %2</source>
        <translation>%1, %2</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="168"/>
        <source>You must create a profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="231"/>
        <location filename="../oscar/profileselector.cpp" line="355"/>
        <source>Enter Password for %1</source>
        <translation>Digite a senha para %1</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="247"/>
        <location filename="../oscar/profileselector.cpp" line="374"/>
        <source>You entered an incorrect password</source>
        <translation>Você digitou uma senha incorreta</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="250"/>
        <source>Forgot your password?</source>
        <translation>Esqueceu sua senha?</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="250"/>
        <source>Ask on the forums how to reset it, it&apos;s actually pretty easy.</source>
        <translation>Pergunte nos fóruns como redefiní-la, na verdade é muito fácil.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="314"/>
        <source>Select a profile first</source>
        <translation>Selecione um perfil primeiro</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="377"/>
        <source>If you&apos;re trying to delete because you forgot the password, you need to either reset it or delete the profile folder manually.</source>
        <translation>Se você está tentando deletar porque você esqueceu a senha, você precisa ou redefiní-la ou deletar a pasta do perfil manualmente.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="387"/>
        <source>You are about to destroy profile &apos;&lt;b&gt;%1&lt;/b&gt;&apos;.</source>
        <translation>Você está prestes a destruir o perfil &apos;&lt;b&gt;%1&lt;/b&gt;&apos;.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="387"/>
        <source>Think carefully, as this will irretrievably delete the profile along with all &lt;b&gt;backup data&lt;/b&gt; stored under&lt;br/&gt;%2.</source>
        <translation>Pense com cuidado, já que isso irá irreverssívelmente deletar o perfil bem como todos os &lt;b&gt;dados de backup&lt;/b&gt; guardados sob&lt;br/&gt;%2.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="387"/>
        <source>Enter the word &lt;b&gt;DELETE&lt;/b&gt; below (exactly as shown) to confirm.</source>
        <translation>Digite a palavra &lt;b&gt;DELETE&lt;/b&gt; abaixo (exatamente como exibida) parar confirmar.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="405"/>
        <source>DELETE</source>
        <translation>DELETE</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="406"/>
        <source>Sorry</source>
        <translation>Desculpe</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="406"/>
        <source>You need to enter DELETE in capital letters.</source>
        <translation>Você precisa digitar DELETE em letras maiúsculas.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="419"/>
        <source>There was an error deleting the profile directory, you need to manually remove it.</source>
        <translation>Houve um erro deletando a pasta do perfil, você precisa removê-lo manualmente.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="423"/>
        <source>Profile &apos;%1&apos; was succesfully deleted</source>
        <translation>Perfil &apos;%1&apos; foi deletado com sucesso</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="433"/>
        <source>Bytes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="433"/>
        <source>KB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="433"/>
        <source>MB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="433"/>
        <source>GB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="433"/>
        <source>TB</source>
        <translation>RC</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="433"/>
        <source>PB</source>
        <translation>RP</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="453"/>
        <source>Summaries:</source>
        <translation>Resumos:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="454"/>
        <source>Events:</source>
        <translation>Eventos:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="455"/>
        <source>Backups:</source>
        <translation>Backups:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="467"/>
        <location filename="../oscar/profileselector.cpp" line="507"/>
        <source>Hide disk usage information</source>
        <translation>Esconder informação de uso de disco</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="470"/>
        <source>Show disk usage information</source>
        <translation>Mostrar informação de uso de disco</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="488"/>
        <source>Name: %1, %2</source>
        <translation>Nome: %1, %2</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="491"/>
        <source>Phone: %1</source>
        <translation>Telefone: %1</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="494"/>
        <source>Email: &lt;a href=&apos;mailto:%1&apos;&gt;%1&lt;/a&gt;</source>
        <translation>Email: &lt;a href=&apos;mailto:%1&apos;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="497"/>
        <source>Address:</source>
        <translation>Endereço:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="500"/>
        <source>No profile information given</source>
        <translation>Nenhuma informação de perfil fornecida</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="503"/>
        <source>Profile: %1</source>
        <translation>Perfil: %1</translation>
    </message>
</context>
<context>
    <name>ProgressDialog</name>
    <message>
        <location filename="../oscar/SleepLib/progressdialog.cpp" line="56"/>
        <source>Abort</source>
        <translation>Abortar</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1246"/>
        <source>No Data</source>
        <translation>Nenhum Dado</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="680"/>
        <source>On</source>
        <translation>Ligado</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="681"/>
        <source>Off</source>
        <translation>Desligado</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="496"/>
        <source>&quot;</source>
        <translation>&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="497"/>
        <source>ft</source>
        <translation>ft</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="498"/>
        <source>lb</source>
        <translation>lb</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="499"/>
        <source>oz</source>
        <translation>oz</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="500"/>
        <source>Kg</source>
        <translation>Kg</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="501"/>
        <source>cmH2O</source>
        <translation>cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="198"/>
        <source>Med.</source>
        <translation>Med.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="216"/>
        <source>Min: %1</source>
        <translation>Min: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="247"/>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="257"/>
        <source>Min: </source>
        <translation>Min: </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="252"/>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="262"/>
        <source>Max: </source>
        <translation>Max: </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="266"/>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="270"/>
        <source>%1: </source>
        <translation>%1: </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="274"/>
        <source>???: </source>
        <translation>???: </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="281"/>
        <source>Max: %1</source>
        <translation>Max: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="287"/>
        <source>%1 (%2 days): </source>
        <translation>%1 (%2 dias): </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="289"/>
        <source>%1 (%2 day): </source>
        <translation>%1 (%2 dia): </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="347"/>
        <source>% in %1</source>
        <translation>% em %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="353"/>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="664"/>
        <location filename="../oscar/SleepLib/common.cpp" line="502"/>
        <source>Hours</source>
        <translation>Horas</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="359"/>
        <source>Min %1</source>
        <translation>Mín %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="649"/>
        <source>
Hours: %1</source>
        <translation>
Horas: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="715"/>
        <source>%1 low usage, %2 no usage, out of %3 days (%4% compliant.) Length: %5 / %6 / %7</source>
        <translation>%1 baixo uso, %2 nenhum uso, de %3 dias (%4% obervância). Duração: %5 / %6 / %7</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="796"/>
        <source>Sessions: %1 / %2 / %3 Length: %4 / %5 / %6 Longest: %7 / %8 / %9</source>
        <translation>Sessões: %1 / %2 / %3 Duração: %4 / %5 / %6 Mais Longa: %7 / %8 / %9</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="900"/>
        <source>%1
Length: %3
Start: %2
</source>
        <translation>%1
Duração: %3
Início: %2
</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="902"/>
        <source>Mask On</source>
        <translation>Mascara Colocada</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="902"/>
        <source>Mask Off</source>
        <translation>Mascara Removida</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="913"/>
        <source>%1
Length: %3
Start: %2</source>
        <translation>%1
Duração: %3
Início: %2</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="1083"/>
        <source>TTIA:</source>
        <translatorcomment>Tempo Total Em Apneia?</translatorcomment>
        <translation>TTIA:</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="1096"/>
        <source>
TTIA: %1</source>
        <translation>
TTIA: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="1196"/>
        <source>%1 %2 / %3 / %4</source>
        <translation>%1 %2 / %3 / %4</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="512"/>
        <source>bpm</source>
        <translation>bpm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="517"/>
        <source>?</source>
        <translation>?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="519"/>
        <source>Severity (0-1)</source>
        <translation>Severidade (0-1)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="523"/>
        <source>Error</source>
        <translation>Erro</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="524"/>
        <source>Warning</source>
        <translation>Aviso</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="527"/>
        <source>Please Note</source>
        <translation>Por Favor Note</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="530"/>
        <source>Compliance Only :(</source>
        <translation>Observância Apenas :(</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="531"/>
        <source>Graphs Switched Off</source>
        <translation>Gráficos Desativados</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="532"/>
        <source>Summary Only :(</source>
        <translation>Resumo Apenas :(</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="533"/>
        <source>Sessions Switched Off</source>
        <translation>Sessões Desativadas</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="537"/>
        <source>&amp;Yes</source>
        <translation>&amp;Sim</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="538"/>
        <source>&amp;No</source>
        <translation>&amp;Não</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="539"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="540"/>
        <source>&amp;Destroy</source>
        <translation>&amp;Destruir</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="541"/>
        <source>&amp;Save</source>
        <translation>&amp;Salvar</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="543"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="303"/>
        <source>BMI</source>
        <translation>IMC</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="544"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="299"/>
        <source>Weight</source>
        <translation>Peso</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="545"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="304"/>
        <source>Zombie</source>
        <translation>Zumbi</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="546"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="185"/>
        <source>Pulse Rate</source>
        <translation>Taxa de Pulso</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="547"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="190"/>
        <source>SpO2</source>
        <translation>SpO2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="548"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="194"/>
        <source>Plethy</source>
        <translation>Pletis</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="549"/>
        <source>Pressure</source>
        <translation>Pressão</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="551"/>
        <source>Daily</source>
        <translation>Diário</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="552"/>
        <source>Profile</source>
        <translation>Perfil</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="553"/>
        <source>Overview</source>
        <translation>Visão-Geral</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="554"/>
        <source>Oximetry</source>
        <translation>Oximetria</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="556"/>
        <source>Oximeter</source>
        <translation>Oxímetro</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="557"/>
        <source>Event Flags</source>
        <translation>Marcações de Evento</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="560"/>
        <source>Default</source>
        <translation>Padrão</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="563"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3462"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3004"/>
        <source>CPAP</source>
        <translation>CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="564"/>
        <source>BiPAP</source>
        <translation>BiPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="565"/>
        <source>Bi-Level</source>
        <translation>Bi-Level</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="566"/>
        <source>EPAP</source>
        <translation>EPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="567"/>
        <source>Min EPAP</source>
        <translation>EPAP Mín</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="568"/>
        <source>Max EPAP</source>
        <translation>EPAP Máx</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="569"/>
        <source>IPAP</source>
        <translation>IPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="572"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3005"/>
        <source>APAP</source>
        <translation>APAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="573"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3011"/>
        <source>ASV</source>
        <translation>ASV</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="574"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="292"/>
        <source>AVAPS</source>
        <translation>AVAPS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="575"/>
        <source>ST/ASV</source>
        <translation>ST/ASV</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="577"/>
        <source>Humidifier</source>
        <translation>Umidifcador</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="579"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="146"/>
        <source>H</source>
        <translation>H</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="580"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="144"/>
        <source>OA</source>
        <translation>OA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="581"/>
        <source>A</source>
        <translation>A</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="582"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="142"/>
        <source>CA</source>
        <translation>CA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="583"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="150"/>
        <source>FL</source>
        <translation>FL</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="585"/>
        <source>LE</source>
        <translation>LE</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="586"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="169"/>
        <source>EP</source>
        <translation>EP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="587"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="154"/>
        <source>VS</source>
        <translation>VS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="589"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="156"/>
        <source>VS2</source>
        <translation>VS2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="590"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="152"/>
        <source>RERA</source>
        <translation>RERA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="591"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3483"/>
        <source>PP</source>
        <translation>PP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="592"/>
        <source>P</source>
        <translation>P</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="593"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="152"/>
        <source>RE</source>
        <translation>RE</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="594"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="166"/>
        <source>NR</source>
        <translation>NR</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="595"/>
        <source>NRI</source>
        <translation>NRI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="596"/>
        <source>O2</source>
        <translation>O2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="597"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="200"/>
        <source>PC</source>
        <translation>PC</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="598"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="175"/>
        <source>UF1</source>
        <translation>UF1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="599"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="178"/>
        <source>UF2</source>
        <translation>UF2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="600"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="181"/>
        <source>UF3</source>
        <translation>UF3</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="602"/>
        <source>PS</source>
        <translation>PS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="603"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="256"/>
        <source>AHI</source>
        <translation>IAH</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="604"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="265"/>
        <source>RDI</source>
        <translation>IDR</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="605"/>
        <source>AI</source>
        <translation>IA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="606"/>
        <source>HI</source>
        <translation>IH</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="607"/>
        <source>UAI</source>
        <translatorcomment>Índice de Apneia Indeterminada</translatorcomment>
        <translation>IAI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="608"/>
        <source>CAI</source>
        <translation>IAC</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="609"/>
        <source>FLI</source>
        <translatorcomment>Índice de Limitação de Fluxo</translatorcomment>
        <translation>FLI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="611"/>
        <source>REI</source>
        <translatorcomment>Índice de Eventos Respiratórios</translatorcomment>
        <translation>REI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="612"/>
        <source>EPI</source>
        <translation>EPI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="646"/>
        <location filename="../oscar/SleepLib/journal.cpp" line="25"/>
        <source>OSCAR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="570"/>
        <source>Min IPAP</source>
        <translation>IPAP Mín</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="490"/>
        <source>Software Engine</source>
        <translation>Motor de Software</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="491"/>
        <source>ANGLE / OpenGLES</source>
        <translation>ANGLE / OpenGLES</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="492"/>
        <source>Desktop OpenGL</source>
        <translation>OpenGL Desktop</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="494"/>
        <source> m</source>
        <translation> m</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="495"/>
        <source> cm</source>
        <translation> cm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="503"/>
        <source>Minutes</source>
        <translation>Minutos</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="504"/>
        <source>Seconds</source>
        <translation>Segundos</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="505"/>
        <source>h</source>
        <translation>h</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="506"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="507"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="508"/>
        <source>ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="509"/>
        <source>Events/hr</source>
        <translation>Eventos/hr</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="511"/>
        <source>Hz</source>
        <translation>Hz</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="514"/>
        <source>Litres</source>
        <translation>Litros</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="515"/>
        <source>ml</source>
        <translation>ml</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="516"/>
        <source>Breaths/min</source>
        <translation>Respirações/min</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="520"/>
        <source>Degrees</source>
        <translation>Graus</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="525"/>
        <source>Information</source>
        <translation>Informação</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="526"/>
        <source>Busy</source>
        <translation>Ocupado</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="571"/>
        <source>Max IPAP</source>
        <translation>IPAP Máx</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="584"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="172"/>
        <source>SA</source>
        <translatorcomment>Apneia do Sono</translatorcomment>
        <translation>AS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="613"/>
        <source>ÇSR</source>
        <translation>RCS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="614"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="140"/>
        <source>PB</source>
        <translatorcomment>Respiração Periódica</translatorcomment>
        <translation>RP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="618"/>
        <source>IE</source>
        <translation>IE</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="619"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="241"/>
        <source>Insp. Time</source>
        <translation>Tempo de Insp.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="620"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="238"/>
        <source>Exp. Time</source>
        <translation>Tempo de Exp.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="621"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="244"/>
        <source>Resp. Event</source>
        <translation>Evento Resp.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="622"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="150"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="247"/>
        <source>Flow Limitation</source>
        <translation>Limitação de Fluxo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="623"/>
        <source>Flow Limit</source>
        <translation>Limite de Fluxo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="624"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="172"/>
        <source>SensAwake</source>
        <translation>SensAwake</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="625"/>
        <source>Pat. Trig. Breath</source>
        <translation>Resp. por Paciente</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="626"/>
        <source>Tgt. Min. Vent</source>
        <translation>Vent. Mín. Alvo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="627"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="250"/>
        <source>Target Vent.</source>
        <translation>Vent Alvo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="628"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="222"/>
        <source>Minute Vent.</source>
        <translation>Vent. Minuto</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="629"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="216"/>
        <source>Tidal Volume</source>
        <translation>Volume Tidal</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="630"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="225"/>
        <source>Resp. Rate</source>
        <translation>Taxa de Resp.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="631"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="219"/>
        <source>Snore</source>
        <translation>Ronco</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="632"/>
        <source>Leak</source>
        <translation>Vazamento</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="633"/>
        <source>Leaks</source>
        <translation>Vazamentos</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="636"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="259"/>
        <source>Total Leaks</source>
        <translation>Vazamentos Toais</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="637"/>
        <source>Unintentional Leaks</source>
        <translation>Vazamentos Não-Intencionais</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="638"/>
        <source>MaskPressure</source>
        <translation>PressãoMáscara</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="639"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="207"/>
        <source>Flow Rate</source>
        <translation>Taxa de Fluxo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="640"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="315"/>
        <source>Sleep Stage</source>
        <translation>Estágio do Sono</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="641"/>
        <source>Usage</source>
        <translation>Uso</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="642"/>
        <source>Sessions</source>
        <translation>Sessões</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="643"/>
        <source>Pr. Relief</source>
        <translation>Alívio de Pr.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="529"/>
        <source>No Data Available</source>
        <translation>Nenhum Dado Disponível</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="645"/>
        <source>Bookmarks</source>
        <translation>Favoritos</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="647"/>
        <source>v%1</source>
        <translation>v%1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="649"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="2999"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3001"/>
        <source>Mode</source>
        <translation>Modo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="650"/>
        <source>Model</source>
        <translation>Modelo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="651"/>
        <source>Brand</source>
        <translation>Marca</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="652"/>
        <source>Serial</source>
        <translation>Serial</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="653"/>
        <source>Series</source>
        <translation>Série</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="654"/>
        <source>Machine</source>
        <translation>Máquina</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="655"/>
        <source>Channel</source>
        <translation>Canal</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="656"/>
        <source>Settings</source>
        <translation>Configurações</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="661"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="662"/>
        <source>DOB</source>
        <translation>Data Nasc.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="663"/>
        <source>Phone</source>
        <translation>Telefone</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="664"/>
        <source>Address</source>
        <translation>Endereço</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="665"/>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="666"/>
        <source>Patient ID</source>
        <translation>RG Paciente</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="667"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="669"/>
        <source>Bedtime</source>
        <translation>Hora de Dormir</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="670"/>
        <source>Wake-up</source>
        <translation>Acordar</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="671"/>
        <source>Mask Time</source>
        <translation>Tempo de Másc</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="672"/>
        <location filename="../oscar/SleepLib/loader_plugins/weinmann_loader.h" line="124"/>
        <source>Unknown</source>
        <translation>Desconhecido</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="673"/>
        <source>None</source>
        <translation>Nenhum</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="674"/>
        <source>Ready</source>
        <translation>Pronto</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="676"/>
        <source>First</source>
        <translation>Primeiro</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="677"/>
        <source>Last</source>
        <translation>Último</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="678"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="305"/>
        <source>Start</source>
        <translation>Início</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="679"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="306"/>
        <source>End</source>
        <translation>Fim</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="682"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3529"/>
        <source>Yes</source>
        <translation>Sim</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="683"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3530"/>
        <source>No</source>
        <translation>Não</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="685"/>
        <source>Min</source>
        <translation>Mín</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="686"/>
        <source>Max</source>
        <translation>Máx</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="687"/>
        <source>Med</source>
        <translation>Méd</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="689"/>
        <source>Average</source>
        <translation>Média</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="690"/>
        <source>Median</source>
        <translation>Mediana</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="202"/>
        <location filename="../oscar/SleepLib/common.cpp" line="691"/>
        <source>Avg</source>
        <translation>Méd</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="200"/>
        <location filename="../oscar/SleepLib/common.cpp" line="692"/>
        <source>W-Avg</source>
        <translation>Méd-Aco</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="246"/>
        <source>RemStar Plus Compliance Only</source>
        <translation>RemStar Plus Apenas Observância</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="249"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3454"/>
        <source>RemStar Pro with C-Flex+</source>
        <translation>RemStar Pro com C-Flex+</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="252"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3455"/>
        <source>RemStar Auto with A-Flex</source>
        <translation>RemStar Auto com A-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="255"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3456"/>
        <source>RemStar BiPAP Pro with Bi-Flex</source>
        <translation>RemStar BiPAP Pro com Bi-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="258"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3457"/>
        <source>RemStar BiPAP Auto with Bi-Flex</source>
        <translation>RemStar BiPAP Auto com Bi-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="261"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3459"/>
        <source>BiPAP autoSV Advanced</source>
        <translation>BiPAP autoSV Advanced</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="264"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3461"/>
        <source>BiPAP AVAPS</source>
        <translation>BiPAP AVAPS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="267"/>
        <source>Unknown Model</source>
        <translation>Modelo Desconhecido</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="275"/>
        <source>System One (60 Series)</source>
        <translation>System One (60 Series)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="278"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="345"/>
        <source>DreamStation</source>
        <translation>DreamStation</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="281"/>
        <source>unknown</source>
        <translation>Desconhecido</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="515"/>
        <source>Getting Ready...</source>
        <translation>Aprontando-se...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="576"/>
        <source>Non Data Capable Machine</source>
        <translation>Máquina Sem Capacidade de Dados</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="577"/>
        <source>Your Philips Respironics CPAP machine (Model %1) is unfortunately not a data capable model.</source>
        <translation>Sua máquina CPAP Philips Respironics (Modelo %1) infelizmente não é um modelo com capacidade de dados.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="588"/>
        <source>Machine Unsupported</source>
        <translation>Máquina Não Suportada</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="589"/>
        <source>Sorry, your Philips Respironics CPAP machine (Model %1) is not supported yet.</source>
        <translation>Lamendo, sua máquina CPAP Philips Respironics (Modelo %1) ainda não é suportada.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="578"/>
        <source>I&apos;m sorry to report that OSCAR can only track hours of use and very basic settings for this machine.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="590"/>
        <source>The developers needs a .zip copy of this machines&apos; SD card and matching Encore .pdf reports to make it work with OSCAR.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="631"/>
        <source>Scanning Files...</source>
        <translation>Vasculhando Arquivos...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="763"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="1980"/>
        <source>Importing Sessions...</source>
        <translation>Importando Sessões...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="768"/>
        <source>Finishing up...</source>
        <translation>Finalizando...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2240"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3574"/>
        <source>15mm</source>
        <translation>15mm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2240"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3573"/>
        <source>22mm</source>
        <translation>22mm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3458"/>
        <source>RemStar Plus</source>
        <translation>RemStar Plus</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3460"/>
        <source>BiPAP autoSV Advanced 60 Series</source>
        <translation>BiPAP autoSV Advanced 60 Series</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3463"/>
        <source>CPAP Pro</source>
        <translation>CPAP Pro</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3464"/>
        <source>Auto CPAP</source>
        <translation>Auto CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3465"/>
        <source>BiPAP Pro</source>
        <translation>BiPAP Pro</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3466"/>
        <source>Auto BiPAP</source>
        <translation>Auto BiPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3487"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3489"/>
        <source>Flex Mode</source>
        <translation>Flex Mode</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3488"/>
        <source>PRS1 pressure relief mode.</source>
        <translation>Modo PRS1 de alívio de pressão.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3494"/>
        <source>C-Flex</source>
        <translation>C-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3495"/>
        <source>C-Flex+</source>
        <translation>C-Flex+</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3496"/>
        <source>A-Flex</source>
        <translation>A-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3497"/>
        <source>Rise Time</source>
        <translation>Tempo de Rampa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3498"/>
        <source>Bi-Flex</source>
        <translation>Bi-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3502"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3504"/>
        <source>Flex Level</source>
        <translation>Flex Level</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3503"/>
        <source>PRS1 pressure relief setting.</source>
        <translation>Configuração PRS1 alívio de pressão.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3508"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3539"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3561"/>
        <source>x1</source>
        <translation>x1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3509"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3540"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3562"/>
        <source>x2</source>
        <translation>x2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3510"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3541"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3563"/>
        <source>x3</source>
        <translation>x3</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3511"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3542"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3564"/>
        <source>x4</source>
        <translation>x4</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3512"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3543"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3565"/>
        <source>x5</source>
        <translation>x5</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3516"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3518"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3056"/>
        <source>Humidifier Status</source>
        <translation>Estado de Umidificador</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3517"/>
        <source>PRS1 humidifier connected?</source>
        <translation>Umidificador PRS1 conectado?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3520"/>
        <source>Disconnected</source>
        <translation>Disconectado</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3521"/>
        <source>Connected</source>
        <translation>Conectado</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3525"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3527"/>
        <source>Heated Tubing</source>
        <translation>Traqueia Aquecida</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3526"/>
        <source>Heated Tubing Connected</source>
        <translation>Traqueia Aquecida Conectada</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3534"/>
        <source>Humidification Level</source>
        <translation>Nível de Umidificação</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3535"/>
        <source>PRS1 Humidification level</source>
        <translation>Nível de Umidifcação PRS1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3536"/>
        <source>Humid. Lvl.</source>
        <translation>Nvl. Umid.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3547"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3548"/>
        <source>System One Resistance Status</source>
        <translation>Estado de System One Resistance</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3549"/>
        <source>Sys1 Resist. Status</source>
        <translation>Estado Sys1 Resist.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3556"/>
        <source>System One Resistance Setting</source>
        <translation>Configuração de System One Resistance</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3557"/>
        <source>System One Mask Resistance Setting</source>
        <translation>Configuração de Máscara System One Mask</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3558"/>
        <source>Sys1 Resist. Set</source>
        <translation>Conf. Sys1 Resist.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3569"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3571"/>
        <source>Hose Diameter</source>
        <translation>Diâmetro da Traquéia</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3570"/>
        <source>Diameter of primary CPAP hose</source>
        <translation>Diâmetro da traquéia do CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3578"/>
        <source>System One Resistance Lock</source>
        <translation>Trava System One Resistance</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3579"/>
        <source>Whether System One resistance settings are available to you.</source>
        <translation>Se as configurações do System One resistance são disponíveis a você ou não.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3580"/>
        <source>Sys1 Resist. Lock</source>
        <translation>Trava Sys1 Resist.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3587"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3589"/>
        <source>Auto On</source>
        <translation>Auto Ligar</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3588"/>
        <source>A few breaths automatically starts machine</source>
        <translation>Algumas respirações automaticamente ligam a máquina</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3596"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3598"/>
        <source>Auto Off</source>
        <translation>Auto Desligar</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3597"/>
        <source>Machine automatically switches off</source>
        <translation>Máquina desliga automaticamente</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3605"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3607"/>
        <source>Mask Alert</source>
        <translation>Alerta de Máscara</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3606"/>
        <source>Whether or not machine allows Mask checking.</source>
        <translation>Se a máquina permite verificação da máscara</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3614"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3616"/>
        <source>Show AHI</source>
        <translation>Mostrar IAH</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3615"/>
        <source>Whether or not machine shows AHI via LCD panel.</source>
        <translation>Se a máquina mostra ou não IAH no painel LCD.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3628"/>
        <source>Unknown PRS1 Code %1</source>
        <translation>Código PRS1 Desconhecido %1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3629"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3630"/>
        <source>PRS1_%1</source>
        <translation>PRS1_%1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3686"/>
        <source>Breathing Not Detected</source>
        <translation>Respiração Não Detectada</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3687"/>
        <source>A period during a session where the machine could not detect flow.</source>
        <translation>Um período durante a sesão onde a máquina não pôde detectar fluxo.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3688"/>
        <source>BND</source>
        <translatorcomment>Respiração Não Detectada</translatorcomment>
        <translation>RND</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3703"/>
        <source>Timed Breath</source>
        <translation>Respiração Cronometrada</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3704"/>
        <source>Machine Initiated Breath</source>
        <translation>Respiração Iniciada pela Máquina</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3705"/>
        <source>TB</source>
        <translatorcomment>Respiração Cronometrada</translatorcomment>
        <translation>RC</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="36"/>
        <source>Windows User</source>
        <translation>Utilizador Windows</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="200"/>
        <source>Using </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="200"/>
        <source>, found SleepyHead -
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="201"/>
        <source>You must run the OSCAR Migration Tool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="510"/>
        <source>&lt;i&gt;Your old machine data should be regenerated provided this backup feature has not been disabled in preferences during a previous data import.&lt;/i&gt;</source>
        <translation>&lt;i&gt;Seus dados de máquina antigos devem ser regenerados, desde que esse recurso de backup não tenha sido desativado nas preferências durante uma importação de dados anterior.&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="443"/>
        <source>Launching Windows Explorer failed</source>
        <translation>Execução do Windows Explorer falhou</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="444"/>
        <source>Could not find explorer.exe in path to launch Windows Explorer.</source>
        <translation>Não foi possível encontrar o explorer.exe no caminho para iniciar o Windows Explorer.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="496"/>
        <source>OSCAR (%1) needs to upgrade its database for %2 %3 %4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="509"/>
        <source>&lt;b&gt;OSCAR maintains a backup of your devices data card that it uses for this purpose.&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="513"/>
        <source>OSCAR does not yet have any automatic card backups stored for this device.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="514"/>
        <source>This means you will need to import this machine data again afterwards from your own backups or data card.</source>
        <translation>Isso significa que você precisará importar os dados da máquina novamente a partir de seus próprios backups ou cartão de dados.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="517"/>
        <source>Important:</source>
        <translation>Importante:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="517"/>
        <source>Once you upgrade, you &lt;font size=+1&gt;can not&lt;/font&gt; use this profile with the previous version anymore.</source>
        <translation>Depois de atualizar, você &lt;font size=+1&gt;não pode&lt;/font&gt; mais usar este perfil com a versão anterior.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="518"/>
        <source>If you are concerned, click No to exit, and backup your profile manually, before starting OSCAR again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="519"/>
        <source>Are you ready to upgrade, so you can run the new version of OSCAR?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="533"/>
        <source>Sorry, the purge operation failed, which means this version of OSCAR can&apos;t start.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="546"/>
        <source>Would you like to switch on automatic backups, so next time a new version of OSCAR needs to do so, it can rebuild from these?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="553"/>
        <source>OSCAR will now start the import wizard so you can reinstall your %1 data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="563"/>
        <source>OSCAR will now exit, then (attempt to) launch your computers file manager so you can manually back your profile up:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="565"/>
        <source>Use your file manager to make a copy of your profile directory, then afterwards, restart OSCAR and complete the upgrade process.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="522"/>
        <source>Machine Database Changes</source>
        <translation>Alterações no Banco de Dados da Máquina</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="534"/>
        <source>The machine data folder needs to be removed manually.</source>
        <translation>A pasta de dados da máquina precisa ser removida manualmente.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="535"/>
        <source>This folder currently resides at the following location:</source>
        <translation>Essa pasta reside atualmente na localização seguinte:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="541"/>
        <source>Rebuilding from %1 Backup</source>
        <translation>Recompilando %1 do backup</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="179"/>
        <source>Choose the SleepyHead data folder to migrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="180"/>
        <source>or CANCEL to skip migration.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="194"/>
        <source>The folder you chose does not contain valid SleepyHead data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="195"/>
        <source>You cannot use this folder:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="210"/>
        <source>Migrating </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="210"/>
        <source> files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="211"/>
        <source>from </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="211"/>
        <source>to </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="396"/>
        <source>OSCAR will set up a folder for your data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="397"/>
        <source>If you have been using SleepyHead, OSCAR can copy your old data to this folder later.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="398"/>
        <source>We suggest you use this folder: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="399"/>
        <source>Click Ok to accept this, or No if you want to use a different folder.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="410"/>
        <source>Next time you run OSCAR, you will be asked again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="405"/>
        <source>Choose or create a new folder for OSCAR data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="409"/>
        <source>As you did not select a data folder, OSCAR will exit.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="421"/>
        <source>The folder you chose is not empty, nor does it already contain valid OSCAR data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="443"/>
        <source>Migrate SleepyHead Data?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="444"/>
        <source>On the next screen OSCAR will ask you to select a folder with SleepyHead data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="445"/>
        <source>Click [OK] to go to the next screen or [No] if you do not wish to use any SleepyHead data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="502"/>
        <source>The version of OSCAR you just ran is OLDER than the one used to create this data (%1).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="504"/>
        <source>It is likely that doing this will cause data corruption, are you sure you want to do this?</source>
        <translation>É provável que isso cause corrupção de dados. Tem certeza de que deseja fazer isso?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="522"/>
        <source>Question</source>
        <translation>Pergunta</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="408"/>
        <source>Exiting</source>
        <translation>Encerrando</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="422"/>
        <source>Are you sure you want to use this folder?</source>
        <translation>Tem certeza de que deseja usar esta pasta?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="253"/>
        <source>Don&apos;t forget to place your datacard back in your CPAP machine</source>
        <translation>Não se esqueça de colocar o seu cartão de volta na sua máquina de CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="253"/>
        <source>OSCAR Reminder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="424"/>
        <source>You can only work with one instance of an individual OSCAR profile at a time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="425"/>
        <source>If you are using cloud storage, make sure OSCAR is closed and syncing has completed first on the other computer before proceeding.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="438"/>
        <source>Loading profile &quot;%1&quot;...</source>
        <translation>Carregando perfil &quot;%1&quot;...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2142"/>
        <source>Sorry, your %1 %2 machine is not currently supported.</source>
        <translation>Lamento, sua máquina %1 %2 no momento não é suportada.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1167"/>
        <source>Are you sure you want to reset all your channel colors and settings to defaults?</source>
        <translation>Tem certeza de que deseja redefinir todas as suas configurações de cores e canais para os padrões?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1220"/>
        <source>Are you sure you want to reset all your waveform channel colors and settings to defaults?</source>
        <translation>Tem certeza de que deseja redefinir todas as cores e configurações do seu canal de forma de onda para os padrões?</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="38"/>
        <source>There are no graphs visible to print</source>
        <translation>Não há gráficos visíveis para imprimir</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="55"/>
        <source>Would you like to show bookmarked areas in this report?</source>
        <translation>Gostaria de mostrar áreas favoritadas neste relatório?</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="98"/>
        <source>Printing %1 Report</source>
        <translation>Imprimindo Relatório %1</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="132"/>
        <source>%1 Report</source>
        <translation>Relatório %1</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="190"/>
        <source>: %1 hours, %2 minutes, %3 seconds
</source>
        <translation>: %1 horas, %2 minutos, %3 segundos
</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="236"/>
        <source>RDI	%1
</source>
        <translation>IDR	%1
</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="238"/>
        <source>AHI	%1
</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="271"/>
        <source>AI=%1 HI=%2 CAI=%3 </source>
        <translation>AI=%1 HI=%2 CAI=%3 </translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="277"/>
        <source>REI=%1 VSI=%2 FLI=%3 PB/CSR=%4%%</source>
        <translation>REI=%1 VSI=%2 FLI=%3 RP/RCS=%4%%</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="281"/>
        <source>UAI=%1 </source>
        <translation>IAI=%1 </translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="283"/>
        <source>NRI=%1 LKI=%2 EPI=%3</source>
        <translation>NRI=%1 LKI=%2 EPI=%3</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="352"/>
        <source>Reporting from %1 to %2</source>
        <translation>Relatando de %1 a %2</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="424"/>
        <source>Entire Day&apos;s Flow Waveform</source>
        <translation>Forma de Onda de Fluxo do Dia Todo</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="430"/>
        <source>Current Selection</source>
        <translation>Seleção Atual</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="440"/>
        <source>Entire Day</source>
        <translation>Dia Inteiro</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="555"/>
        <source>OSCAR v%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="561"/>
        <source>Page %1 of %2</source>
        <translation>Página %1 de %2</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="64"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="395"/>
        <source>Jan</source>
        <translation>Jan</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="64"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="395"/>
        <source>Feb</source>
        <translation>Fev</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="64"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="395"/>
        <source>Mar</source>
        <translation>Mar</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="64"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="395"/>
        <source>Apr</source>
        <translation>Abr</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="64"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="395"/>
        <source>May</source>
        <translation>ai</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="64"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="395"/>
        <source>Jun</source>
        <translation>Jun</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="396"/>
        <source>Jul</source>
        <translation>Jul</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="396"/>
        <source>Aug</source>
        <translation>Ago</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="396"/>
        <source>Sep</source>
        <translation>Set</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="396"/>
        <source>Oct</source>
        <translation>Out</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="396"/>
        <source>Nov</source>
        <translation>Nov</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="396"/>
        <source>Dec</source>
        <translation>Dez</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="366"/>
        <source>Events</source>
        <translation>Eventos</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="364"/>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="367"/>
        <source>Duration</source>
        <translation>Duração</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="381"/>
        <source>(% %1 in events)</source>
        <translation>(% %1 em eventos)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="110"/>
        <source>Couldn&apos;t parse Channels.xml, this build is seriously borked, no choice but to abort!!</source>
        <translation>Impossível interpretar Channels.xml, essa compilação está seriamente comprometida, nenhuma alternativa a não ser abortar!</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="121"/>
        <source>Therapy Pressure</source>
        <translation>Pressão da Terapia</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="122"/>
        <source>Inspiratory Pressure</source>
        <translation>Pressão Inspiratória</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="123"/>
        <source>Lower Inspiratory Pressure</source>
        <translation>Pressão Inferior Respiratória</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="124"/>
        <source>Higher Inspiratory Pressure</source>
        <translation>Pressão Superior Respiratória</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="125"/>
        <source>Expiratory Pressure</source>
        <translation>Pressão Expiratória</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="126"/>
        <source>Lower Expiratory Pressure</source>
        <translation>Pressão Expiratória Inferior</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="127"/>
        <source>Higher Expiratory Pressure</source>
        <translation>Pressão Expiratória Superior</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="128"/>
        <source>Pressure Support</source>
        <translation>Suporte de Pressão</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="129"/>
        <source>PS Min</source>
        <translation>PS Mín</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="129"/>
        <source>Pressure Support Minimum</source>
        <translation>Mínima Pressão de Suporte</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="130"/>
        <source>PS Max</source>
        <translation>PS Máx</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="130"/>
        <source>Pressure Support Maximum</source>
        <translation>Máxima Pressão de Suporte</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="131"/>
        <source>Min Pressure</source>
        <translation>Pressão Mín</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="131"/>
        <source>Minimum Therapy Pressure</source>
        <translation>Mínima Pressão de Terapia</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="132"/>
        <source>Max Pressure</source>
        <translation>Pressão Máx</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="132"/>
        <source>Maximum Therapy Pressure</source>
        <translation>Máxima Pressão de Terapia</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="133"/>
        <source>Ramp Time</source>
        <translation>Tempo de Rampa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="133"/>
        <source>Ramp Delay Period</source>
        <translation>Período de Atraso de Rampa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="134"/>
        <source>Ramp Pressure</source>
        <translation>Pressão de Rampa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="134"/>
        <source>Starting Ramp Pressure</source>
        <translation>Pressão de Rampa Inicial</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="135"/>
        <source>Ramp Event</source>
        <translation>Evento de Rampa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3134"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3136"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="135"/>
        <source>Ramp</source>
        <translation>Rampa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="156"/>
        <source>Vibratory Snore (VS2) </source>
        <translation>Ronco Vibratório (VS2) </translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="275"/>
        <source>Mask On Time</source>
        <translation>Tempo com Máscara</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="275"/>
        <source>Time started according to str.edf</source>
        <translation>Tempo iniciado de acordo com str.edf</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="278"/>
        <source>Summary Only</source>
        <translation>Apenas Resumo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="510"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="142"/>
        <source>An apnea where the airway is open</source>
        <translation>Uma apneia em que a via aérea está aberta</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="144"/>
        <source>An apnea caused by airway obstruction</source>
        <translation>Uma apneia causada por uma bostrução de via aérea</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="146"/>
        <source>Hypopnea</source>
        <translation>Hipoapneia</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="146"/>
        <source>A partially obstructed airway</source>
        <translation>Uma via aérea parcialmente obstruída</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="148"/>
        <source>Unclassified Apnea</source>
        <translation>Apneia Indeterminada</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="148"/>
        <source>UA</source>
        <translation>AI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="154"/>
        <source>Vibratory Snore</source>
        <translation>Ronco Vibratório</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="154"/>
        <source>A vibratory snore</source>
        <translation>Um ronco vibratório</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="156"/>
        <source>A vibratory snore as detcted by a System One machine</source>
        <translation>Um ronco vibratório como detectado por uma máquina System One</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3481"/>
        <source>Pressure Pulse</source>
        <translation>Pulso de Pressão</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3482"/>
        <source>A pulse of pressure &apos;pinged&apos; to detect a closed airway.</source>
        <translation>Um pulseo de pressão &apos;pingado&apos; para detectar uma via aérea fechada.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="634"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="163"/>
        <source>Large Leak</source>
        <translation>Grande Vazamento</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="159"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="163"/>
        <source>A large mask leak affecting machine performance.</source>
        <translation>Um grande vazamento de máscara afetando o desempenho da máquina.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="635"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="163"/>
        <source>LL</source>
        <translation>GV</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="166"/>
        <source>Non Responding Event</source>
        <translation>Um Evento Não Respondendo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="166"/>
        <source>A type of respiratory event that won&apos;t respond to a pressure increase.</source>
        <translation>Um tipo de evento que não irá responder a um aumento na pressão.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="169"/>
        <source>Expiratory Puff</source>
        <translation>Sopro Expiratório</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="169"/>
        <source>Intellipap event where you breathe out your mouth.</source>
        <translation>Evento Intellipap no qual você expira pela boca.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="172"/>
        <source>SensAwake feature will reduce pressure when waking is detected.</source>
        <translation>Recursos SensAwake reduzirá a pressão quando caminhar é detectado.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="175"/>
        <source>User Flag #1</source>
        <translation>Marca de Utilizador #1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="178"/>
        <source>User Flag #2</source>
        <translation>Marca de Utilizador #2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="181"/>
        <source>User Flag #3</source>
        <translation>Marca de Utilizador #3</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="185"/>
        <source>Heart rate in beats per minute</source>
        <translation>Taxa cardíaca em batimentos por minuto</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="190"/>
        <source>Blood-oxygen saturation percentage</source>
        <translation>Porcentagem de saturação de oxigênio no sangue</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="190"/>
        <source>SpO2 %</source>
        <translation>SpO2 %</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="194"/>
        <source>Plethysomogram</source>
        <translation>Pletismograma</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="194"/>
        <source>An optical Photo-plethysomogram showing heart rhythm</source>
        <translation>Um pletismograma foto-óptico mostrando o ritmo cardíaco</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="200"/>
        <source>Pulse Change</source>
        <translation>Mudança no pulso</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="200"/>
        <source>A sudden (user definable) change in heart rate</source>
        <translation>Uma mudança brusca (definível pelo utilizador) na taxa cardíaca</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="203"/>
        <source>SpO2 Drop</source>
        <translation>Queda de SpO2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="203"/>
        <source>A sudden (user definable) drop in blood oxygen saturation</source>
        <translation>Uma quebra brusca (definível pelo utilizador) na saturação do sangue</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="203"/>
        <source>SD</source>
        <translation>QB</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="207"/>
        <source>Breathing flow rate waveform</source>
        <translation>Forma de onda da taxa de fluxo respiratório</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="513"/>
        <source>L/min</source>
        <translation>L/min</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="210"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="213"/>
        <source>Mask Pressure</source>
        <translation>Pressão de Máscara</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="213"/>
        <source>Mask Pressure (High resolution)</source>
        <translation>Pressão de Máscara (alta resolução)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="216"/>
        <source>Amount of air displaced per breath</source>
        <translation>Quantidade de ar deslocado por respiração</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="219"/>
        <source>Graph displaying snore volume</source>
        <translation>Gráfico mostrando o volume de ronco</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="222"/>
        <source>Minute Ventilation</source>
        <translation>Ventilação por Minuto</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="222"/>
        <source>Amount of air displaced per minute</source>
        <translation>Quantidade de ar deslocado por minuto</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="225"/>
        <source>Respiratory Rate</source>
        <translation>Taxa Respiratória</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="225"/>
        <source>Rate of breaths per minute</source>
        <translation>Taxa de respirações por minuto</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="228"/>
        <source>Patient Triggered Breaths</source>
        <translation>Respirações Iniciadas pelo Paciente</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="228"/>
        <source>Percentage of breaths triggered by patient</source>
        <translation>Porcentagem de respirações iniciadas pelo paciente</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="228"/>
        <source>Pat. Trig. Breaths</source>
        <translation>Resp. Inic. Paciente</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="231"/>
        <source>Leak Rate</source>
        <translation>Taxa de Vazamento</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="231"/>
        <source>Rate of detected mask leakage</source>
        <translation>Taxa do vazamento detectado na máscara</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="235"/>
        <source>I:E Ratio</source>
        <translation>Taxa I:E</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="235"/>
        <source>Ratio between Inspiratory and Expiratory time</source>
        <translation>Taxa entre tempo inspiratório e expiratório</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="518"/>
        <source>ratio</source>
        <translation>taxa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="131"/>
        <source>Pressure Min</source>
        <translation>Mín Pressão</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="132"/>
        <source>Pressure Max</source>
        <translation>Máx Pressão</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="138"/>
        <source>Cheyne Stokes Respiration</source>
        <translation>Respiração Cheyne Stokes</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="138"/>
        <source>An abnormal period of Cheyne Stokes Respiration</source>
        <translation>Um período anormal de respiração Cheyne Stokes</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="138"/>
        <source>CSR</source>
        <translation>RCS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="140"/>
        <source>Periodic Breathing</source>
        <translation>Respiração Periódica</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="140"/>
        <source>An abnormal period of Periodic Breathing</source>
        <translation>Um período anormal de respiração</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="142"/>
        <source>Clear Airway</source>
        <translation>Via Aérea Livre</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="144"/>
        <source>Obstructive</source>
        <translation>Obstrutiva</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="148"/>
        <source>An apnea that couldn&apos;t be determined as Central or Obstructive.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="150"/>
        <source>A restriction in breathing from normal, causing a flattening of the flow waveform.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="152"/>
        <source>Respiratory Effort Related Arousal: An restriction in breathing that causes an either an awakening or sleep disturbance.</source>
        <translation>Excitação Relacionada ao Esforço Respiratório: Uma restrição na respiração que causa um despertar ou distúrbio do sono.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="159"/>
        <source>Leak Flag</source>
        <translation>Marcação Vazamento</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="159"/>
        <source>LF</source>
        <translation>MV</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="175"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="178"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="181"/>
        <source>A user definable event detected by OSCAR&apos;s flow waveform processor.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="197"/>
        <source>Perfusion Index</source>
        <translation>Índice de Perfusão</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="197"/>
        <source>A relative assessment of the pulse strength at the monitoring site</source>
        <translation>Uma avaliação relativa da força de pulso no lugar de monitoramente</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="197"/>
        <source>Perf. Index %</source>
        <translation>Índice Perf. %</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="238"/>
        <source>Expiratory Time</source>
        <translation>Tempo Expiratório</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="238"/>
        <source>Time taken to breathe out</source>
        <translation>Tempo usado para expirar</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="241"/>
        <source>Inspiratory Time</source>
        <translation>Tempo Inspiratório</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="241"/>
        <source>Time taken to breathe in</source>
        <translation>Tempo usado para inspirar</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="244"/>
        <source>Respiratory Event</source>
        <translation>Evento Respiratório</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="244"/>
        <source>A ResMed data source showing Respiratory Events</source>
        <translation>Uma fonte de dados da ResMed mostrando Eventos Respiratórios</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="247"/>
        <source>Graph showing severity of flow limitations</source>
        <translation>Gráfico mostrando a severidade de limitações de fluxo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="247"/>
        <source>Flow Limit.</source>
        <translation>Limite de Fluxo.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="250"/>
        <source>Target Minute Ventilation</source>
        <translation>Alvo Ventilações Minuto</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="253"/>
        <source>Maximum Leak</source>
        <translation>Vazamento Máximo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="253"/>
        <source>The maximum rate of mask leakage</source>
        <translation>A taxa máxima de vazamento da máscara</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="253"/>
        <source>Max Leaks</source>
        <translation>Vazamentos Máx</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="256"/>
        <source>Apnea Hypopnea Index</source>
        <translation>Índice de Apneia Hipo-apneia</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="256"/>
        <source>Graph showing running AHI for the past hour</source>
        <translation>Gráfico mostrando IAH na hora precedente</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="259"/>
        <source>Total Leak Rate</source>
        <translation>Taxa Total de Vazamento</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="259"/>
        <source>Detected mask leakage including natural Mask leakages</source>
        <translation>Vazamento de máscara detectado incluindo vazamentos naturais de máscara</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="262"/>
        <source>Median Leak Rate</source>
        <translation>Taxa Mediana de Vazamento</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="262"/>
        <source>Median rate of detected mask leakage</source>
        <translation>Taxa mediana de vazamento detectado na máscara</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="262"/>
        <source>Median Leaks</source>
        <translation>Vazamentos Medianos</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="265"/>
        <source>Respiratory Disturbance Index</source>
        <translation>Índice de Distúrbio Respiratório</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="265"/>
        <source>Graph showing running RDI for the past hour</source>
        <translation>Gráfico Mostrando o IDR na hora precedente</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="278"/>
        <source>CPAP Session contains summary data only</source>
        <translation>Sessão CPAP contém apenas dados resumidos</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="282"/>
        <source>PAP Mode</source>
        <translation>Modo PAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="282"/>
        <source>PAP Device Mode</source>
        <translation>Modo Aparelho PAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="286"/>
        <source>APAP (Variable)</source>
        <translation>APAP (Variável)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="290"/>
        <source>ASV (Fixed EPAP)</source>
        <translation>ASV (EPAP Fixo)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="291"/>
        <source>ASV (Variable EPAP)</source>
        <translation>ASV (EPAP Variável)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="300"/>
        <source>Height</source>
        <translation>Altura</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="300"/>
        <source>Physical Height</source>
        <translation>Altura Física</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="301"/>
        <source>Notes</source>
        <translation>Notas</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="301"/>
        <source>Bookmark Notes</source>
        <translation>Notas de Favorito</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="303"/>
        <source>Body Mass Index</source>
        <translation>Índice de Massa Corporal</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="304"/>
        <source>How you feel (0 = like crap, 10 = unstoppable)</source>
        <translation>Como se sente (0 = como lixo, 10 = imparável)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="305"/>
        <source>Bookmark Start</source>
        <translation>Começo do Favorito</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="306"/>
        <source>Bookmark End</source>
        <translation>Fim do Favorito</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="307"/>
        <source>Last Updated</source>
        <translation>Última Atualização</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="308"/>
        <source>Journal Notes</source>
        <translation>Notas de Diário</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="308"/>
        <source>Journal</source>
        <translation>Diário</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="315"/>
        <source>1=Awake 2=REM 3=Light Sleep 4=Deep Sleep</source>
        <translation>1=Acordado 2=REM 3=Sono Leve 4=Sono Profundo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="317"/>
        <source>Brain Wave</source>
        <translation>Onda Cerebral</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="317"/>
        <source>BrainWave</source>
        <translation>OndaCerebral</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="318"/>
        <source>Awakenings</source>
        <translation>Despertares</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="318"/>
        <source>Number of Awakenings</source>
        <translation>Número de Despertares</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="319"/>
        <source>Morning Feel</source>
        <translation>Sensação Matutina</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="319"/>
        <source>How you felt in the morning</source>
        <translation>Como se sentiu na manhã</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="320"/>
        <source>Time Awake</source>
        <translation>Tempo Acordado</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="320"/>
        <source>Time spent awake</source>
        <translation>Tempo gasto acordado</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="321"/>
        <source>Time In REM Sleep</source>
        <translation>Tempo No Sono REM</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="321"/>
        <source>Time spent in REM Sleep</source>
        <translation>Tempo gasto no sono REM</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="321"/>
        <source>Time in REM Sleep</source>
        <translation>Tempo no Sono REM</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="322"/>
        <source>Time In Light Sleep</source>
        <translation>Tempo Em Sono Leve</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="322"/>
        <source>Time spent in light sleep</source>
        <translation>Tempo gasto em sono leve</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="322"/>
        <source>Time in Light Sleep</source>
        <translation>Tempo em Sono Leve</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="323"/>
        <source>Time In Deep Sleep</source>
        <translation>Tempo Em Sono Profundo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="323"/>
        <source>Time spent in deep sleep</source>
        <translation>Tempo gasto em sono profundo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="323"/>
        <source>Time in Deep Sleep</source>
        <translation>Tempo em Sono Profundo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="324"/>
        <source>Time to Sleep</source>
        <translation>Tempo para Dormir</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="324"/>
        <source>Time taken to get to sleep</source>
        <translation>Tempo exigido para conseguir adormecer</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="325"/>
        <source>Zeo ZQ</source>
        <translation>Zeo ZQ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="325"/>
        <source>Zeo sleep quality measurement</source>
        <translation>Medição Zeo de qualidade do sono</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="325"/>
        <source>ZEO ZQ</source>
        <translation>ZEO ZQ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="334"/>
        <source>Debugging channel #1</source>
        <translation>Canal de depuração #1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="334"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="335"/>
        <source>Top secret internal stuff you&apos;re not supposed to see ;)</source>
        <translation>Coisa ultra secreta e interna que você não deveria ver ;)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="334"/>
        <source>Test #1</source>
        <translation>Test #1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="335"/>
        <source>Debugging channel #2</source>
        <translation>Canal de depuração #2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="335"/>
        <source>Test #2</source>
        <translation>Teste #2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="730"/>
        <source>Zero</source>
        <translation>Zero</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="733"/>
        <source>Upper Threshold</source>
        <translation>Limite Superior</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="736"/>
        <source>Lower Threshold</source>
        <translation>Limite Inferior</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="659"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="269"/>
        <source>Orientation</source>
        <translation>Orientação</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="269"/>
        <source>Sleep position in degrees</source>
        <translation>Posição de sono em graus</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="658"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="272"/>
        <source>Inclination</source>
        <translation>Inclinação</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="272"/>
        <source>Upright angle in degrees</source>
        <translation>Ângulo na vertical em graus</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="997"/>
        <source>Days: %1</source>
        <translation>Dias: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1001"/>
        <source>Low Usage Days: %1</source>
        <translation>Dias de Pouco Uso: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1002"/>
        <source>(%1% compliant, defined as &gt; %2 hours)</source>
        <translation>(%1% obervância, definida como &gt; %2 horas)</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1118"/>
        <source>(Sess: %1)</source>
        <translation>(Sess: %1)</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1126"/>
        <source>Bedtime: %1</source>
        <translation>Hora de cama: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1128"/>
        <source>Waketime: %1</source>
        <translation>Hora acordado: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1168"/>
        <source>90%</source>
        <translation>90%</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1240"/>
        <source>(Summary Only)</source>
        <translation>(Apenas Resumod)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="423"/>
        <source>There is a lockfile already present for this profile &apos;%1&apos;, claimed on &apos;%2&apos;.</source>
        <translation>Existe um arquivo de bloqueio já presente para este perfil &apos;%1&apos;, reivindicado em &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="154"/>
        <source>Peak</source>
        <translation>Pico</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="158"/>
        <source>%1% %2</source>
        <translation>%1% %2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="287"/>
        <source>Fixed Bi-Level</source>
        <translation>Bi-Level Fixo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="288"/>
        <source>Auto Bi-Level (Fixed PS)</source>
        <translation>Auto Bi-Level (PS Fixa)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="289"/>
        <source>Auto Bi-Level (Variable PS)</source>
        <translation>Auto Bi-Level (PS Variável)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1373"/>
        <source>%1%2</source>
        <translation>%1%2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1396"/>
        <source>Fixed %1 (%2)</source>
        <translation>%1 (%2) Fixa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1398"/>
        <source>Min %1 Max %2 (%3)</source>
        <translation>Mín %1 Máx %2 (%3)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1400"/>
        <location filename="../oscar/SleepLib/day.cpp" line="1415"/>
        <source>EPAP %1 IPAP %2 (%3)</source>
        <translation>EPAP %1 IPAP %2 (%3)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1402"/>
        <source>PS %1 over %2-%3 (%4)</source>
        <translation>PS %1 sobre %2-%3 (%4)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1404"/>
        <location filename="../oscar/SleepLib/day.cpp" line="1408"/>
        <source>Min EPAP %1 Max IPAP %2 PS %3-%4 (%5)</source>
        <translation>EPAP Mín %1 IPAP Máx %2 PS %3-%4 (%5)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1406"/>
        <source>EPAP %1 PS %2-%3 (%6)</source>
        <translation>EPAP %1 PS %2-%3 (%6)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="145"/>
        <location filename="../oscar/SleepLib/day.cpp" line="147"/>
        <location filename="../oscar/SleepLib/day.cpp" line="149"/>
        <location filename="../oscar/SleepLib/day.cpp" line="154"/>
        <source>%1 %2</source>
        <translation>%1 %2</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="297"/>
        <source>Most recent Oximetry data: &lt;a onclick=&apos;alert(&quot;daily=%2&quot;);&apos;&gt;%1&lt;/a&gt; </source>
        <translation>Dados de Oximetria mais recentes: &lt;a onclick=&apos;alert(&quot;daily=%2&quot;);&apos;&gt;%1&lt;/a&gt; </translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="298"/>
        <source>(last night)</source>
        <translation>(noite passada)</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="299"/>
        <source>(yesterday)</source>
        <translation>(ontem)</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="300"/>
        <source>(%2 day ago)</source>
        <translation>(%2 dias atrás)</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="305"/>
        <source>No oximetry data has been imported yet.</source>
        <translation>Nenum dado de oximetria foi importado ainda.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.h" line="39"/>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.h" line="41"/>
        <source>Contec</source>
        <translation>Contec</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.h" line="39"/>
        <source>CMS50</source>
        <translation>CMS50</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.h" line="77"/>
        <source>Fisher &amp; Paykel</source>
        <translation>Fisher &amp; Paykel</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.h" line="77"/>
        <source>ICON</source>
        <translation>ICON</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.h" line="77"/>
        <source>DeVilbiss</source>
        <translation>DeVilbiss</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.h" line="77"/>
        <source>Intellipap</source>
        <translation>Intellipap</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.h" line="85"/>
        <source>SmartFlex Settings</source>
        <translation>Configurações SmartFlex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.h" line="39"/>
        <source>ChoiceMMed</source>
        <translation>ChoiceMMed</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.h" line="39"/>
        <source>MD300</source>
        <translation>MD300</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/mseries_loader.h" line="66"/>
        <source>Respironics</source>
        <translation>Respironics</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/mseries_loader.h" line="66"/>
        <source>M-Series</source>
        <translation>M-Series</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.h" line="221"/>
        <source>Philips Respironics</source>
        <translation>Philips Respironics</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="272"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.h" line="221"/>
        <source>System One</source>
        <translation>System One</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.h" line="446"/>
        <source>ResMed</source>
        <translation>ResMed</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.h" line="446"/>
        <source>S9</source>
        <translation>S9</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.h" line="455"/>
        <source>EPR: </source>
        <translation>EPR: </translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/somnopose_loader.h" line="37"/>
        <source>Somnopose</source>
        <translation>Somnopose</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/somnopose_loader.h" line="37"/>
        <source>Somnopose Software</source>
        <translation>Software Somnopose</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/zeo_loader.h" line="38"/>
        <source>Zeo</source>
        <translation>Zeo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/zeo_loader.h" line="38"/>
        <source>Personal Sleep Coach</source>
        <translation>Personal Sleep Coach</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="196"/>
        <source>Database Outdated
Please Rebuild CPAP Data</source>
        <translation>Banco de Dados Desatualizado
Por favor, Reconstrua os dados CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="373"/>
        <source> (%2 min, %3 sec)</source>
        <translation> (%2 min, %3 seg)</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="375"/>
        <source> (%3 sec)</source>
        <translation> (%3 seg)</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="386"/>
        <source>Pop out Graph</source>
        <translation>Deslocar Gráfico</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1406"/>
        <source>I&apos;m very sorry your machine doesn&apos;t record useful data to graph in Daily View :(</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1409"/>
        <source>There is no data to graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1590"/>
        <source>d MMM [ %1 - %2 ]</source>
        <translation>d MMM [ %1 - %2 ]</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2111"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2154"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2225"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2242"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2206"/>
        <source>Hide All Events</source>
        <translation>Esconder Todos Eveitos</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2207"/>
        <source>Show All Events</source>
        <translation>Mostrar Todos Eventos</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2548"/>
        <source>Unpin %1 Graph</source>
        <translation>Fixar Gráfico %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2550"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2625"/>
        <source>Popout %1 Graph</source>
        <translation>Deslocar Gráfico %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2627"/>
        <source>Pin %1 Graph</source>
        <translation>Fixar Gráfico %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineChart.cpp" line="1036"/>
        <source>Plots Disabled</source>
        <translation>Desenhos Desativados</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineChart.cpp" line="1110"/>
        <source>Duration %1:%2:%3</source>
        <translation>Duração %1:%2:%3</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineChart.cpp" line="1111"/>
        <source>AHI %1</source>
        <translation>IAH %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="107"/>
        <source>%1: %2</source>
        <translation>%1: %2</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="150"/>
        <source>Relief: %1</source>
        <translation>Alívio: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="156"/>
        <source>Hours: %1h, %2m, %3s</source>
        <translation>Horas: %1h, %2m, %3s</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="259"/>
        <source>Machine Information</source>
        <translation>Informação de Máquina</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="25"/>
        <source>Journal Data</source>
        <translation>Dados de Diário</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="43"/>
        <source>OSCAR found an old Journal folder, but it looks like it&apos;s been renamed:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="45"/>
        <source>OSCAR will not touch this folder, and will create a new one instead.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="46"/>
        <source>Please be careful when playing in OSCAR&apos;s profile folders :-P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="53"/>
        <source>For some reason, OSCAR couldn&apos;t find a journal object record in your profile, but did find multiple Journal data folders.

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="54"/>
        <source>OSCAR picked only the first one of these, and will use it in future:

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="56"/>
        <source>If your old data is missing, copy the contents of all the other Journal_XXXXXXX folders to this one manually.</source>
        <translation>Se seus dados antigos estiverem faltando, copie o conteúdo de todas as outras pastas nomeadas Journal_XXXXXXX para esta manualmente.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.h" line="41"/>
        <source>CMS50F3.7</source>
        <translation>CMS50F3.7</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.h" line="41"/>
        <source>CMS50F</source>
        <translation>CMS50F</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1543"/>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1545"/>
        <source>SmartFlex Mode</source>
        <translation>Modo SmartFlex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1544"/>
        <source>Intellipap pressure relief mode.</source>
        <translation>Modo Intellipap de alívio de pressão.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1550"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3025"/>
        <source>Ramp Only</source>
        <translation>Apenas Rampa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1551"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3026"/>
        <source>Full Time</source>
        <translation>Tempo Total</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1554"/>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1556"/>
        <source>SmartFlex Level</source>
        <translation>Nível SmartFlex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1555"/>
        <source>Intellipap pressure relief level.</source>
        <translation>Nível de alívio de pressão Intellipap.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="657"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="1676"/>
        <source>VPAP Adapt</source>
        <translation>VPAP Adapt</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="1644"/>
        <source>Parsing Identification File</source>
        <translation>Interpretando Arquivo de Identificação</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="1743"/>
        <source>Locating STR.edf File(s)...</source>
        <translation>Localizando arquivo(s) STR.edf...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="1918"/>
        <source>Cataloguing EDF Files...</source>
        <translation>Catalogando arquivos EDF...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="1929"/>
        <source>Queueing Import Tasks...</source>
        <translation>Ordenando Tarefas Importantes...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="1989"/>
        <source>Finishing Up...</source>
        <translation>Terminando...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3000"/>
        <source>CPAP Mode</source>
        <translation>Modo CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3006"/>
        <source>VPAP-T</source>
        <translation>VPAP-T</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3007"/>
        <source>VPAP-S</source>
        <translation>VPAP-S</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3008"/>
        <source>VPAP-S/T</source>
        <translation>VPAP-S/T</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3009"/>
        <source>??</source>
        <translation>??</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3010"/>
        <source>VPAPauto</source>
        <translation>VPAPauto</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3012"/>
        <source>ASVAuto</source>
        <translation>ASVAuto</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3013"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3014"/>
        <source>???</source>
        <translation>???</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3015"/>
        <source>Auto for Her</source>
        <translation>Auto para Ela</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3018"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3020"/>
        <source>EPR</source>
        <translation>EPR</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3019"/>
        <source>ResMed Exhale Pressure Relief</source>
        <translation>Alívio de Pressão ResMed Exhale</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3027"/>
        <source>Patient???</source>
        <translation>Paciente???</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3030"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3032"/>
        <source>EPR Level</source>
        <translation>Nível EPR</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3031"/>
        <source>Exhale Pressure Relief Level</source>
        <translation>Alívio de Pressão de Expiração</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3035"/>
        <source>0cmH2O</source>
        <translation>0cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3036"/>
        <source>1cmH2O</source>
        <translation>1cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3037"/>
        <source>2cmH2O</source>
        <translation>2cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3038"/>
        <source>3cmH2O</source>
        <translation>3cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3045"/>
        <source>SmartStart</source>
        <translation>SmartStart</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3046"/>
        <source>Machine auto starts by breathing</source>
        <translation>Máquina liga automaticamente com a respiração</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3047"/>
        <source>Smart Start</source>
        <translation>Smart Start</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3054"/>
        <source>Humid. Status</source>
        <translation>Estado do Umidif.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3055"/>
        <source>Humidifier Enabled Status</source>
        <translation>Estado de Umidificador Ativo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3063"/>
        <source>Humid. Level</source>
        <translation>Nível do Umidif.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3064"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3065"/>
        <source>Humidity Level</source>
        <translation>Nível de Umidade</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3079"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3081"/>
        <source>Temperature</source>
        <translation>Temperatura</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3080"/>
        <source>ClimateLine Temperature</source>
        <translation>Temperatura ClimateLine</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3086"/>
        <source>Temp. Enable</source>
        <translation>Temper. Ativa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3087"/>
        <source>ClimateLine Temperature Enable</source>
        <translation>Ativar Temperatura ClimateLine</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3088"/>
        <source>Temperature Enable</source>
        <translation>Ativar Temperatura</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3097"/>
        <source>AB Filter</source>
        <translation>Filtro AB</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3098"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3099"/>
        <source>Antibacterial Filter</source>
        <translation>Filtro Antibacteriano</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3106"/>
        <source>Pt. Access</source>
        <translation>Acesso Pac.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3107"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3108"/>
        <source>Patient Access</source>
        <translation>Acesso Paciente</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3115"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3116"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3117"/>
        <source>Climate Control</source>
        <translation>Controle Climático</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3120"/>
        <source>Manual</source>
        <translation>Manual</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3121"/>
        <source>Auto</source>
        <translation>Automático</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3124"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3126"/>
        <source>Mask</source>
        <translation>Máscara</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3125"/>
        <source>ResMed Mask Setting</source>
        <translation>Configuração de Máscara ResMed</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3129"/>
        <source>Pillows</source>
        <translation>Almofadas</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3130"/>
        <source>Full Face</source>
        <translation>Facial Total</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3131"/>
        <source>Nasal</source>
        <translation>Nasal</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3135"/>
        <source>Ramp Enable</source>
        <translation>Ativar Rampa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/weinmann_loader.h" line="116"/>
        <source>Weinmann</source>
        <translation>Weinmann</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/weinmann_loader.h" line="116"/>
        <source>SOMNOsoft2</source>
        <translation>SOMNOsoft2</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="351"/>
        <source>Snapshot %1</source>
        <translation>Captura %1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="266"/>
        <source>CMS50D+</source>
        <translation>CMS50D+</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="266"/>
        <source>CMS50E/F</source>
        <translation>CMS50E/F</translation>
    </message>
    <message>
        <location filename="../oscar/updateparser.cpp" line="228"/>
        <source>%1
Line %2, column %3</source>
        <translation>%1
Linha %2, coluna %3</translation>
    </message>
    <message>
        <location filename="../oscar/updateparser.cpp" line="241"/>
        <source>Could not parse Updates.xml file.</source>
        <translation>Impossível interpretar arquivo Updates.xml.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="612"/>
        <source>Loading %1 data for %2...</source>
        <translation>Carregando dados %1 para %2...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="622"/>
        <source>Scanning Files</source>
        <translation>Vasculhando arquivos</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="658"/>
        <source>Migrating Summary File Location</source>
        <translation>Migrando Localização de Arquivo de Resumo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="961"/>
        <source>Loading Summaries.xml.gz</source>
        <translation>Carregando Summaries.xml.gz</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="1088"/>
        <source>Loading Summary Data</source>
        <translation>Carregando Dados de Resumos</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/progressdialog.cpp" line="14"/>
        <source>Please Wait...</source>
        <translation>Por favor Aguarde...</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/MinutesAtPressure.cpp" line="312"/>
        <source>Peak %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Report</name>
    <message>
        <location filename="../oscar/reports.ui" line="14"/>
        <source>Form</source>
        <translation>Forma</translation>
    </message>
    <message>
        <location filename="../oscar/reports.ui" line="27"/>
        <source>about:blank</source>
        <translation>about:blank</translation>
    </message>
</context>
<context>
    <name>SessionBar</name>
    <message>
        <location filename="../oscar/sessionbar.cpp" line="246"/>
        <source>%1h %2m</source>
        <translation type="unfinished">%1: %2m {1h?}</translation>
    </message>
    <message>
        <location filename="../oscar/sessionbar.cpp" line="289"/>
        <source>No Sessions Present</source>
        <translation>Nenhuma Sessão Presente</translation>
    </message>
</context>
<context>
    <name>Statistics</name>
    <message>
        <location filename="../oscar/statistics.cpp" line="1111"/>
        <source>Details</source>
        <translation>Detalhes</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1051"/>
        <source>Most Recent</source>
        <translation>Mais Recente</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="507"/>
        <source>Compliance</source>
        <translation>Observância</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="509"/>
        <source>Therapy Efficacy</source>
        <translation>Eficácia da Terapia</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1053"/>
        <source>Last 30 Days</source>
        <translation>Últimos 30 Dias</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1055"/>
        <source>Last Year</source>
        <translation>Último Ano</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="558"/>
        <location filename="../oscar/statistics.cpp" line="559"/>
        <source>Average %1</source>
        <translation>Média %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="502"/>
        <source>CPAP Statistics</source>
        <translation>Estatísticas CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="505"/>
        <location filename="../oscar/statistics.cpp" line="1216"/>
        <source>CPAP Usage</source>
        <translation>Uso CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="506"/>
        <source>Average Hours per Night</source>
        <translation>Horas Médias por Noite</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="519"/>
        <source>Leak Statistics</source>
        <translation>Estatísticas de Vazamento</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="524"/>
        <source>Pressure Statistics</source>
        <translation>Estatísticas de Pressão</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="537"/>
        <source>Oximeter Statistics</source>
        <translation>Estatísticas de Oxímetro</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="541"/>
        <source>Blood Oxygen Saturation</source>
        <translation>Saturação de Oxigênio Sérico</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="546"/>
        <source>Pulse Rate</source>
        <translation>Taxa de Pulso</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="557"/>
        <source>%1 Median</source>
        <translation>Mediana %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="561"/>
        <source>Min %1</source>
        <translation>Mín %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="562"/>
        <source>Max %1</source>
        <translation>Máx %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="563"/>
        <source>%1 Index</source>
        <translation>Índice %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="564"/>
        <source>% of time in %1</source>
        <translation>% de tempo em %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="565"/>
        <source>% of time above %1 threshold</source>
        <translation>% acima do limite %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="566"/>
        <source>% of time below %1 threshold</source>
        <translation>% do tempo abaixo do limite %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="587"/>
        <source>Name: %1, %2</source>
        <translation type="unfinished">Nome: %1, %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="589"/>
        <source>DOB: %1</source>
        <translation type="unfinished">Data Nasc.: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="592"/>
        <source>Phone: %1</source>
        <translation type="unfinished">Telefone: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="595"/>
        <source>Email: %1</source>
        <translation type="unfinished">Email: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="598"/>
        <source>Address:</source>
        <translation type="unfinished">Endereço:</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="641"/>
        <source>Usage Statistics</source>
        <translation type="unfinished">Estatísticas de Uso</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="657"/>
        <source>This report was generated by OSCAR v%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="993"/>
        <source>I can haz data?!?</source>
        <translation>Eu posso ter dados?!?</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="995"/>
        <source>Oscar has no data to report :(</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1217"/>
        <source>Days Used: %1</source>
        <translation type="unfinished">Dias de Uso: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1218"/>
        <source>Low Use Days: %1</source>
        <translation type="unfinished">Dias de Pouco Uso: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1219"/>
        <source>Compliance: %1%</source>
        <translation type="unfinished">Observância: %1%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1243"/>
        <source>Days AHI of 5 or greater: %1</source>
        <translation type="unfinished">Dias com IAH 5 ou mais: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1250"/>
        <source>Best AHI</source>
        <translation type="unfinished">Melhor AHI</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1254"/>
        <location filename="../oscar/statistics.cpp" line="1266"/>
        <source>Date: %1 AHI: %2</source>
        <translation type="unfinished">Data: %1 IAH: %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1260"/>
        <source>Worst AHI</source>
        <translation type="unfinished">Pior IAH</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1297"/>
        <source>Best Flow Limitation</source>
        <translation type="unfinished">Melhor Limitação de Fluxo</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1301"/>
        <location filename="../oscar/statistics.cpp" line="1314"/>
        <source>Date: %1 FL: %2</source>
        <translation type="unfinished">Data: %1 LF: %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1307"/>
        <source>Worst Flow Limtation</source>
        <translation type="unfinished">Pior Limitação de Fluxo</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1319"/>
        <source>No Flow Limitation on record</source>
        <translation type="unfinished">Nenhuma Limitação de Fluxo na gravação</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1340"/>
        <source>Worst Large Leaks</source>
        <translation type="unfinished">Pior Grande Vazamento</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1348"/>
        <source>Date: %1 Leak: %2%</source>
        <translation type="unfinished">Data: %1 Vazamento: %2%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1354"/>
        <source>No Large Leaks on record</source>
        <translation type="unfinished">Nenhum Grande Vazamento na gravação</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1377"/>
        <source>Worst CSR</source>
        <translation type="unfinished">Pior RCS</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1385"/>
        <source>Date: %1 CSR: %2%</source>
        <translation type="unfinished">Data: %1 RCS: %2%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1390"/>
        <source>No CSR on record</source>
        <translation type="unfinished">Nenhuma RCS na gravação</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1407"/>
        <source>Worst PB</source>
        <translation type="unfinished">Pior PR</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1415"/>
        <source>Date: %1 PB: %2%</source>
        <translation type="unfinished">Data: %1 PR: %2%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1420"/>
        <source>No PB on record</source>
        <translation type="unfinished">Nenhum PR na gravação</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1428"/>
        <source>Want more information?</source>
        <translation type="unfinished">Quer mais informações?</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1429"/>
        <source>OSCAR needs all summary data loaded to calculate best/worst data for individual days.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1430"/>
        <source>Please enable Pre-Load Summaries checkbox in preferences to make sure this data is available.</source>
        <translation type="unfinished">Ative a caixa de seleção Pré-Carregar Dados Resumidos nas preferências para garantir que esses dados estejam disponíveis.</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1450"/>
        <source>Best RX Setting</source>
        <translation type="unfinished">Melhor Configuração RX</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1453"/>
        <location filename="../oscar/statistics.cpp" line="1465"/>
        <source>Date: %1 - %2</source>
        <translation type="unfinished">Data: %1 - %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1456"/>
        <location filename="../oscar/statistics.cpp" line="1468"/>
        <source>Culminative AHI: %1</source>
        <translation type="unfinished">IAH Culminante: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1457"/>
        <location filename="../oscar/statistics.cpp" line="1469"/>
        <source>Culminative Hours: %1</source>
        <translation type="unfinished">Horas Culminantes: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1462"/>
        <source>Worst RX Setting</source>
        <translation type="unfinished">Pior Configuração RX</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1052"/>
        <source>Last Week</source>
        <translation>Última Semana</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1054"/>
        <source>Last 6 Months</source>
        <translation>Últimos 6 Meses</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1059"/>
        <source>Last Session</source>
        <translation>Última Sessão</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1125"/>
        <source>No %1 data available.</source>
        <translation>Nenhum dado %1 disponível.</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1128"/>
        <source>%1 day of %2 Data on %3</source>
        <translation>%1 dia de %2 dados em %3</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1134"/>
        <source>%1 days of %2 Data, between %3 and %4</source>
        <translation>%1 dia de %2 dados em %3 e %4</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="874"/>
        <source>Changes to Prescription Settings</source>
        <translation>Mudanças nas Configurações Receitadas</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="658"/>
        <source>OSCAR is free open-source CPAP report software</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="882"/>
        <source>Days</source>
        <translation>Dias</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="886"/>
        <source>Pressure Relief</source>
        <translation>Alívio de Pressão</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="888"/>
        <source>Pressure Settings</source>
        <translation>Configurações de Pressão</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="818"/>
        <source>Machine Information</source>
        <translation>Informação de Máquina</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="825"/>
        <source>First Use</source>
        <translation>Primeiro Uso</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="826"/>
        <source>Last Use</source>
        <translation>Último Uso</translation>
    </message>
</context>
<context>
    <name>UpdaterWindow</name>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="14"/>
        <source>OSCAR Updater</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="60"/>
        <source>A new version of $APP is available</source>
        <translation>Uma nova versão de $APP está disponível</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="106"/>
        <source>Version Information</source>
        <translation>Informação da Versão</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="117"/>
        <source>Release Notes</source>
        <translation>Notas de Lançamento</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="142"/>
        <source>Build Notes</source>
        <translation>Notas da Compilação</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="195"/>
        <source>Maybe &amp;Later</source>
        <translation>Talvez &amp;Depois</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="215"/>
        <source>&amp;Upgrade Now</source>
        <translation>&amp;Atualizar Agora</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="250"/>
        <source>Please wait while updates are downloaded and installed...</source>
        <translation>Por favor aguarde enquanto atualizações são baixadas e instaladas...</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="264"/>
        <source>Updates</source>
        <translation>Atualizações</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="304"/>
        <source>Component</source>
        <translation>Componente</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="309"/>
        <source>Version</source>
        <translation>Versão</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="314"/>
        <source>Size</source>
        <translation>Tamanho</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="319"/>
        <source>Progress</source>
        <translation>Progresso</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="328"/>
        <source>Log</source>
        <translation>Relatório</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="374"/>
        <source>Downloading &amp; Installing Updates</source>
        <translation>Baixando e Instalando Atualizações</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="394"/>
        <source>&amp;Finished</source>
        <translation>&amp;Terminado</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="93"/>
        <source>Updates are not yet implemented</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="127"/>
        <source>Checking for OSCAR Updates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="146"/>
        <source>Requesting </source>
        <translation>Solicitando </translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="158"/>
        <source>OSCAR Updates are currently unvailable for this platform</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="158"/>
        <location filename="../oscar/UpdaterWindow.cpp" line="504"/>
        <location filename="../oscar/UpdaterWindow.cpp" line="510"/>
        <source>OSCAR Updates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="459"/>
        <source>Version %1 of OSCAR is available, opening link to download site.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="504"/>
        <source>No updates were found for your platform.</source>
        <translation>Nenhuma atualização encontrada para sua plataforma.</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="511"/>
        <source>New OSCAR Updates are avilable:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="171"/>
        <source>%1 bytes received</source>
        <translation>%1 bytes recebidos</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="462"/>
        <source>You are already running the latest version.</source>
        <translation>Você já está executando a versão mais recente.</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="512"/>
        <source>Would you like to download and install them now?</source>
        <translation>Você gostaria de baixar e instalá-las agora?</translation>
    </message>
</context>
<context>
    <name>Welcome</name>
    <message>
        <location filename="../oscar/welcome.ui" line="14"/>
        <source>Form</source>
        <translation>Forma</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="128"/>
        <source>Welcome to the Open Source CPAP Analysis Reporter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="143"/>
        <source>What would you like to do?</source>
        <translation>O que você gostaria de fazer?</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="186"/>
        <source>CPAP Importer</source>
        <translation>Importador CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="224"/>
        <source>Oximetry Wizard</source>
        <translation>Assistente de Oximetria</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="262"/>
        <source>Daily View</source>
        <translation>Visão Diária</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="300"/>
        <source>Overview</source>
        <translation>Visão-Geral</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="338"/>
        <source>Statistics</source>
        <translation>Estatísticas</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="581"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Warning: &lt;/span&gt;&lt;span style=&quot; font-size:10pt; color:#ff0000;&quot;&gt;ResMed S9 SDCards &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; color:#ff0000;&quot;&gt;need &lt;/span&gt;&lt;span style=&quot; font-size:10pt; color:#ff0000;&quot;&gt;to be locked &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; color:#ff0000;&quot;&gt;before &lt;/span&gt;&lt;span style=&quot; font-size:10pt; color:#ff0000;&quot;&gt;inserting into your computer&lt;br/&gt;&lt;/span&gt;&lt;span style=&quot; font-size:10pt; color:#000000;&quot;&gt;Some operating systems write cache files which break their special filesystem Journal&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Aviso: &lt;/span&gt;&lt;span style=&quot; font-size:10pt; color:#ff0000;&quot;&gt;cartões SD ResMed S9 &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; color:#ff0000;&quot;&gt;precisam &lt;/span&gt;&lt;span style=&quot; font-size:10pt; color:#ff0000;&quot;&gt;ser travados &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; color:#ff0000;&quot;&gt;antes &lt;/span&gt;&lt;span style=&quot; font-size:10pt; color:#ff0000;&quot;&gt;de inseri-los no seu computador&lt;br/&gt;&lt;/span&gt;&lt;span style=&quot; font-size:10pt; color:#000000;&quot;&gt;Alguns sistemas operativos escrevem arquivos de cache que corrompem o diário de sistema de arquivos especial deles&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="139"/>
        <source>It would be a good idea to check File-&gt;Preferences first,</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="140"/>
        <source>as there are some options that affect import.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="141"/>
        <source>Note that some preferences are forced when a ResMed machine is detected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="142"/>
        <source>First import can take a few minutes.</source>
        <translation>A primeira importação pode levar alguns minutos.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="155"/>
        <source>The last time you used your %1...</source>
        <translation>A vez mais recente que você usou seu %1...</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="159"/>
        <source>last night</source>
        <translation>noite passada</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="160"/>
        <source>yesterday</source>
        <translation>ontem</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="161"/>
        <source>%2 days ago</source>
        <translation>%2 dias atrás</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="163"/>
        <source>was %1 (on %2)</source>
        <translation>foi %1 (em %2)</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="171"/>
        <source>%1 hours, %2 minutes and %3 seconds</source>
        <translation>%1 horas, %2 minutos e %3 segundos</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="174"/>
        <source>Your machine was on for %1.</source>
        <translation>Sua máquina estava ligada para %1.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="175"/>
        <source>&lt;font color = red&gt;You only had the mask on for %1.&lt;/font&gt;</source>
        <translation>&lt;font color = red&gt;Você só manteve a máscara em uso por %1.&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="186"/>
        <source>under</source>
        <translation>abaixo</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="187"/>
        <source>over</source>
        <translation>acima</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="188"/>
        <source>reasonably close to</source>
        <translation>razoavelmente próximo de</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="189"/>
        <source>equal to</source>
        <translation>igual a</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="203"/>
        <source>You had an AHI of %1, which is %2 your %3 day average of %4.</source>
        <translation>Você teve um IAH de %1, que é %2 sua média de %3 dias de %4.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="212"/>
        <source>Your CPAP machine used a constant %1 %2 of air</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="215"/>
        <source>Your pressure was under %1 %2 for %3% of the time.</source>
        <translation>Sua pressão ficou abaixo de %1 %2, %3% do tempo.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="219"/>
        <source>Your machine used a constant %1-%2 %3 of air.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="228"/>
        <source>Your EPAP pressure fixed at %1 %2.</source>
        <translation>Sua pressão EPAP fixou em %1 %2.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="229"/>
        <location filename="../oscar/welcome.cpp" line="235"/>
        <source>Your IPAP pressure was under %1 %2 for %3% of the time.</source>
        <translation>Sua pressão IPAP ficou abaixo de %1 %2, %3% do tempo.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="234"/>
        <source>Your EPAP pressure was under %1 %2 for %3% of the time.</source>
        <translation>Sua pressão EPAP ficou abaixo de %1 %2, %3% do tempo.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="223"/>
        <source>Your machine was under %1-%2 %3 for %4% of the time.</source>
        <translation>Sua máquina ficou abaixo de %1-%2 %3, %4% do tempo.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="255"/>
        <source>Your average leaks were %1 %2, which is %3 your %4 day average of %5.</source>
        <translation>Seus vazamentos médios foram %1 %2, que é %3 sua média de %4 dias de %5.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="261"/>
        <source>No CPAP data has been imported yet.</source>
        <translation>Nenhum dado CPAP foi importado ainda.</translation>
    </message>
</context>
<context>
    <name>gGraph</name>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="793"/>
        <source>%1 days</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>gGraphView</name>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="392"/>
        <source>100% zoom level</source>
        <translation>100% de aproximação</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="393"/>
        <source>Restore X-axis zoom too 100% to view entire days data.</source>
        <translation>Restore a aproximação de EixoX para 100% para ver os dados completos do dia.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="395"/>
        <source>Reset Graph Layout</source>
        <translation>Redefinir Disposição de Gráfico</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="396"/>
        <source>Resets all graphs to a uniform height and default order.</source>
        <translation>Redefine todos os gráficos para altura uniforme e ordenação padrão.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="399"/>
        <source>Y-Axis</source>
        <translation>EixoY</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="400"/>
        <source>Plots</source>
        <translation>Desenhos</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="405"/>
        <source>CPAP Overlays</source>
        <translation>Sobreposições CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="408"/>
        <source>Oximeter Overlays</source>
        <translation>Sobreposições Oxímetro</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="411"/>
        <source>Dotted Lines</source>
        <translation>Linhas Pontilhadas</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1746"/>
        <source>Double click title to pin / unpin
Click and drag to reorder graphs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2044"/>
        <source>Remove Clone</source>
        <translation>Remover Clone</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2048"/>
        <source>Clone %1 Graph</source>
        <translation>Clonar Gráfico %1</translation>
    </message>
</context>
</TS>
