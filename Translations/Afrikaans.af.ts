<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="af_ZA" sourcelanguage="en_US">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialoog</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="35"/>
        <source>&amp;About</source>
        <translation>R&amp;akende</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="49"/>
        <location filename="../oscar/aboutdialog.cpp" line="125"/>
        <source>Release Notes</source>
        <translation>Vrystelling Notas</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="63"/>
        <source>Credits</source>
        <translation>Erkenning</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="77"/>
        <source>GPL License</source>
        <translation>GPL Lisensie</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="239"/>
        <source>Close</source>
        <translation>Maak Toe</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="43"/>
        <source>Show data folder</source>
        <translation>Vertoon data vouer</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="47"/>
        <source>About OSCAR</source>
        <translation>Rakende OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="91"/>
        <source>Sorry, could not locate About file.</source>
        <translation>Jammer, kon nie Rakende lêer opspoor nie.</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="104"/>
        <source>Sorry, could not locate Credits file.</source>
        <translation>Jammer, kon nie Krediete lêer opspoor nie.</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="116"/>
        <source>Sorry, could not locate Release Notes.</source>
        <translation>Jammer, kon nie Vrystellingnotas opspoor nie.</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="126"/>
        <source>OSCAR v%1</source>
        <translation>OSCAR v%1</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="129"/>
        <source>Important:</source>
        <translation>Belangrik:</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="130"/>
        <source>As this is a pre-release version, it is recommended that you &lt;b&gt;back up your data folder manually&lt;/b&gt; before proceding, because attempting to roll back later may break things.</source>
        <translation>Hierdie is &apos;n toets weergawe. Dit word aanbeveel dat u u &lt;b&gt;data lêer self rugsteun&lt;/b&gt; voordat u voortgaan, deurdat dinge verkeerd kan gaan deur later te probeer terugrol.</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="142"/>
        <source>To see if the license text is available in your language, see %1.</source>
        <translation>Om te sien of die lisensie beskikbaar is in u taal, sien %1.</translation>
    </message>
</context>
<context>
    <name>CMS50F37Loader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.cpp" line="873"/>
        <source>Could not find the oximeter file:</source>
        <translation>Kon nie die oximeter lêer vind nie:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.cpp" line="879"/>
        <source>Could not open the oximeter file:</source>
        <translation>Kon nie die oximeter lêer oopmaak nie:</translation>
    </message>
</context>
<context>
    <name>CMS50Loader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="478"/>
        <source>Could not get data transmission from oximeter.</source>
        <translation>Nie in staat om data van die oximeter te ontvang nie.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="478"/>
        <source>Please ensure you select &apos;upload&apos; from the oximeter devices menu.</source>
        <translation>Kies asb &quot;upload&quot; in dei oximeter toestelle kieskaart.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="546"/>
        <source>Could not find the oximeter file:</source>
        <translation>Kon nie die oximeter lêer vind nie:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="552"/>
        <source>Could not open the oximeter file:</source>
        <translation>Kon nie die oximeter lêer oopmaak nie:</translation>
    </message>
</context>
<context>
    <name>Daily</name>
    <message>
        <location filename="../oscar/daily.ui" line="435"/>
        <source>Form</source>
        <translation>Vorm</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="506"/>
        <source>Go to the previous day</source>
        <translation>Gaan na vorige dag</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="551"/>
        <source>Show or hide the calender</source>
        <translation>Wys of verwyder kalender</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="614"/>
        <source>Go to the next day</source>
        <translation>Gaan na die volgende dag</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="674"/>
        <source>Go to the most recent day with data records</source>
        <translation>Gaan na mees onlangse dag met data inskrywings</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="861"/>
        <source>Events</source>
        <translation>Gebeurtenisse</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="923"/>
        <source>View Size</source>
        <translation>Kyk grootte</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="968"/>
        <location filename="../oscar/daily.ui" line="1401"/>
        <source>Notes</source>
        <translation>Notas</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1022"/>
        <source>Journal</source>
        <translation>Joernaal</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1050"/>
        <source> i </source>
        <translation> i </translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1066"/>
        <source>B</source>
        <translation>B</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1081"/>
        <source>u</source>
        <translation>u</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1094"/>
        <source>Color</source>
        <translation>Kleur</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1116"/>
        <location filename="../oscar/daily.ui" line="1126"/>
        <source>Small</source>
        <translation>Klein</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1131"/>
        <source>Medium</source>
        <translation>Middelmatig</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1136"/>
        <source>Big</source>
        <translation>Groot</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1195"/>
        <source>Zombie</source>
        <translation>Zombie</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1208"/>
        <source>I&apos;m feeling ...</source>
        <translation>Ek voel ...</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1224"/>
        <source>Weight</source>
        <translation>Massa</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1298"/>
        <source>Awesome</source>
        <translation>Uitstekend</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1336"/>
        <source>B.M.I.</source>
        <translation>B.M.I.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1352"/>
        <source>Bookmarks</source>
        <translation>Boekmerke</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1373"/>
        <source>Add Bookmark</source>
        <translation>Voeg Boekmerk by</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1396"/>
        <source>Starts</source>
        <translation>Begin</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1409"/>
        <source>Remove Bookmark</source>
        <translation>Verwyder Boekmerk</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1502"/>
        <source>Flags</source>
        <translation>Merkers</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1554"/>
        <source>Graphs</source>
        <translation>Grafiek</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1579"/>
        <source>Show/hide available graphs.</source>
        <translation>Wys/verberg beskikbare grafieke.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="128"/>
        <source>Details</source>
        <translation>Details</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="220"/>
        <source>Breakdown</source>
        <translation>Afbraak</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="220"/>
        <source>events</source>
        <translation>gebeurtenisse</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="231"/>
        <source>UF1</source>
        <translation>UF1</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="232"/>
        <source>UF2</source>
        <translation>UF2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="291"/>
        <source>Time at Pressure</source>
        <translation>Tyd teen Druk</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="533"/>
        <source>No %1 events are recorded this day</source>
        <translation>Geen %1 gebeurtenisse hierdie dag opgeneem nie</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="639"/>
        <source>%1 event</source>
        <translation>%1 gebeurtenis</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="640"/>
        <source>%1 events</source>
        <translation>%1 gebeurtenisse</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="677"/>
        <source>Session Start Times</source>
        <translation>Sessie Begintye</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="678"/>
        <source>Session End Times</source>
        <translation>Sessie Eindtye</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="838"/>
        <source>Session Information</source>
        <translation>Sessie Inligting</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="861"/>
        <source>Oximetry Sessions</source>
        <translation>Oximeter Sessies</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="881"/>
        <source>Duration</source>
        <translation>Tydsduur</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="899"/>
        <source>Click to %1 this session.</source>
        <translation>Kliek om die sessie te %1.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="899"/>
        <source>disable</source>
        <translation>afskakel</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="899"/>
        <source>enable</source>
        <translation>in staat te stel</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="915"/>
        <source>%1 Session #%2</source>
        <translation>%1 Sessie #%2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="916"/>
        <source>%1h %2m %3s</source>
        <translation>%1h %2m %3s</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="930"/>
        <source>One or more waveform record(s) for this session had faulty source data. Some waveform overlay points may not match up correctly.</source>
        <translation>Een of meer golfvorm rekord(s) vir die sessie het foutiewe data. Sommige van die golfvorm punte mag dalk nie reg oplyn nie.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1539"/>
        <source>No data is available for this day.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="858"/>
        <source>CPAP Sessions</source>
        <translation>CPAP Sessies</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="864"/>
        <source>Sleep Stage Sessions</source>
        <translation>Slaapvlak Sessies</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="867"/>
        <source>Position Sensor Sessions</source>
        <translation>Posisie Sensor Sessies</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="872"/>
        <source>Unknown Session</source>
        <translation>Onbekende Sessie</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="942"/>
        <source>Machine Settings</source>
        <translation>Masjien Instellings</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="946"/>
        <source>&lt;b&gt;Please Note:&lt;/b&gt; All settings shown below are based on assumptions that nothing&apos;s changed since previous days.</source>
        <translation>&lt;b&gt;Neem asb Kennis:&lt;/b&gt; Alle waardes hieronder vertoon is gebaseer op die aanname dat niks verander het van vorige dae af nie.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="948"/>
        <source>Machine Settings Unavailable</source>
        <translation>Masjien Instellings nie beskikbaar nie</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1094"/>
        <source>Model %1 - %2</source>
        <translation>Model %1 - %2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1099"/>
        <source>PAP Mode: %1</source>
        <translation>PAP Mode: %1</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1103"/>
        <source>(Mode/Pressure settings are guessed on this day.)</source>
        <translatorcomment>**Possible typo: should the full stop not rather be outside the parenthesis as per the translation?</translatorcomment>
        <translation>(Mode/Druk instellings word geraai op hierdie dag.)</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1212"/>
        <source>This day just contains summary data, only limited information is available.</source>
        <translation>Hierdie dag bevat opsommende data, slegs beperkte informasie is beskikbaar.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1238"/>
        <source>Total ramp time</source>
        <translation>Totale stygtyd</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1242"/>
        <source>Time outside of ramp</source>
        <translation>Tyd buite styg</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1283"/>
        <source>Start</source>
        <translation>Begin</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1283"/>
        <source>End</source>
        <translation>Einde</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1485"/>
        <source>Unable to display Pie Chart on this system</source>
        <translation>Kan nie die Sirkelgrafiek op hierdie stelsel vertoon nie</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1517"/>
        <source>Sorry, this machine only provides compliance data.</source>
        <translation>Jammer, hierdie masjien verskaf slegs voldoenigsdata.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1536"/>
        <source>&quot;Nothing&apos;s here!&quot;</source>
        <translation>&quot;Hier is niks!&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1068"/>
        <source>Oximeter Information</source>
        <translation>Oximeter Informasie</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1072"/>
        <source>SpO2 Desaturations</source>
        <translatorcomment>Baie slegte vertaling!!!</translatorcomment>
        <translation>SpO2 Desaturasies</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1073"/>
        <source>Pulse Change events</source>
        <translation>Polsslag Verandering Gebeurtenisse</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1074"/>
        <source>SpO2 Baseline Used</source>
        <translation>SpO2 Basislyn Gebruik</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1145"/>
        <source>%1%2</source>
        <translation>%1%2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1140"/>
        <source>Statistics</source>
        <translation>Staistieke</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1222"/>
        <source>Total time in apnea</source>
        <translation>Totale tyd in apneë</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1232"/>
        <source>Time over leak redline</source>
        <translation>Tyd oor lek rooilyn</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1428"/>
        <source>BRICK! :(</source>
        <translatorcomment>Moontlik verkeerd!</translatorcomment>
        <translation>BAKSTEEN! :(</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1472"/>
        <source>Event Breakdown</source>
        <translation>Gebeurtenis Afbraak</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1507"/>
        <source>Sessions all off!</source>
        <translation>Sessies is almal af!</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1509"/>
        <source>Sessions exist for this day but are switched off.</source>
        <translation>Sessies bestaan vir hierdie dag maar is afgeskakel.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1512"/>
        <source>Impossibly short session</source>
        <translation>Onmoontlike kort sessie</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1513"/>
        <source>Zero hours??</source>
        <translation>Nul ure??</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1516"/>
        <source>BRICK :(</source>
        <translatorcomment>Moontlik verkeerde vertaling</translatorcomment>
        <translation>BAKSTEEN :(</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1518"/>
        <source>Complain to your Equipment Provider!</source>
        <translation>Bring dit onder die aandag van die verskaffer van u toerusting!</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1831"/>
        <source>Pick a Colour</source>
        <translation>Kies &apos;n Kleur</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2092"/>
        <source>This bookmarked is in a currently disabled area..</source>
        <translation>Hierdie boekmerk is tans in &apos;n gedeaktiveerde area.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2135"/>
        <source>Bookmark at %1</source>
        <translation>Boekmerk by %1</translation>
    </message>
</context>
<context>
    <name>ExportCSV</name>
    <message>
        <location filename="../oscar/exportcsv.ui" line="14"/>
        <source>Export as CSV</source>
        <translation>Stoor as CSV</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="24"/>
        <source>Dates:</source>
        <translation>Datums:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="37"/>
        <source>Resolution:</source>
        <translation>Resolusie:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="46"/>
        <source>Details</source>
        <translation>Details</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="53"/>
        <source>Sessions</source>
        <translation>Sessies</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="60"/>
        <source>Daily</source>
        <translation>Daagliks</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="85"/>
        <source>Filename:</source>
        <translation>Lêernaam:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="107"/>
        <source>Cancel</source>
        <translation>Kanselleer</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="114"/>
        <source>Export</source>
        <translation>Voer uit</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="131"/>
        <source>Start:</source>
        <translation>Begin:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="154"/>
        <source>End:</source>
        <translation>Einde:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="192"/>
        <source>Quick Range:</source>
        <translation>Vinnige gedeelte:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="200"/>
        <location filename="../oscar/exportcsv.cpp" line="60"/>
        <location filename="../oscar/exportcsv.cpp" line="122"/>
        <source>Most Recent Day</source>
        <translation>Mees Onlangse Dag</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="205"/>
        <location filename="../oscar/exportcsv.cpp" line="125"/>
        <source>Last Week</source>
        <translation>Laasweek</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="210"/>
        <location filename="../oscar/exportcsv.cpp" line="128"/>
        <source>Last Fortnight</source>
        <translation>Laaste Twee Weke</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="215"/>
        <location filename="../oscar/exportcsv.cpp" line="131"/>
        <source>Last Month</source>
        <translation>Laaste Maand</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="220"/>
        <location filename="../oscar/exportcsv.cpp" line="134"/>
        <source>Last 6 Months</source>
        <translation>Laaste 6 Maande</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="225"/>
        <location filename="../oscar/exportcsv.cpp" line="137"/>
        <source>Last Year</source>
        <translation>Laaste Jaar</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="230"/>
        <location filename="../oscar/exportcsv.cpp" line="119"/>
        <source>Everything</source>
        <translation>Alles</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="235"/>
        <location filename="../oscar/exportcsv.cpp" line="108"/>
        <source>Custom</source>
        <translation>Eiedoelige</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="73"/>
        <source>OSCAR_</source>
        <translation>OSCAR_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="76"/>
        <source>Details_</source>
        <translation>Details_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="78"/>
        <source>Sessions_</source>
        <translation>Sessies_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="80"/>
        <source>Summary_</source>
        <translation>Opsomming_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="87"/>
        <source>Select file to export to</source>
        <translation>Kies lêer om na uit te voer</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="88"/>
        <source>CSV Files (*.csv)</source>
        <translation>CSV Lêers (*.csv)</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <source>DateTime</source>
        <translation>DatumTyd</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>Session</source>
        <translation>Sessie</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <source>Event</source>
        <translation>Gebeurtenis</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <source>Data/Duration</source>
        <translation>Data/Tydsduur</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <source>Session Count</source>
        <translation>Sessie Aantal</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>Start</source>
        <translation>Begin</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>End</source>
        <translation>Einde</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="207"/>
        <location filename="../oscar/exportcsv.cpp" line="210"/>
        <source>Total Time</source>
        <translation>Totale Tyd</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="207"/>
        <location filename="../oscar/exportcsv.cpp" line="210"/>
        <source>AHI</source>
        <translation>AHI</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="214"/>
        <source> Count</source>
        <translation> Aantal</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="222"/>
        <source>%1% </source>
        <translation>%1% </translation>
    </message>
</context>
<context>
    <name>FPIconLoader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.cpp" line="139"/>
        <source>Import Error</source>
        <translation>Intrek Fout</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.cpp" line="140"/>
        <source>This Machine Record cannot be imported in this profile.</source>
        <translation>Hierdie Masjien Rekord kan nie in hierdie profiel ingetrek word nie.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.cpp" line="140"/>
        <source>The Day records overlap with already existing content.</source>
        <translation>Die Dag rekords oorvleuel bestaande inhoud.</translation>
    </message>
</context>
<context>
    <name>Help</name>
    <message>
        <location filename="../oscar/help.ui" line="20"/>
        <source>Form</source>
        <translation>Vorm</translation>
    </message>
    <message>
        <location filename="../oscar/help.ui" line="92"/>
        <source>Hide this message</source>
        <translation>Verberg hierdie boodskap</translation>
    </message>
    <message>
        <location filename="../oscar/help.ui" line="198"/>
        <source>Search Topic:</source>
        <translation>Soek Onderwerp:</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="58"/>
        <source>Help Files are not yet available for %1 and will display in %2.</source>
        <translation>Hulp boodskappe is nog nie beskikbaar vir %1 en sal vertoon in %2.</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="68"/>
        <source>Help files do not appear to be present.</source>
        <translation>Dit lyk nie of hulplêers bskikbaar is nie.</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="84"/>
        <source>HelpEngine did not set up correctly</source>
        <translation>Dei Hulpengine het nie reg opgestel nie</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="99"/>
        <source>HelpEngine could not register documentation correctly.</source>
        <translation>Die Hulpengine het nie reg geregistreer nie.</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="110"/>
        <source>Contents</source>
        <translation>Inhoud</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="111"/>
        <source>Index</source>
        <translation>Indeks</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="115"/>
        <source>Search</source>
        <translation>Soek</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="125"/>
        <source>No documentation available</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="213"/>
        <source>Please wait a bit.. Indexing still in progress</source>
        <translation>Wag asseblief... Indeksering is in proses</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="239"/>
        <source>No</source>
        <translation>Nee</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="240"/>
        <source>%1 result(s) for &quot;%2&quot;</source>
        <translation>%1 resultaat(e) vir &quot;%2&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="241"/>
        <source>clear</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MD300W1Loader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.cpp" line="165"/>
        <source>Could not find the oximeter file:</source>
        <translation>Kon nie die oximeter lêer vind nie:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.cpp" line="171"/>
        <source>Could not open the oximeter file:</source>
        <translation>Kon nie die oximeter lêer oopmaak nie:</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../oscar/mainwindow.ui" line="942"/>
        <source>&amp;Statistics</source>
        <translation>&amp;Statistieke</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="996"/>
        <source>Report Mode</source>
        <translation>Verslag Mode</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1003"/>
        <source>Standard</source>
        <translation>Standaard</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1013"/>
        <source>Monthly</source>
        <translation>Maandelikse</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1020"/>
        <source>Date Range</source>
        <translation>Datum Bestek</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1391"/>
        <source>Statistics</source>
        <translation>Statistieke</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1441"/>
        <source>Daily</source>
        <translation>Daagliks</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1485"/>
        <source>Overview</source>
        <translation>Oorsig</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1529"/>
        <location filename="../oscar/mainwindow.cpp" line="1112"/>
        <source>Oximetry</source>
        <translation>Oximetrie</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1579"/>
        <source>Import</source>
        <translation>Intrek</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1629"/>
        <source>Help</source>
        <translation>Help</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2805"/>
        <source>&amp;File</source>
        <translation>&amp;Lêer</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2830"/>
        <source>&amp;View</source>
        <translation>&amp;Vertoon</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2848"/>
        <source>&amp;Help</source>
        <translation>&amp;Help</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2864"/>
        <source>&amp;Data</source>
        <translation>&amp;Data</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2868"/>
        <source>&amp;Advanced</source>
        <translation>&amp;Gevorderd</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2909"/>
        <source>&amp;Import SDcard Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2980"/>
        <source>&amp;Maximize Toggle</source>
        <translation>&amp;Maksimiseer Skakel</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3185"/>
        <source>Report an Issue</source>
        <translation>Rapporteer &apos;n Probleem</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2888"/>
        <source>Rebuild CPAP Data</source>
        <translation>Herbou CPAP Data</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2917"/>
        <source>&amp;Preferences</source>
        <translation>&amp;Voorkeure</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2922"/>
        <source>&amp;Profiles</source>
        <translation>&amp;Profiele</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2972"/>
        <source>&amp;About OSCAR</source>
        <translation>&amp;Rakende OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3170"/>
        <source>Show Performance Information</source>
        <translation>Vertoon Verrigting Informasie</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3175"/>
        <source>CSV Export Wizard</source>
        <translation>CSV Uitvoer Helper</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3180"/>
        <source>Export for Review</source>
        <translation>Uitvoer vir Hersiening</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="103"/>
        <source>E&amp;xit</source>
        <translation>E&amp;xit</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2927"/>
        <source>Exit</source>
        <translation>Gaan Uit</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2932"/>
        <source>View &amp;Daily</source>
        <translation>Besigtig &amp;Daagliks</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2940"/>
        <source>View &amp;Overview</source>
        <translation>Vertoon &amp;Oorsig</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2948"/>
        <source>View &amp;Welcome</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2956"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2967"/>
        <source>Use &amp;AntiAliasing</source>
        <translation>Gebruik &amp;AntiAliasering</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2991"/>
        <source>Show Debug Pane</source>
        <translation>Vertoon Ontfouting Paneel</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2999"/>
        <source>&amp;Reset Graph Layout</source>
        <translation>He&amp;rstel Grafiese Uitleg</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3004"/>
        <source>Take &amp;Screenshot</source>
        <translation>Neem &amp;Skermskoot</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3012"/>
        <source>O&amp;ximetry Wizard</source>
        <translation>O&amp;ximetrie Helper</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3020"/>
        <source>Print &amp;Report</source>
        <translation>D&amp;ruk Verslag</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3025"/>
        <source>&amp;Edit Profile</source>
        <translation>V&amp;erander Profiel</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3154"/>
        <source>Daily Calendar</source>
        <translation>Daaglikse Kalender</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3162"/>
        <source>Backup &amp;Journal</source>
        <translation>Rugsteun &amp;Joernaal</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3030"/>
        <source>Online Users &amp;Guide</source>
        <translation>Aanlyn &amp;Gebruikersgids</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3035"/>
        <source>&amp;Frequently Asked Questions</source>
        <translation>Gereelde &amp;Vrae</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3040"/>
        <source>&amp;Automatic Oximetry Cleanup</source>
        <translation>&amp;Outomatiese Oximetry Skoonmaak</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3121"/>
        <source>Toggle &amp;Line Cursor</source>
        <translation>Skakel &amp;Lyn Wyser</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3045"/>
        <source>Change &amp;User</source>
        <translation>Verander &amp;Gebruiker</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3050"/>
        <source>Purge &amp;Current Selected Day</source>
        <translation>Wis &amp;Huidige Gekose Day</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3061"/>
        <source>Right &amp;Sidebar</source>
        <translation>Regterkantste &amp;Kantlyn</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3140"/>
        <source>Daily Sidebar</source>
        <translation>Daaglikse Kantlyn</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3069"/>
        <source>View S&amp;tatistics</source>
        <translation>Vertoon &amp;Statistieke</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1320"/>
        <source>Navigation</source>
        <translation>Navigasie</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="441"/>
        <source>OSCAR</source>
        <translation>OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1751"/>
        <source>Bookmarks</source>
        <translation>Boekmerke</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2746"/>
        <source>Records</source>
        <translation>Rekords</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2878"/>
        <source>Purge ALL CPAP Data</source>
        <translation>Wis ALLE CPAP Data</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2809"/>
        <source>Exp&amp;ort Data</source>
        <translation>V&amp;oer Data Uit</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1347"/>
        <source>Profiles</source>
        <translation>Profiele</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2872"/>
        <source>Purge Oximetry Data</source>
        <translation>Wis Oximetrie Data</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3072"/>
        <location filename="../oscar/mainwindow.ui" line="3075"/>
        <source>View Statistics</source>
        <translation>Vertoon Statistieke</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3083"/>
        <source>Import &amp;ZEO Data</source>
        <translation>Voer &amp;ZEO Data In</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3088"/>
        <source>Import RemStar &amp;MSeries Data</source>
        <translation>Voer RemStar &amp;MReeks Data In</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3093"/>
        <source>Sleep Disorder Terms &amp;Glossary</source>
        <translation>Slaapafwyking &amp;Oorsig van Terme</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3098"/>
        <source>Change &amp;Language</source>
        <translation>Verander &amp;Taal</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3103"/>
        <source>Change &amp;Data Folder</source>
        <translation>Verander &amp;Data Vouer</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3108"/>
        <source>Import &amp;Somnopose Data</source>
        <translation>Voer &amp;Somnopose Data In</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3113"/>
        <source>Current Days</source>
        <translation>Huidige Dae</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="522"/>
        <source>Profile</source>
        <translation>Profiel</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="501"/>
        <location filename="../oscar/mainwindow.cpp" line="2237"/>
        <source>Welcome</source>
        <translation>Welkom</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="100"/>
        <source>&amp;About</source>
        <translation>&amp;Rakende</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="703"/>
        <location filename="../oscar/mainwindow.cpp" line="1932"/>
        <source>Please wait, importing from backup folder(s)...</source>
        <translation>Wag asseblief, besig om data van rugsteun vouer af te laai...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="648"/>
        <source>Import Problem</source>
        <translation>Invoer Probleem</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="804"/>
        <source>Please insert your CPAP data card...</source>
        <translation>Sit asseblief u CPAP data kaart in...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="877"/>
        <source>Access to Import has been blocked while recalculations are in progress.</source>
        <translation>Toegang tot Invoer is geblok terwyl berekeninge uitgevoer word.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="916"/>
        <source>CPAP Data Located</source>
        <translation>CPAP Data Gestoor</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="945"/>
        <source>Please remember to point the importer at the root folder or drive letter of your data-card, and not a subfolder.</source>
        <translation>Onthou asseblief om die invoerder te verwys na die boonste stoorplek of stoorplek letter van u data kaart en nie &apos;n sublêer nie.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="946"/>
        <source>Import Reminder</source>
        <translation>Invoer Herinnering</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1009"/>
        <source>Processing import list...</source>
        <translation>Prosesseer invoer lys...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1030"/>
        <source>Importing Data</source>
        <translation>Voer Data In</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1099"/>
        <source>This software has been created to assist you in reviewing the data produced by CPAP Machines, used in the treatment of various Sleep Disorders.</source>
        <translation>Hierdie sagteware is geskep om u by te staan om die data wat deur CPAP masjiene geskep word te hersien en te gebruik in die behandeling van verskeie Slaap Afwykings.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1103"/>
        <source>This is a beta release, some features may not yet behave as expected.</source>
        <translation>Hierdie is &apos;n beta vrystelling, sommige funksies mag dalk nog nie optree soos verwag nie.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1095"/>
        <source>Welcome to OSCAR</source>
        <translation>Welkom by OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1098"/>
        <source>About OSCAR</source>
        <translation>Rakende OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1101"/>
        <source>OSCAR has been designed by a software developer with personal experience with a sleep disorder, and shaped by the feedback of many other willing testers dealing with similar conditions.</source>
        <translation>OSCAR is ontwikkel deur &apos;n sagteware ontwikkelaar met persoonlike ondervinding met &apos;n slaapafwyking en gevorm deur die terugvoering van baie ander gewillige vrywilligers met soortgelyke toestande.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1104"/>
        <source>Please report any bugs you find to the OSCAR developer&apos;s group.</source>
        <translation>Rapporteer asseblief enige foute wat u vind aan die OSCAR ontwikkelaarsgroep.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1106"/>
        <source>Currenly supported machines:</source>
        <translation>Masjiene wat tans ondersteun word:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1107"/>
        <source>CPAP</source>
        <translation>CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1108"/>
        <source>Philips Respironics System One (CPAP Pro, Auto, BiPAP &amp; ASV models)</source>
        <translation>Philips Respironics System One (CPAP Pro, Auto, BiPAP &amp; ASV modelle)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1109"/>
        <source>ResMed S9 models (CPAP, Auto, VPAP)</source>
        <translation>ResMed S9 modelle (CPAP, Auto, VPAP)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1110"/>
        <source>DeVilbiss Intellipap (Auto)</source>
        <translation>DeVilbiss Intellipap (Auto)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1111"/>
        <source>Fisher &amp; Paykel ICON (CPAP, Auto)</source>
        <translation>Fisher &amp; Paykel ICON (CPAP, Auto)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1113"/>
        <source>Contec CMS50D+, CMS50E and CMS50F (not 50FW) Oximeters</source>
        <translation>Contec CMS50D+, CMS50E and CMS50F (not 50FW) Oximeters</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1114"/>
        <source>ResMed S9 Oximeter Attachment</source>
        <translation>ResMed S9 Oximeter Attachment</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1115"/>
        <source>Online Help Resources</source>
        <translation>Aanlyn Hulpbronne</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1116"/>
        <source>Note:</source>
        <translatorcomment>**Could also be: &quot;Neem kennis&quot; - confirm where used!</translatorcomment>
        <translation>Nota:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1117"/>
        <source>I don&apos;t recommend using this built in web browser to do any major surfing in, it will work, but it&apos;s mainly meant as a help browser.</source>
        <translation>Die ingeboude webleser word nie aanbeveel vir ernstige net navigasie nie. Dit sal werk, maar dit is hoofsaaklik bedoel as &apos;n hulpbron leser.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1119"/>
        <source>(It doesn&apos;t support SSL encryption, so it&apos;s not a good idea to type your passwords or personal details anywhere.)</source>
        <translation>(Dit ondersteun nie SSL enkodering nie, dus is dit nie so goeie idee om u wagwoord of enige persoonlike inligting op enige plek te verskaf nie.)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1134"/>
        <source>The release notes for this version can be found in the About OSCAR menu item.</source>
        <translation>Die vrystellingsnotas vir hierdie weergawe kan in die Rakende OSCAR kieslys gevind word.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1511"/>
        <source>The User&apos;s Guide will open in your default browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2310"/>
        <source>The Glossary will open in your default browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2542"/>
        <source>Export review is not yet implemented</source>
        <translation>Uitvoer oorsig is nog nie geimplementeer nie</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2557"/>
        <source>Reporting issues is not yet implemented</source>
        <translation>Rapportering is nog nie geimplementeer nie</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1132"/>
        <source>Further Information</source>
        <translation>Verdere Inligting</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1136"/>
        <source>Plus a few &lt;a href=&apos;qrc:/docs/usage.html&apos;&gt;usage notes&lt;/a&gt;, and some important information for Mac users.</source>
        <translation>Plus &apos;n paar &lt;a href=&apos;qrc:/docs/usage.html&apos;&gt;gebruiksnotas&lt;/a&gt;, en belangrike inligting vir Mac gebruikers.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1138"/>
        <source>About &lt;a href=&apos;http://en.wikipedia.org/wiki/Sleep_apnea&apos;&gt;Sleep Apnea&lt;/a&gt; on Wikipedia</source>
        <translation>Verwante &lt;a href=&apos;http://en.wikipedia.org/wiki/Sleep_apnea&apos;&gt;Slaap Apneë&lt;/a&gt; op Wikipedia</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1141"/>
        <source>Friendly forums to talk and learn about Sleep Apnea:</source>
        <translation>Vriendelike Forums om op te kommunikeer en te leer oor Slaap Apneë:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1142"/>
        <source>&lt;a href=&apos;http://www.cpaptalk.com&apos;&gt;CPAPTalk Forum&lt;/a&gt;,</source>
        <translation>&lt;a href=&apos;http://www.cpaptalk.com&apos;&gt;CPAPTalk Forum&lt;/a&gt;,</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1151"/>
        <source>Copyright:</source>
        <translation>Kopiereg:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1153"/>
        <source>License:</source>
        <translation>Lisensie:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1157"/>
        <source>DISCLAIMER:</source>
        <translation>VRYWARING:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1159"/>
        <source>This is &lt;font color=&apos;red&apos;&gt;&lt;u&gt;NOT&lt;/u&gt;&lt;/font&gt; medical software. This application is merely a data viewer, and no guarantee is made regarding accuracy or correctness of any calculations or data displayed.</source>
        <translation>Hierdie is &lt;font color=&apos;red&apos;&gt;&lt;u&gt;NIE&lt;/u&gt;&lt;/font&gt; mediese sagteware nie.Hierdie program is slegs &apos;n data vertoon en geen waarborg word verskaf rakende die akkuraatheid of korrektheid van enige berekeninge of data wat vertoon word nie.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1163"/>
        <source>Your doctor should always be your first and best source of guidance regarding the important matter of managing your health.</source>
        <translation>U dokter behoort altyd u eerste en beste bron van leiding te wees sover dit die bestuur van u gesondheid aangaan.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1165"/>
        <source>*** &lt;u&gt;Use at your own risk&lt;/u&gt; ***</source>
        <translation>*** &lt;u&gt;Gebruik op u eie risiko&lt;/u&gt; ***</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1767"/>
        <location filename="../oscar/mainwindow.cpp" line="1794"/>
        <source>If you can read this, the restart command didn&apos;t work. You will have to do it yourself manually.</source>
        <translation>Indien u hierdie kan lees, het die herbegin opdrag nie gewerk nie. U sal dit self handrolies moet uitvoer.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1907"/>
        <source>Are you sure you want to rebuild all CPAP data for the following machine:

</source>
        <translation>Is u seker dat u al die CPAP data vir die volgende masjien wil herbou:

</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1910"/>
        <source>Please note, that this could result in loss of data if OSCAR&apos;s backups have been disabled.</source>
        <translation>Neem asseblief kennis dat dit kan lei tot verlies in data as OSCAR se rugsteun afgeskakel is.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1917"/>
        <source>For some reason, OSCAR does not have any backups for the following machine:</source>
        <translation>Vir een of ander rede het OSCAR geen rugsteun vir die volgende masjien nie:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1972"/>
        <source>You are about to &lt;font size=+2&gt;obliterate&lt;/font&gt; OSCAR&apos;s machine database for the following machine:&lt;/p&gt;</source>
        <translation>U is op die punt om OSCAR se databasis vir die volgende masjien &lt;font size=+2&gt;te vernietig&lt;/font&gt;:&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2025"/>
        <source>A file permission error casued the purge process to fail; you will have to delete the following folder manually:</source>
        <translation>&apos;n Lêer toegang fout verhoed dat die proses uitgevoer word; u sal self die lêer moet uitvee:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2074"/>
        <source>No help is available.</source>
        <translation>Geen hulp is beskikbaar nie.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2318"/>
        <source>Donations are not implemented</source>
        <translation>Donanies is nog nie geimplementeer nie</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2520"/>
        <source>%1&apos;s Journal</source>
        <translation>%1 se Joernaal</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2522"/>
        <source>Choose where to save journal</source>
        <translation>Kies waar om u joernaal te stoor</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2522"/>
        <source>XML Files (*.xml)</source>
        <translation>XML Lêers (*.xml)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="238"/>
        <source>Help Browser</source>
        <translation>Hulp Leser</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="312"/>
        <source>%1 %2</source>
        <translation>%1 %2</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="483"/>
        <source>Loading profile &quot;%1&quot;</source>
        <translation>Laai profiel &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1291"/>
        <source>Please open a profile first.</source>
        <translation>Maak asb eers &apos;n profiel oop.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1919"/>
        <source>Provided you have made &lt;i&gt;your &lt;b&gt;own&lt;/b&gt; backups for ALL of your CPAP data&lt;/i&gt;, you can still complete this operation, but you will have to restore from your backups manually.</source>
        <translation>Gegewe dat u &lt;i&gt;u &lt;b&gt;eie&lt;/b&gt; rugsteun vir al u CPAP data gemaak het&lt;/i&gt;, kan u steeds hierdie aksie voltooi, maar u sal handmatig van u rugsteun af die data moet herstel.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1920"/>
        <source>Are you really sure you want to do this?</source>
        <translation>Is u regtig seker dat u dit wil doen?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1935"/>
        <source>Because there are no internal backups to rebuild from, you will have to restore from your own.</source>
        <translation>Omdat daar nie interne rugsteun is om vanaf te herbou nie, sal u moet herskep van u eie af.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1936"/>
        <source>Would you like to import from your own backups now? (you will have no data visible for this machine until you do)</source>
        <translation>Wil u invoer vanaf u eie rugsteun? (u sal geen data sigbaar hê vir hierdie masjien totdat u dit doen nie)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1974"/>
        <source>Note as a precaution, the backup folder will be left in place.</source>
        <translation>Neem kennis dat as &apos;n voorkomende maatreël, die rugsteun vouer in plek gelaat word.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1975"/>
        <source>Are you &lt;b&gt;absolutely sure&lt;/b&gt; you want to proceed?</source>
        <translation>Is u &lt;b&gt;doodseker&lt;/b&gt; dat u wil voortgaan?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2459"/>
        <source>Are you sure you want to delete oximetry data for %1</source>
        <translation>Is u seker dat u die oximeter data wil uitvee vir %1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2461"/>
        <source>&lt;b&gt;Please be aware you can not undo this operation!&lt;/b&gt;</source>
        <translation>&lt;b&gt;Wees asseblief bewus dat u nie hierdie aksie kan omkeer nie!&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2482"/>
        <source>Select the day with valid oximetry data in daily view first.</source>
        <translation>Kies eers die dag met geldige oximeter data in die daaglikse vertoon.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="644"/>
        <source>Imported %1 CPAP session(s) from

%2</source>
        <translation>%1 CPAP sessie(s) ingevoer van

%2</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="644"/>
        <source>Import Success</source>
        <translation>Suksesvolle Invoer</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="646"/>
        <source>Already up to date with CPAP data at

%1</source>
        <translation>Alreeds op datum met CPAP data by

%1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="646"/>
        <source>Up to date</source>
        <translation>Op datum</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="648"/>
        <source>Couldn&apos;t find any valid Machine Data at

%1</source>
        <translation>Kon geen geldige Masjien Data vind by

%1 nie</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="811"/>
        <source>Choose a folder</source>
        <translation>Kies &apos;n vouer</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="873"/>
        <source>No profile has been selected for Import.</source>
        <translation>Geen profiel is gekies vir Invoer nie.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="881"/>
        <source>Import is already running in the background.</source>
        <translation>Reeds besig om in die agtergrond in te voer.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="909"/>
        <source>A %1 file structure for a %2 was located at:</source>
        <translation>&apos;n %1 vouer struktuur vir &apos;n %2 is gevind by:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="911"/>
        <source>A %1 file structure was located at:</source>
        <translation>&apos;n %1 vouer struktuur is gevind by:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="917"/>
        <source>Would you like to import from this location?</source>
        <translation>Wil u invoer van hierdie ligging af?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="920"/>
        <source>Specify</source>
        <translation>Spesifiseer</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1143"/>
        <source>&lt;a href=&apos;http://www.apneaboard.com/forums/&apos;&gt;Apnea Board&lt;/a&gt;</source>
        <translation>&lt;a href=&apos;http://www.apneaboard.com/forums/&apos;&gt;Apnea Board&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1154"/>
        <source>This software is released freely under the &lt;a href=&quot;qrc:/COPYING&quot;&gt;GNU Public License version 3&lt;/a&gt;.</source>
        <translation>Hierdie sagteware word gratis vrygestel onder die &lt;a href=&quot;qrc:/COPYING&quot;&gt;GNU Public License version 3&lt;/a&gt;.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1161"/>
        <source>The authors will NOT be held liable by anyone who harms themselves or others by use or misuse of this software.</source>
        <translation>Die outeurs sal NIE aanspreeklik gehou word deur enigeen wat hulself of ander skade aanrig deur gebruik of misbruik van hierdie sagteware nie.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1296"/>
        <source>Access to Preferences has been blocked until recalculation completes.</source>
        <translation>Toegang tot Verrigting is geblok todat die herberekening voltooi is.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1333"/>
        <source>Updates are not yet implemented</source>
        <translation>Opdaterings is nog nie geimplementeer nie</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1393"/>
        <source>There was an error saving screenshot to file &quot;%1&quot;</source>
        <translation>Daar was &apos;n fout tydens stoor van die skermskoot na lêer &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1395"/>
        <source>Screenshot saved to file &quot;%1&quot;</source>
        <translation>Skermskoot gestoor na lêer &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1519"/>
        <source>The FAQ is not yet implemented</source>
        <translation>Die FAQ is nog nie geimplementeer nie</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1766"/>
        <location filename="../oscar/mainwindow.cpp" line="1793"/>
        <source>Gah!</source>
        <translation>Gmmmfff!</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2269"/>
        <source>There was a problem opening ZEO File: </source>
        <translation>Daar was &apos;n probleem met die oopmaak van ZEO lêer: </translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2273"/>
        <source>Zeo CSV Import complete</source>
        <translation>Zeo CSV Invoer voltooi</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2295"/>
        <source>There was a problem opening MSeries block File: </source>
        <translation>Daar was &apos;n probleem met die oopmaak van MSeries blok Lêer: </translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2299"/>
        <source>MSeries Import complete</source>
        <translation>MSeries Invoer voltooi</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2356"/>
        <source>There was a problem opening Somnopose Data File: </source>
        <translation>Daar was &apos;n probleem met die oopmaak van Somnopose Data Lêer: </translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2360"/>
        <source>Somnopause Data Import complete</source>
        <translation>Somnopose Data Invoer voltooi</translation>
    </message>
</context>
<context>
    <name>MinMaxWidget</name>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1964"/>
        <source>Auto-Fit</source>
        <translation>Auto Pas</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1965"/>
        <source>Defaults</source>
        <translation>Verstekwaardes</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1966"/>
        <source>Override</source>
        <translation>Oorskryf</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1967"/>
        <source>The Y-Axis scaling mode, &apos;Auto-Fit&apos; for automatic scaling, &apos;Defaults&apos; for settings according to manufacturer, and &apos;Override&apos; to choose your own.</source>
        <translation>Die Y-As skalering mode, &apos;Auto-Pas&apos; vir outomatiese skalering, &apos;Verstek&apos; vir waardes per vervaardiger en &quot;Oorskryf&apos; om u eie te kies.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1973"/>
        <source>The Minimum Y-Axis value.. Note this can be a negative number if you wish.</source>
        <translation>Die Minimum Y-as waarde. Hierdie kan &apos;n negatiewe getal wees as u so verkies.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1974"/>
        <source>The Maximum Y-Axis value.. Must be greater than Minimum to work.</source>
        <translation>Die Maksimum Y-As waarde. Moet groter wees as die Minimum om te werk.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2009"/>
        <source>Scaling Mode</source>
        <translation>Skalering Mode</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2031"/>
        <source>This button resets the Min and Max to match the Auto-Fit</source>
        <translation>Hierdie herstel die Min en Maks om soos die Auto Pas te wees</translation>
    </message>
</context>
<context>
    <name>NewProfile</name>
    <message>
        <location filename="../oscar/newprofile.ui" line="14"/>
        <source>Edit User Profile</source>
        <translation>Redigeer Gebruiker Profiel</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="70"/>
        <source>I agree to all the conditions above.</source>
        <translation>Ek stem saam met al die voorwaardes hierbo.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="111"/>
        <source>User Information</source>
        <translation>Gebruikersinligting</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="137"/>
        <source>User Name</source>
        <translation>Naam van Gebruiker</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="152"/>
        <source>Keep the kids out.. Nothing more.. This isn&apos;t meant to be uber security.</source>
        <translation>Hou die knders uit.. Niks meer nie.. Hierdie is nie veronderstel om uber sekuriteit te wees nie.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="155"/>
        <source>Password Protect Profile</source>
        <translation>Beskerm Profiel met Wagwoord</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="182"/>
        <source>Password</source>
        <translation>Wagwoord</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="196"/>
        <source>...twice...</source>
        <translation>...tweekeer...</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="213"/>
        <source>Locale Settings</source>
        <translation>Ligging Opstellings</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="285"/>
        <source>Country</source>
        <translation>Land</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="253"/>
        <source>TimeZone</source>
        <translation>Tydzone</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="54"/>
        <source>about:blank</source>
        <translation>about:blank</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="240"/>
        <source>DST Zone</source>
        <translation>DST Zone</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="320"/>
        <source>Personal Information (for reports)</source>
        <translation>Persoonlike Inligting (vir verslae)</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="344"/>
        <source>First Name</source>
        <translation>Naam</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="354"/>
        <source>Last Name</source>
        <translation>Van</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="364"/>
        <source>It&apos;s totally ok to fib or skip this, but your rough age is needed to enhance accuracy of certain calculations.</source>
        <translation>Dis aanvaarbaar om hierdie te los, maar u benaderde ouderdom word benodig om die akkuraatheid van sekere berekeninge te verbeter.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="367"/>
        <source>D.O.B.</source>
        <translation>Geboortedatum</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="383"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Biological (birth) gender is sometimes needed to enhance the accuracy of a few calculations, feel free to leave this blank and skip any of them.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Biologiese (geboorte) geslag word somtyds benodig om die akkuraatheid van &apos;n paar berekeninge te verbeter, maar voel vry om dit oop te los.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="386"/>
        <source>Gender</source>
        <translation>Geslag</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="399"/>
        <source>Male</source>
        <translation>Manlik</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="404"/>
        <source>Female</source>
        <translation>Vroulik</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="420"/>
        <source>Height</source>
        <translation>Lengte</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="459"/>
        <source>metric</source>
        <translation>metrieke</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="464"/>
        <source>archiac</source>
        <translatorcomment>**IS THE ENGLISH MISSPELLED?!? -&gt; ARCHAIC</translatorcomment>
        <translation>argaïese</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="477"/>
        <source>Contact Information</source>
        <translation>Kontak Inligting</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="504"/>
        <location filename="../oscar/newprofile.ui" line="779"/>
        <source>Address</source>
        <translation>Adres</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="521"/>
        <location filename="../oscar/newprofile.ui" line="810"/>
        <source>Email</source>
        <translation>Epos</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="531"/>
        <location filename="../oscar/newprofile.ui" line="800"/>
        <source>Phone</source>
        <translation>Telefoon</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="576"/>
        <source>CPAP Treatment Information</source>
        <translation>CPAP Behandeling Inligting</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="603"/>
        <source>Date Diagnosed</source>
        <translation>Datum Gediagnoseer</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="617"/>
        <source>Untreated AHI</source>
        <translation>Onbehandelde AHI</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="631"/>
        <source>CPAP Mode</source>
        <translation>CPAP Mode</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="639"/>
        <source>CPAP</source>
        <translation>CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="644"/>
        <source>APAP</source>
        <translation>APAP</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="649"/>
        <source>Bi-Level</source>
        <translation>Bi-Level</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="654"/>
        <source>ASV</source>
        <translation>ASV</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="662"/>
        <source>RX Pressure</source>
        <translation>RX Druk</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="708"/>
        <source>Doctors / Clinic Information</source>
        <translation>Dokter / Kliniek Inligting</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="735"/>
        <source>Doctors Name</source>
        <translation>Dokter se Naam</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="752"/>
        <source>Practice Name</source>
        <translation>Praktyk Naam</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="762"/>
        <source>Patient ID</source>
        <translation>Pasiënt ID</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="877"/>
        <source>OSCAR</source>
        <translation>OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="955"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Kanselleer</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="971"/>
        <source>&amp;Back</source>
        <translation>&amp;Terug</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="987"/>
        <location filename="../oscar/newprofile.cpp" line="274"/>
        <location filename="../oscar/newprofile.cpp" line="283"/>
        <source>&amp;Next</source>
        <translation>&amp;Volgende</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="61"/>
        <source>Select Country</source>
        <translation>Kies Land</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="111"/>
        <source>This software is being designed to assist you in reviewing the data produced by your CPAP machines and related equipment.</source>
        <translation>Hierdie sagteware is ontwerp om u te help om die data deur u CPAP masjiene en verwante toerusting te hersien.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="116"/>
        <source>PLEASE READ CAREFULLY</source>
        <translation>LEES ASSEBLIEF NOUKEURIG</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="120"/>
        <source>Accuracy of any data displayed is not and can not be guaranteed.</source>
        <translation>Akkuraatheid van enige data vertoon is nie en kan nie gewaarborg word nie.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="122"/>
        <source>Any reports generated are for PERSONAL USE ONLY, and NOT IN ANY WAY fit for compliance or medical diagnostic purposes.</source>
        <translation>Enige verslae gegenereer is vir PERSOONLIKE GEBRUIK ALLENLIK en nie OP ENIGE MANIER geskik vir voldoening of mediese diagnose doeleindes nie.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="129"/>
        <source>Use of this software is entirely at your own risk.</source>
        <translation>Gebruik van hierdie sagteware is geheel en al op u eie risiko.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="109"/>
        <source>Welcome to the Open Source CPAP Analysis Reporter</source>
        <translation>Welkom by die  Open Source CPAP Analysis Reporter</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="114"/>
        <source>OSCAR has been released freely under the &lt;a href=&apos;qrc:/COPYING&apos;&gt;GNU Public License v3&lt;/a&gt;, and comes with no warranty, and without ANY claims to fitness for any purpose.</source>
        <translation>OSCAR is gratis vrygestel onder die &lt;a href=&apos;qrc:/COPYING&apos;&gt;GNU Public License v3&lt;/a&gt;, het geen waarborg nie, en sonder enige stelling van geskiktheid vir enige doel.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="117"/>
        <source>OSCAR is intended merely as a data viewer, and definitely not a substitute for competent medical guidance from your Doctor.</source>
        <translation>OSCAR is slegs bedoel om data te besigtig en nie as &apos;n vervanging vir bevoegde mediese leidingvan u dokter af nie.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="125"/>
        <source>The authors will not be held liable for &lt;u&gt;anything&lt;/u&gt; related to the use or misuse of this software.</source>
        <translation>Die outeurs sal nie verantwoordelik gehou word vir &lt;u&gt;enine iets&lt;/u&gt; rakende die gebruik of miskruik van hierdie sagteware nie.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="132"/>
        <source>OSCAR is copyright &amp;copy;2011-2018 Mark Watkins and portions &amp;copy;2019 Nightowl Software</source>
        <translation>OSCAR is kopiereg &amp;copy;2011-2018 Mark Watkins en gedeeltes &amp;copy;2019 Nightowl Software</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="155"/>
        <source>Please provide a username for this profile</source>
        <translation>Verskaf asseblief &apos;n gebruikersnaam vir hierdie profiel</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="165"/>
        <source>Passwords don&apos;t match</source>
        <translation>Wagwoorde is nie dieselfde nie</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="195"/>
        <source>Profile Changes</source>
        <translation>Profiel Veranderings</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="195"/>
        <source>Accept and save this information?</source>
        <translation>Aanvaar en stoor hierdie inligting?</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="272"/>
        <source>&amp;Finish</source>
        <translation>&amp;Maak klaar</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="448"/>
        <source>&amp;Close this window</source>
        <translation>&amp;Maak hierdie venster toe</translation>
    </message>
</context>
<context>
    <name>Overview</name>
    <message>
        <location filename="../oscar/overview.ui" line="14"/>
        <source>Form</source>
        <translation>Vorm</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="68"/>
        <source>Range:</source>
        <translation>Bereik:</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="82"/>
        <source>Last Week</source>
        <translation>Laaste Week</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="87"/>
        <source>Last Two Weeks</source>
        <translation>Laaste Twee Weke</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="92"/>
        <source>Last Month</source>
        <translation>Laaste Maand</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="97"/>
        <source>Last Two Months</source>
        <translation>Laaste Twee Maande</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="102"/>
        <source>Last Three Months</source>
        <translation>Laaste Drie Maande</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="107"/>
        <source>Last 6 Months</source>
        <translation>Laaste 6 Maande</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="112"/>
        <source>Last Year</source>
        <translation>Laaste Jaar</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="117"/>
        <source>Everything</source>
        <translation>Alles</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="122"/>
        <source>Custom</source>
        <translation>Eie</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="130"/>
        <source>Start:</source>
        <translation>Begin:</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="153"/>
        <source>End:</source>
        <translation>Einde:</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="176"/>
        <source>Reset view to selected date range</source>
        <translation>Herstel vertoon na gekose data bereik</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="195"/>
        <location filename="../oscar/overview.ui" line="244"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="225"/>
        <source>Toggle Graph Visibility</source>
        <translation>Skakel Grafiek Sigbaarheid</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="260"/>
        <source>Drop down to see list of graphs to switch on/off.</source>
        <translation>Breek af om lys van grafieke te sien om aan/af te skakel.</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="264"/>
        <source>Graphs</source>
        <translation>Grafiek</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="158"/>
        <source>Respiratory
Disturbance
Index</source>
        <translation>Respiratoriese
Versteuring
Indeks</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="160"/>
        <source>Apnea
Hypopnea
Index</source>
        <translation>Apneë
Hypopneë
Indeks</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="166"/>
        <source>Usage</source>
        <translation>Gebruik</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="166"/>
        <source>Usage
(hours)</source>
        <translation>Gebruik
(ure)</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="169"/>
        <source>Session Times</source>
        <translation>Sessie Tye</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="177"/>
        <source>Total Time in Apnea</source>
        <translation>Totale Tyd in Apneë</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="177"/>
        <source>Total Time in Apnea
(Minutes)</source>
        <translation>Totale Tyd in Apneë
(Minute)</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="211"/>
        <source>Body
Mass
Index</source>
        <translation>Liggaam
Massa
Indeks</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="212"/>
        <source>How you felt
(0-10)</source>
        <translation>Hoe u gevoel het
(0-10)</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="502"/>
        <source>Show all graphs</source>
        <translation>Vertoon alle grafieke</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="515"/>
        <source>Hide all graphs</source>
        <translation>Steek alle grafieke weg</translation>
    </message>
</context>
<context>
    <name>OximeterImport</name>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialoog</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="61"/>
        <location filename="../oscar/oximeterimport.cpp" line="37"/>
        <source>Oximeter Import Wizard</source>
        <translation>Oximeter Invoer Helper</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="768"/>
        <source>Skip this page next time.</source>
        <translation>Gaan volgende keer verby hierdie bladsy.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="866"/>
        <source>Where would you like to import from?</source>
        <translation>Vanwaar vir u invoer?</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="967"/>
        <source>CMS50E/F users, when importing directly, please don&apos;t select upload on your device until OSCAR prompts you to.</source>
        <translation>CMS50E/F gebruikers, wanneer direk ingetrek, moet asseblief nie oplaai kies op die toerustingvoordat OSCAR daarvoor vra nie.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1004"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If enabled, OSCAR will automatically reset your CMS50&apos;s internal clock using your computers current time.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Indien gekies, sal OSCAR outomaties u CMS50 se interne klok herstel na u rekenaar se interne klok toe.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1077"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option will erase the imported session from your oximeter after import has completed. &lt;/p&gt;&lt;p&gt;Use with caution,  becauseif something goes wrong before OSCAR saves your session, you won&apos;t get it back.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Hierdie keuse sal die ingetrekte sessie uitvee op die oximeter. &lt;/p&gt;&lt;p&gt;Gebruik versigtig,  want u sal dit nie kan terugkry indien iets verkeerd gaan voordat OSCAR u sessie gestoor het nie.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1106"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option allows you to import (via cable) from your oximeters internal recordings.&lt;/p&gt;&lt;p&gt;After selecting on this option, some oximeters will require you to do something in the devices menu to initiate the upload.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Hierdie opsie laat u toe om in te voer (dmv kabel) vanaf u oximete se interne opnames.&lt;/p&gt;&lt;p&gt;Nadat hierdie opsie gekies is, sal sekere oximeters vereis dat u iets doen in die toestel se kieskaart om die invoer te begin.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1148"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If you don&apos;t mind a being attached to a running computer overnight, this option provide a useful plethysomogram graph, which gives an indication of heart rhythm, on top of the normal oximetry readings.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Indien u nie omgee om aan &apos;n werkende rekenaar vas te wees oornag nie, verskaf hierdie opsie &apos;n bruikbare plethysomogram grafiek, wat &apos;n aanduiding gee van hartritme, sowel as die normale oximeter lesings.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1154"/>
        <source>Record attached to computer overnight (provides plethysomogram)</source>
        <translation>Neem op terwyl gekoppel aan rekenaar oornag (verskaf die plethysomogram)</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1187"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option allows you to import from data files created by software that came with your Pulse Oximeter, such as SpO2Review.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Hierdie opsie laat u toe om in te voer vanaf data lêers wat deur u Oximeter se sagteware,  soos SpO2Review, geskep is.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1193"/>
        <source>Import from a datafile saved by another program, like SpO2Review</source>
        <translation>Voer in van &apos;n data lêer af wat deur &apos;n ander program gestoor is, soos SpO2Review</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1272"/>
        <location filename="../oscar/oximeterimport.ui" line="1319"/>
        <source>Please connect your oximeter device</source>
        <translation>Koppel asseblief u oximeter toestel</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1290"/>
        <source>If you can read this, you likely have your oximeter type set wrong in preferences.</source>
        <translation>Indien u hierdie sien, het u waarskynlik verkeerde oximeter in u instellings opgestel.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1345"/>
        <source>Press Start to commence recording</source>
        <translation>Druk Begin om op te neem</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1391"/>
        <source>Show Live Graphs</source>
        <translation>Vertoon Deurlopende Grafieke</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1422"/>
        <location filename="../oscar/oximeterimport.ui" line="1676"/>
        <source>Duration</source>
        <translation>Tydsduur</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1458"/>
        <source>SpO2 %</source>
        <translation>SpO2 %</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1540"/>
        <source>Pulse Rate</source>
        <translation>Polstempo</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1632"/>
        <source>Multiple Sessions Detected</source>
        <translation>Veelvoudige Sessies Bespeur</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1671"/>
        <source>Start Time</source>
        <translation>Begintyd</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1681"/>
        <source>Details</source>
        <translation>Details</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1698"/>
        <source>Import Completed. When did the recording start?</source>
        <translation>Invoer voltooi. Wanneer is die opname begin?</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1722"/>
        <source>Day recording (normally would of) started</source>
        <translation>Dag opname (sou normaalweg) begin</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1768"/>
        <source>Oximeter Starting time</source>
        <translation>Oximeter Begintyd</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1780"/>
        <source>I want to use the time reported by my oximeter&apos;s built in clock.</source>
        <translation>Ek wil die tyd gebruik wat gerapporteer word deur my oximeter se ingeboude klok.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1796"/>
        <source>I started this oximeter recording at (or near) the same time as a session on my CPAP machine.</source>
        <translation>Ek het hierdie opname begin op (of naby) dieselfde tyd as &apos;n sessie op my CPAP masjien.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1857"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Note: Syncing to CPAP session starting time will always be more accurate.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Nota: Sinkronisasie met &apos;n CPAP sessie begintyd sal altyd meer akkuraat wees.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1878"/>
        <source>Choose CPAP session to sync to:</source>
        <translation>Kies CPAP sessie om mee te sinkroniseer:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1917"/>
        <location filename="../oscar/oximeterimport.ui" line="1956"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1998"/>
        <source>You can manually adjust the time here if required:</source>
        <translation>U ma handmatig die tyd hier wysig indien nodig:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2019"/>
        <source>HH:mm:ssap</source>
        <translation>HH:mm:ssap</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2116"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Kanselleer</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2097"/>
        <source>&amp;Information Page</source>
        <translation>&amp;Informasie Bladsy</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="823"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Please note: &lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Make sure your correct oximeter type is selected otherwise import will fail.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Neem asb kennis: &lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Maak seker dat u korrekte oximeter tipe gekies is, andersins sal die invoer faal.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="910"/>
        <source>Select Oximeter Type:</source>
        <translation>Kies Oximeter Tipe:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="921"/>
        <source>CMS50Fv3.7+/H/I, Pulox PO-400/500</source>
        <translation>CMS50Fv3.7+/H/I, Pulox PO-400/500</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="926"/>
        <source>CMS50D+/E/F, Pulox PO-200/300</source>
        <translation>CMS50D+/E/F, Pulox PO-200/300</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="931"/>
        <source>ChoiceMMed MD300W1</source>
        <translation>ChoiceMMed MD300W1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1007"/>
        <source>Set device date/time</source>
        <translation>Stel toestel datum/tyd</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1014"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Check to enable updating the device identifier next import, which is useful for those who have multiple oximeters lying around.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Merk om die toestel identifisering tydens die volgende invoer op te dateer, wat nuttig is vir diegene wat meer as een oximeter het.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1017"/>
        <source>Set device identifier</source>
        <translation>Stel toestel identifikasie</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1036"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Here you can enter a 7 character pet name for this oximeter.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Hier kan u &apos;n 7 karakter bynaam insit vir hierdie oximeter.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1080"/>
        <source>Erase session after successful upload</source>
        <translation>Vee sessie uit na suksesvolle oplaai</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1112"/>
        <source>Import directly from a recording on a device</source>
        <translation>Voer direk in vanaf &apos;n opname op &apos;n toestel</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1237"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Reminder for CPAP users: &lt;/span&gt;&lt;span style=&quot; color:#fb0000;&quot;&gt;Did you remember to import your CPAP sessions first?&lt;br/&gt;&lt;/span&gt;If you forget, you won&apos;t have a valid time to sync this oximetry session to.&lt;br/&gt;To a ensure good sync between devices, always try to start both at the same time.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Herinnering vir CPAP gebruikers: &lt;/span&gt;&lt;span style=&quot; color:#fb0000;&quot;&gt;Het u onthou om u CPAP sessies eers in te voer?&lt;br/&gt;&lt;/span&gt;Indien u vergeet, sal u nie &apos;n geldige tyd hê om hierdie oximeter sessie mee te sinkroniseer nie.&lt;br/&gt;Probeer altyd om albei op dieselfde tyd te begin om &apos;n goeie sinkronisasie tussen toestelle te verseker.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1648"/>
        <source>Please choose which one you want to import into OSCAR</source>
        <translation>Kies asseblief watter een u in OSCAR wil intrek</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1825"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;OSCAR needs a starting time to know where to save this oximetry session to.&lt;/p&gt;&lt;p&gt;Choose one of the following options:&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;OSCAR benodig &apos;n begintyd om te weet waar om die oximeter sessie te stoor.&lt;/p&gt;&lt;p&gt;Kies een van die volgende keuses:&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2135"/>
        <source>&amp;Retry</source>
        <translation>&amp;Probeer weer</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2154"/>
        <source>&amp;Choose Session</source>
        <translation>&amp;Kies Sessie</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2173"/>
        <source>&amp;End Recording</source>
        <translation>&amp;Beeindig Opname</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2192"/>
        <source>&amp;Sync and Save</source>
        <translation>&amp;Sinkroniseer en Stoor</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2211"/>
        <source>&amp;Save and Finish</source>
        <translation>&amp;Stoor en Maak Klaar</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2230"/>
        <source>&amp;Start</source>
        <translation>&amp;Begin</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="188"/>
        <source>Scanning for compatible oximeters</source>
        <translation>Soek vir versoenbare oximeters</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="220"/>
        <source>Could not detect any connected oximeter devices.</source>
        <translation>Kon nie enige gekoppelde oximeter toestelle opspoor nie.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="228"/>
        <source>Connecting to %1 Oximeter</source>
        <translation>Koppel na %1 Oximeter</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="256"/>
        <source>Renaming this oximeter from &apos;%1&apos; to &apos;%2&apos;</source>
        <translation>Herbenoem hiereie oximeter van &apos;%1&apos; na &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="259"/>
        <source>Oximeter name is different.. If you only have one and are sharing it between profiles, set the name to the same on both profiles.</source>
        <translation>Oximeetr naam verskil... As u net een het en dit deel tussen profiele, stel die naam dieselfde op beide profiele.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="299"/>
        <source>&quot;%1&quot;, session %2</source>
        <translation>&quot;%1&quot;, sessie %2</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="321"/>
        <source>Nothing to import</source>
        <translation>Niks om in te voer nie</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="323"/>
        <source>Your oximeter did not have any valid sessions.</source>
        <translation>U oximeter het geen geldige sessies gehad nie.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="324"/>
        <source>Close</source>
        <translation>Maak toe</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="327"/>
        <source>Waiting for %1 to start</source>
        <translation>Wag vir %1 om te begin</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="328"/>
        <source>Waiting for the device to start the upload process...</source>
        <translation>Wag vir die toestel om die invoer proses te begin...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="330"/>
        <source>Select upload option on %1</source>
        <translation>Kies die oplaai opsie op %1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="331"/>
        <source>You need to tell your oximeter to begin sending data to the computer.</source>
        <translation>U moet die oximeter vertel om data te begin stuur na die rekenaar toe.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="333"/>
        <source>Please connect your oximeter, enter it&apos;s menu and select upload to commence data transfer...</source>
        <translation>Koppel asb u oximeter, gaan in sy kieskaart in en kies om op te laai om data oordrag te begin...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="364"/>
        <source>%1 device is uploading data...</source>
        <translation>%1 toestel isbesig om dat op te laai...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="365"/>
        <source>Please wait until oximeter upload process completes. Do not unplug your oximeter.</source>
        <translation>Wag asseblief totdat dei oximeter oplaai proses voltooi is. Moenie die oximeter ontkoppel nie.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="384"/>
        <source>Oximeter import completed..</source>
        <translation>Oximeter invoer voltooi..</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="408"/>
        <source>Select a valid oximetry data file</source>
        <translation>Kies &apos;n geldige oximeter dat lêer</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="408"/>
        <source>Oximetry Files (*.spo *.spor *.spo2 *.SpO2 *.dat)</source>
        <translation>Oximetrie Lêers (*.spo *.spor *.spo2 *.SpO2 *.dat)</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="430"/>
        <source>No Oximetry module could parse the given file:</source>
        <translation>Geen Oksimetrie module kon die gegewe lêer ontleed nie:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="475"/>
        <source>Live Oximetry Mode</source>
        <translation>Lewendige Oksimetrie Mode</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="527"/>
        <source>Live Oximetry Stopped</source>
        <translation>Lewendige Oksimetrie Gestop</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="528"/>
        <source>Live Oximetry import has been stopped</source>
        <translation>Lewendige Oksimetrie invoer is gestop</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1072"/>
        <source>Oximeter Session %1</source>
        <translation>Oksimeter Sessie %1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1117"/>
        <source>OSCAR gives you the ability to track Oximetry data alongside CPAP session data, which can give valuable insight into the effectiveness of CPAP treatment. It will also work standalone with your Pulse Oximeter, allowing you to store, track and review your recorded data.</source>
        <translation>OSCAR gee u een van die vermoë om oximetrie data teenoor CPAP sessie data te volg, wat waardevolle insig kan gee in die effektiwiteit van CPAP behandeling. Dit sal ook allenstaande werk met u pols oximeter, wat u sooende toelaat om u data wat opgeneem is te stoor, na te gaan en te ondersoek.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1119"/>
        <source>OSCAR is currently compatible with Contec CMS50D+, CMS50E, CMS50F and CMS50I serial oximeters.&lt;br/&gt;(Note: Direct importing from bluetooth models is &lt;span style=&quot; font-weight:600;&quot;&gt;probably not&lt;/span&gt; possible yet)</source>
        <translation>OSCAR is tans versoenbaar met Contec CMS50D+, CMS50E, CMS50F en CMS50I seriale oximeters.&lt;br/&gt;(Nota: Direkte invoering van bluetooth modelle is &lt;span style=&quot; font-weight:600;&quot;&gt;waarskynlik&lt;/span&gt; nog nie moontlik nie)</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1128"/>
        <source>If you are trying to sync oximetry and CPAP data, please make sure you imported your CPAP sessions first before proceeding!</source>
        <translation>Indien u probeer om oksimetrie en CPAP data te sinkroniseer, maak asb seker dat u die CPAP sessies reeds ingevoer het voordat u voortgaan!</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1131"/>
        <source>For OSCAR to be able to locate and read directly from your Oximeter device, you need to ensure the correct device drivers (eg. USB to Serial UART) have been installed on your computer. For more information about this, %1click here%2.</source>
        <translation>U moet seker maak dat die korrekte device drivers gelaai is vir OSCAR om u oximeter op te spoor en data daarvan af te kan lees (bv UART of USB). Vir meer informasie hieroor, %1sien%2.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="457"/>
        <source>Oximeter not detected</source>
        <translation>Oximeter nie bespeur nie</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="464"/>
        <source>Couldn&apos;t access oximeter</source>
        <translation>Kon nie toegang tot oximeter kry nie</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="478"/>
        <source>Starting up...</source>
        <translation>Begin nou...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="479"/>
        <source>If you can still read this after a few seconds, cancel and try again</source>
        <translation>Indien u steeds hierdie kan lees na &apos;n paas sekondes, kanselleer en probeer weer</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="526"/>
        <source>Live Import Stopped</source>
        <translation>Lewendige Invoer Gestop</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="578"/>
        <source>%1 session(s) on %2, starting at %3</source>
        <translation>%1 sessie(s) op %2, beginnende by %3</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="582"/>
        <source>No CPAP data available on %1</source>
        <translation>Geen CPAP data beskikbaar op %1 nie</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="588"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="710"/>
        <source>Recording...</source>
        <translation>Neem op...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="717"/>
        <source>Finger not detected</source>
        <translation>Vinger nie bespeur nie</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="817"/>
        <source>I want to use the time my computer recorded for this live oximetry session.</source>
        <translation>Ek wil die tyd wat my rekenaar opgeneem het gebruik vir hierdie reële-tyd oximeter sessie.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="820"/>
        <source>I need to set the time manually, because my oximeter doesn&apos;t have an internal clock.</source>
        <translation>Ek het nodig om die tyd handmatig te stel, omdat my oximeter nie beskik oor &apos;n interne klok nie.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="832"/>
        <source>Something went wrong getting session data</source>
        <translation>Iets het fout gegaan tydens sessie data invordering</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1113"/>
        <source>Welcome to the Oximeter Import Wizard</source>
        <translation>Elkom by die Oximeter Invoer Helper</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1115"/>
        <source>Pulse Oximeters are medical devices used to measure blood oxygen saturation. During extended Apnea events and abnormal breathing patterns, blood oxygen saturation levels can drop significantly, and can indicate issues that need medical attention.</source>
        <translation>Pols Oximeters is mediese toestelle wat gebruik word om bloed suurstofvlak versadiging te meet. Gedurende verlengde Apneë gebeurtenisse en abnormale asemhalingspatrone kan bloed suurstof versadigingsvlakke noemenswaardig afneem en kan dui op kwessies wat mediese aandag benodig.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1121"/>
        <source>You may wish to note, other companies, such as Pulox, simply rebadge Contec CMS50&apos;s under new names, such as the Pulox PO-200, PO-300, PO-400. These should also work.</source>
        <translation>U mag dalk wil weet dat ander maatskappye, soos Polux, eenvoudig die Contec CMS50 onder &apos;n nuwe naam verpak, soos die Polux PO-200, PO-300, PO-400. Hierdie behoort ook te werk.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1124"/>
        <source>It also can read from ChoiceMMed MD300W1 oximeter .dat files.</source>
        <translation>Dit kan ook lees vanaf ChoiceMMed MD300W1 oximeter .dat lêers.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1126"/>
        <source>Please remember:</source>
        <translation>Onthou asseblief:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1130"/>
        <source>Important Notes:</source>
        <translation>Belangrike Notas:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1133"/>
        <source>Contec CMS50D+ devices do not have an internal clock, and do not record a starting time. If you do not have a CPAP session to link a recording to, you will have to enter the start time manually after the import process is completed.</source>
        <translation>Contec CMS50D+ toestelle het nie &apos;n interne klok nie en noteer gevolglik nie &apos;n begintyd nie. Indien u nie &apos;n CPAP sessie het waaraan &apos;n opname gekoppel kan word nie, sal u &apos;n begintyd handmatig moet verskaf nadat die invoer proses voltooi is.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1135"/>
        <source>Even for devices with an internal clock, it is still recommended to get into the habit of starting oximeter records at the same time as CPAP sessions, because CPAP internal clocks tend to drift over time, and not all can be reset easily.</source>
        <translation>Selfs vir toestelle met &apos;n interne klok, word dit steeds aanbeveel om die gewoonte aan te kweek om oximeter opnames te begin op dieselfde tyd as CPAP sessies, omdat CPAP interne tydhouding oor tyd kan dryf en nie almal kan maklik herstel word nie.</translation>
    </message>
</context>
<context>
    <name>Oximetry</name>
    <message>
        <location filename="../oscar/oximetry.ui" line="14"/>
        <source>Form</source>
        <translation>Vorm</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="89"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="102"/>
        <source>d/MM/yy h:mm:ss AP</source>
        <translation>d/MM/yy h:mm:ss AP</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="131"/>
        <source>R&amp;eset</source>
        <translation>H&amp;erstel</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="160"/>
        <source>SpO2</source>
        <translation>SpO2</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="245"/>
        <source>Pulse</source>
        <translation>Pols</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="346"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="366"/>
        <source>&amp;Open .spo/R File</source>
        <translation>Maak .spo/R Lêer &amp;Oop</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="385"/>
        <source>Serial &amp;Import</source>
        <translation>Seriale &amp;Invoer</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="398"/>
        <source>&amp;Start Live</source>
        <translation>&amp;Begin Lewendig</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="421"/>
        <source>Serial Port</source>
        <translation>Seriale Poort</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="450"/>
        <source>&amp;Rescan Ports</source>
        <translation>Herlees Poo&amp;rte</translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="23"/>
        <source>Preferences</source>
        <translation>Voorkeure</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="64"/>
        <source>&amp;Import</source>
        <translation>&amp;Invoer</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="154"/>
        <source>Combine Close Sessions </source>
        <translation>Kombineer Naby Sessies </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="164"/>
        <location filename="../oscar/preferencesdialog.ui" line="249"/>
        <location filename="../oscar/preferencesdialog.ui" line="731"/>
        <source>Minutes</source>
        <translation>Minute</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="184"/>
        <source>Multiple sessions closer together than this value will be kept on the same day.
</source>
        <translation>Veelvuldige sessien nader aan mekaar as hierdie waarde sal op dieselfde dag gehou word.
</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="239"/>
        <source>Ignore Short Sessions</source>
        <translation>Ingoreer Kort Sessies</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="266"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Sessions shorter in duration than this will not be displayed&lt;span style=&quot; font-style:italic;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-style:italic;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Sessies korter as hierdie sal nie vertoon word nie&lt;span style=&quot; font-style:italic;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-style:italic;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="310"/>
        <source>Day Split Time</source>
        <translation>Dag Verdeling Tyd</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="320"/>
        <source>Sessions starting before this time will go to the previous calendar day.</source>
        <translation>Sessies wat voor hierdie tyd begin sal na die vorige kalenderdag toe gaan.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="393"/>
        <source>Session Storage Options</source>
        <translation>Sessie Stoor Opsies</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="439"/>
        <source>Compress SD Card Backups (slower first import, but makes backups smaller)</source>
        <translation>Pers SD kaart rugsteun saam (stadiger eerste invoer, maar maak rugsteun kleiner)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="417"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Changing SD Backup compression options doesn&apos;t automatically recompress backup data.  &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Verandering van SD Kaart rugsteun samepersing opsies pers nie outomaties die rugsteun data weer saam nie.  &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="637"/>
        <source>&amp;CPAP</source>
        <translation>&amp;CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1267"/>
        <source>Regard days with under this usage as &quot;incompliant&quot;. 4 hours is usually considered compliant.</source>
        <translation>Beskou dae met gebruik onder hierdie waarde as&quot;onvoldoening&quot;. 4 ure word gewoonlik as voldoening beskou.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1270"/>
        <source> hours</source>
        <translation> ure</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="945"/>
        <source>Enable/disable experimental event flagging enhancements. 
It allows detecting borderline events, and some the machine missed.
This option must be enabled before import, otherwise a purge is required.</source>
        <translation>Aktiveer / deaktiveer verbeterings vir eksperimentele gebeurtenisse.
Dit laat grensgebeurtenisse opspoor, en op sommige van die masjiene word dit gemis.
Hierdie opsie moet geaktiveer word voor data invoer, anders word &apos;n opruiming vereis.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1023"/>
        <source>Flow Restriction</source>
        <translation>Vloei Beperking</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1064"/>
        <source>Percentage of restriction in airflow from the median value. 
A value of 20% works well for detecting apneas. </source>
        <translation>Persentasie beperkings in lugvloei vanaf die mediaanwaarde.
&apos;n waarde van 20% werk goed vir die opsporing van apneë. </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="987"/>
        <location filename="../oscar/preferencesdialog.ui" line="1068"/>
        <location filename="../oscar/preferencesdialog.ui" line="1545"/>
        <location filename="../oscar/preferencesdialog.ui" line="1705"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1041"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:italic;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Custom flagging is an experimental method of detecting events missed by the machine. They are &lt;span style=&quot; text-decoration: underline;&quot;&gt;not&lt;/span&gt; included in AHI.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:italic;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Eie merking  is &apos;n eksperimentele metode om gebeurtenisse wat deur die masjien gemis word, op te spoor. Hulle is &lt;span style=&quot; text-decoration: underline;&quot;&gt;nie&lt;/span&gt; ingesluit in die AHI nie.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1085"/>
        <source>Duration of airflow restriction</source>
        <translation>Tydsduur van lugvloei beperking</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="977"/>
        <location filename="../oscar/preferencesdialog.ui" line="1088"/>
        <location filename="../oscar/preferencesdialog.ui" line="1562"/>
        <location filename="../oscar/preferencesdialog.ui" line="1650"/>
        <location filename="../oscar/preferencesdialog.ui" line="1679"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1124"/>
        <source>Event Duration</source>
        <translation>Geburtenis Tydsduur</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1111"/>
        <source>Allow duplicates near machine events.</source>
        <translation>Laat herhaling in die omgewing van masjien gebeurtenisse toe.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1189"/>
        <source>Adjusts the amount of data considered for each point in the AHI/Hour graph.
Defaults to 60 minutes.. Highly recommend it&apos;s left at this value.</source>
        <translation>Pas die hoeveelheid data aan wat oorweeg word vir elke punt in die AHI/Uur grafiek.
Verstekwaarde is 60 minute. Dit word sterk aanbeveel om dit op hierdie waarde te los.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1193"/>
        <source> minutes</source>
        <translation> minute</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1232"/>
        <source>Reset the counter to zero at beginning of each (time) window.</source>
        <translation>Stel die teller terug na nul aan die begin van elke (tyd) venster.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1235"/>
        <source>Zero Reset</source>
        <translation>Zero Herstel</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="684"/>
        <source>CPAP Clock Drift</source>
        <translation>CPAP Klok Dryf</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="500"/>
        <source>Do not import sessions older than:</source>
        <translation>Moenie sessies ouer as hierdie invoer nie:</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="507"/>
        <source>Sessions older than this date will not be imported</source>
        <translation>Sessies ouer as hierdie datum sal nie ingevoer word nie</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="533"/>
        <source>dd MMMM yyyy</source>
        <translation>dd MMMM yyyy</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1248"/>
        <source>User definable threshold considered large leak</source>
        <translation>Gebruikers gedefinieerde drempel wat as groot lek beskou word</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1251"/>
        <source> L/min</source>
        <translation> L/min</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1215"/>
        <source>Whether to show the leak redline in the leak graph</source>
        <translation>Welke die rooi lek lyn op die lek grafiek vertoon moet word</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1792"/>
        <location filename="../oscar/preferencesdialog.ui" line="1864"/>
        <source>Search</source>
        <translation>Soek</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1488"/>
        <source>&amp;Oximetry</source>
        <translation>&amp;Oximetrie</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1078"/>
        <source>Show in Event Breakdown Piechart</source>
        <translation>Vertoon in Gebeurtenis Afbraak Sirkelkaart</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1101"/>
        <source>#1</source>
        <translation>#1</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1007"/>
        <source>#2</source>
        <translation>#2</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1000"/>
        <source>Resync Machine Detected Events (Experimental)</source>
        <translation>Hersinkroniseer Masjien Bespeurde Gebeurtenisse (Eksperimenteel)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1640"/>
        <source>SPO2</source>
        <translation>SpO2</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1702"/>
        <source>Percentage drop in oxygen saturation</source>
        <translation>Persentasie afname in suurstofversadiging</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1695"/>
        <source>Pulse</source>
        <translation>Pols</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1660"/>
        <source>Sudden change in Pulse Rate of at least this amount</source>
        <translation>Skielike verandering in Polstempo van ten minste hierdie hoeveelheid</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1552"/>
        <location filename="../oscar/preferencesdialog.ui" line="1582"/>
        <location filename="../oscar/preferencesdialog.ui" line="1663"/>
        <source> bpm</source>
        <translation> bpm</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1647"/>
        <source>Minimum duration of drop in oxygen saturation</source>
        <translation>Minimum tydsduur van daling in suurstofversadiging</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1676"/>
        <source>Minimum duration of pulse change event.</source>
        <translation>Minimum duur van polsverandering gebeurtenis.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1559"/>
        <source>Small chunks of oximetry data under this amount will be discarded.</source>
        <translation>Klein stukkies oximetrie data onder hierdie hoeveelheid sal ignoreer word.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1920"/>
        <source>&amp;General</source>
        <translation>Al&amp;gemeen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1315"/>
        <source>Changes to the following settings needs a restart, but not a recalc.</source>
        <translation>Wysigings aan die volgende instellings het &apos;n herbegin nodig, maar nie &apos;n herberekening nie.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1318"/>
        <source>Preferred Calculation Methods</source>
        <translation>Voorkeur Berekening Metodes</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1347"/>
        <source>Middle Calculations</source>
        <translation>Middelberekenings</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1361"/>
        <source>Upper Percentile</source>
        <translation>Boonste Persentiel</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="91"/>
        <source>Session Splitting Settings</source>
        <translation>Sessie Verdeling Instellings</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="356"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;This setting should be used with caution...&lt;/span&gt; Switching it off comes with consequences involving accuracy of summary only days, as certain calculations only work properly provided summary only sessions that came from individual day records are kept together. &lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;ResMed users:&lt;/span&gt; Just because it seems natural to you and I that the 12 noon session restart should be in the previous day, does not mean ResMed&apos;s data agrees with us. The STF.edf summary index format has serious weaknesses that make doing this not a good idea.&lt;/p&gt;&lt;p&gt;This option exists to pacify those who don&apos;t care and want to see this &amp;quot;fixed&amp;quot; no matter the costs, but know it comes with a cost. If you keep your SD card in every night, and import at least once a week, you won&apos;t see problems with this very often.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Hierdie instelling moet met omsigtigheid gebruik word ...&lt;/span&gt; Om dit af te skakel kom met gevolge wat die akkuraatheid van die opsomming van enkele dae betref, aangesien sekere berekeninge net behoorlik werk indien opsomming sessies wat uit individuele dagrekords kom, bymekaar gehou word. &lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;ResMed gebruikers:&lt;/span&gt; Net omdat dit natuurlik vir u en my lyk dat die middel-van-die-dag sessie herbegin moet word in die vorige dag, beteken nie ResMed se data stem saam met ons nie. Die STF.edf opsomming indeks formaat het ernstige swakpunte wat maak dat dit nie &apos;n goeie idee is nie.&lt;/p&gt;&lt;p&gt;Hierdie opsie bestaan om diegene wat nie omgee nie, gerus te stel en toe te laat om dit &amp;quot;reg te stel&amp;quot; ongeag die gevolge, maat weet dat dit met &apos;n koste gepaard gaan. As u u SD-kaart elke aand inhou en minstens een keer per week invoer, sal u dit nie baie dikwels probleme sien nie.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="359"/>
        <source>Don&apos;t Split Summary Days (Warning: read the tooltip!)</source>
        <translation>Moenie Opsommingsdae Skei Nie (Waarskuwing: lees die tooltip!)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="567"/>
        <source>Memory and Startup Options</source>
        <translation>Geheue en Aanvangsopsies</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="609"/>
        <source>Pre-Load all summary data at startup</source>
        <translation>Laai alle opsommende data vooraf</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="596"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This setting keeps waveform and event data in memory after use to speed up revisiting days.&lt;/p&gt;&lt;p&gt;This is not really a necessary option, as your operating system caches previously used files too.&lt;/p&gt;&lt;p&gt;Recommendation is to leave it switched off, unless your computer has a ton of memory.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Hierdie instelling hou die gebruik van golfvorm- en gebeurtenisdata in gebruik om die herhalende dae te bespoedig.&lt;/p&gt;&lt;p&gt;Dit is nie regtig &apos;n noodsaaklike opsie nie, aangesien u bedryfstelsel ook voorheen-gebruikte lêers cache.&lt;/p&gt;&lt;p&gt;Die aanbeveling is om dit af te skakel, tensy u rekenaar &apos;n groot klomp geheue het.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="599"/>
        <source>Keep Waveform/Event data in memory</source>
        <translation>Hou Golfvorm/Gebeurtenis data in geheue</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="623"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Cuts down on any unimportant confirmation dialogs during import.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Beperk enige onbelangrike bevestigingsdialoë tydens invoer.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="626"/>
        <source>Import without asking for confirmation</source>
        <translation>Voer in sonder om te vra vir bevestiging</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1153"/>
        <source>General CPAP and Related Settings</source>
        <translation>Algemene CPAP en Verwante Stellings</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1162"/>
        <source>Enable Unknown Events Channels</source>
        <translation>Aktiveer Onbekende Gebeurtenis Kanale</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1293"/>
        <source>AHI</source>
        <extracomment>Apnea Hypopnea Index</extracomment>
        <translation>AHI</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1298"/>
        <source>RDI</source>
        <extracomment>Respiratory Disturbance Index</extracomment>
        <translation>RDI</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1169"/>
        <source>AHI/Hour Graph Time Window</source>
        <translation>AHI/Uur Grafiek Tyd Venster</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1225"/>
        <source>Preferred major event index</source>
        <translation>Gewenste groot gebeurtenis indeks</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1176"/>
        <source>Compliance defined as</source>
        <translation>Voldoening gedefinieer as</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1218"/>
        <source>Flag leaks over threshold</source>
        <translation>Merk lekke oor drempel</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="765"/>
        <source>Seconds</source>
        <translation>Sekondes</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="711"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Note: This is not intended for timezone corrections! Make sure your operating system clock and timezone is set correctly.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Nota: Dit is nie bedoel vir tydsone regstellings nie! Maak seker dat u bedryfstelsel klok en tydsone korrek ingestel is.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="758"/>
        <source>Hours</source>
        <translation>Ure</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1324"/>
        <source>For consistancy, ResMed users should use 95% here,
as this is the only value available on summary-only days.</source>
        <translation>Vir konsekwentheid moet ResMed-gebruikers 95% hier gebruik,
aangesien dit die enigste waarde is wat op slegs opsommende dae beskikbaar is.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1375"/>
        <source>Median is recommended for ResMed users.</source>
        <translation>Mediaan word aanbeveel vir ResMed gebruikers.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1379"/>
        <location filename="../oscar/preferencesdialog.ui" line="1442"/>
        <source>Median</source>
        <translation>Mediaan</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1384"/>
        <source>Weighted Average</source>
        <translation>Geweegde Gemiddeld</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1389"/>
        <source>Normal Average</source>
        <translation>Normale Gemiddeld</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1413"/>
        <source>True Maximum</source>
        <translation>Ware Maksimum</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1418"/>
        <source>99% Percentile</source>
        <translation>99% Persentiel</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1354"/>
        <source>Maximum Calcs</source>
        <translation>Maksimum Berekeninge</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1949"/>
        <source>General Settings</source>
        <translation>Algemene Instellings</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2690"/>
        <source>Daily view navigation buttons will skip over days without data records</source>
        <translation>Daaglikse vertoon navigasieknoppies sal oor dae gaan wat nie data rekords het nie</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2693"/>
        <source>Skip over Empty Days</source>
        <translation>Gaan oor Dae Sonder Rekords</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1970"/>
        <source>Allow use of multiple CPU cores where available to improve performance. 
Mainly affects the importer.</source>
        <translation>Laat gebruik van verskeie CPU kerne toe waar beskikbaar om prestasie te verbeter.
Hoofsaaklik van toepassing op die invoerder.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1974"/>
        <source>Enable Multithreading</source>
        <translation>Aktiveer Multi-Verwerking</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="573"/>
        <source>Bypass the login screen and load the most recent User Profile</source>
        <translation>Gaan verby die aanmeldskerm en laai die mees onlangse gebruikersprofiel</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="482"/>
        <source>Create SD Card Backups during Import (Turn this off at your own peril!)</source>
        <translation>Skep SD Kaart Rugsteun tydens Invoer (Skakel hierdie af op u eie risiko!)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1409"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;True maximum is the maximum of the data set.&lt;/p&gt;&lt;p&gt;99th percentile filters out the rarest outliers.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ware maksimum is die maksimum van die datastel.&lt;/p&gt;&lt;p&gt;99ste persentiel filter die seldsame uitskieters uit.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1427"/>
        <source>Combined Count divided by Total Hours</source>
        <translation>Gekombineerde Telling gedeel deur Totale Ure</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1432"/>
        <source>Time Weighted average of Indice</source>
        <translation>Tyd-geweegde gemiddeld van Indekse</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1437"/>
        <source>Standard average of indice</source>
        <translation>Standaard gemiddeld van indekse</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1368"/>
        <source>Culminative Indices</source>
        <translatorcomment>*?????</translatorcomment>
        <translation>Kumulatiewe Indekse</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="950"/>
        <source>Custom CPAP User Event Flagging</source>
        <translation>Aangepaste CPAP Gebruikers Gebeurtenis Merking</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1769"/>
        <source>Events</source>
        <translation>Gebeurtenisse</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1815"/>
        <location filename="../oscar/preferencesdialog.ui" line="1894"/>
        <source>Reset &amp;Defaults</source>
        <translation>Herstel &amp;Verstekwaardes</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1828"/>
        <location filename="../oscar/preferencesdialog.ui" line="1907"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warning: &lt;/span&gt;Just because you can, does not mean it&apos;s good practice.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Waarskuwing: &lt;/span&gt;Net omdat u kan, beteken nie noodwendig dis &apos;n goeie idee nie.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1841"/>
        <source>Waveforms</source>
        <translation>Golfvorms</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1628"/>
        <source>Flag rapid changes in oximetry stats</source>
        <translation>Merk vinnige veranderinge in oximetrie waardes</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1539"/>
        <source>Other oximetry options</source>
        <translation>Ander oximetrie opsies</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1589"/>
        <source>Flag SPO2 Desaturations Below</source>
        <translation>Merk SPO2 Verarming Onder</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1572"/>
        <source>Discard segments under</source>
        <translation>Ignoreer segmente onder</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1609"/>
        <source>Flag Pulse Rate Above</source>
        <translation>Merk Polstempo Bo</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1599"/>
        <source>Flag Pulse Rate Below</source>
        <translation>Merk Polstempo Onder</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="431"/>
        <source>Compress ResMed (EDF) backups to save disk space.
Backed up EDF files are stored in the .gz format, 
which is common on Mac &amp; Linux platforms.. 

OSCAR can import from this compressed backup directory natively.. 
To use it with ResScan will require the .gz files to be uncompressed first..</source>
        <translation>Pers ResMed (EDF) rugsteun saam om skyfspasie te stoor.
Gerugsteunde EDF-lêers word gestoor in die .gz-formaat,
wat algemeen op Mac en Linux platforms voorkom.

OSCAR kan direk invoer vanaf hierdie saamgeperste rugsteun.
Om dit met ResScan te gebruik, sal die .gz-lêers eers ontplooi moet word.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="451"/>
        <source>The following options affect the amount of disk space OSCAR uses, and have an effect on how long import takes.</source>
        <translation>Die volgende opsies beïnvloed die hoeveelheid skyfspasie wat OSCAR gebruik, en het &apos;n effek op hoe lank invoer duur.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="461"/>
        <source>This makes OSCAR&apos;s data take around half as much space.
But it makes import and day changing take longer.. 
If you&apos;ve got a new computer with a small solid state disk, this is a good option.</source>
        <translation>Dit maak dat OSCAR se data ongeveer die helfte soveel ruimte in beslag neem.
Maar dit maak invoer en verandering van dae in OSCAR langer.
As jy &apos;n nuwe rekenaar het met &apos;n vinnige stoorspasie, is dit &apos;n goeie opsie.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="466"/>
        <source>Compress Session Data (makes OSCAR data smaller, but day changing slower.)</source>
        <translation>Pers Sessie Data Saam (maak OSCAR data kleiner, maar dagverandering stadiger)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="473"/>
        <source>This maintains a backup of SD-card data for ResMed machines, 

ResMed S9 series machines delete high resolution data older than 7 days, 
and graph data older than 30 days..

OSCAR can keep a copy of this data if you ever need to reinstall. 
(Highly recomended, unless your short on disk space or don&apos;t care about the graph data)</source>
        <translation>Dit behou &apos;n rugsteun van SD-kaart data vir ResMed masjiene,

ResMed S9 reeks masjiene verwyder hoë resolusie data ouer as 7 dae,
en grafiekdata ouer as 30 dae.

OSCAR kan &apos;n afskrif van hierdie data behou as u ooit weer moet installeer.
(Hoogs aanbeveel, tensy u kort is op skyfspasie of nie omgee vir die grafiekdata nie)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="606"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Makes starting OSCAR a bit slower, by pre-loading all the summary data in advance, which speeds up overview browsing and a few other calculations later on. &lt;/p&gt;&lt;p&gt;If you have a large amount of data, it might be worth keeping this switched off, but if you typically like to view &lt;span style=&quot; font-style:italic;&quot;&gt;everything&lt;/span&gt; in overview, all the summary data still has to be loaded anyway. &lt;/p&gt;&lt;p&gt;Note this setting doesn&apos;t affect waveform and event data, which is always demand loaded as needed.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Maak aanvang van OSCAR &apos;n bietjie stadiger, deur al die opsomming data vooraf te laai, wat vinniger oorsig verskaf sowel as &apos;n paar ander berekeninge later aan. &lt;/p&gt;&lt;p&gt;As u &apos;n groot hoeveelheid data het, kan dit die moeite werd wees om dit af te skakel, maar as u gewoonlik &lt;span style=&quot; font-style:italic;&quot;&gt;alles&lt;/span&gt; wil sien in die oorsig, sal al die data in elk geval gelaai moet word. &lt;/p&gt;&lt;p&gt;Neem kennis dat hierdie stelling nie golfvorm en gebeurtenis data affekteer nie, wat altyd gelaai word soos benodig.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="997"/>
        <source>This experimental option attempts to use OSCAR&apos;s event flagging system to improve machine detected event positioning.</source>
        <translation>Hierdie eksperimentele opsie poog om OSCAR se gebeurtenismerkerstelsel te gebruik om masjien opgespoor gebeurtenis posisionering te verbeter.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1159"/>
        <source>Show flags for machine detected events that haven&apos;t been identified yet.</source>
        <translation>Wys merkers vir masjien opgespoor gebeurtenisse wat nog nie geïdentifiseer is nie.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1749"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Syncing Oximetry and CPAP Data&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;CMS50 data imported from SpO2Review (from .spoR files) or the serial import method does &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600; text-decoration: underline;&quot;&gt;not&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt; have the correct timestamp needed to sync.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Live view mode (using a serial cable) is one way to acheive an accurate sync on CMS50 oximeters, but does not counter for CPAP clock drift.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;If you start your Oximeters recording mode at &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-style:italic;&quot;&gt;exactly &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;the same time you start your CPAP machine, you can now also achieve sync. &lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;The serial import process takes the starting time from last nights first CPAP session. (Remember to import your CPAP data first!)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Sinkroniseer Oximetrie en CPAP Data&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;CMS50 data ingevoer van SpO2Review (van .spoR lêers) of die seriale invoer metode het &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600; text-decoration: underline;&quot;&gt;nie&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt; die regte tydkode om te sinkroniseer nie.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Live View-mode (met behulp van &apos;n seriële kabel) is een manier om &apos;n akkurate sinkronisasie op CMS50-oximeters te implementeer, maar werk nie CPAP tyd wisseling teë nie.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Indien u u Oximeter se opname &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-style:italic;&quot;&gt;presies &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;op dieselfde tyd begin as wat u u CPAP masjien begin, kan u nou ook sinkronisasie bewerk. &lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Die seriale invoerproses neem die aanvangstyd van die eerste CPAP sessie van die vorige nag. (Onthou om eers u CPAP data in te voer!)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1981"/>
        <source>Show Remove Card reminder notification on OSCAR shutdown</source>
        <translation>Vertoon Verwyder Kaart kennisgewing tydens OSCAR uitgaan</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2013"/>
        <source>Automatically Check For Updates</source>
        <translation>Kyk Outomaties vir Opdaterings</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2030"/>
        <source>Check for new version every</source>
        <translation>Kyk vir nuwe weergawe elke</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2037"/>
        <source>Sourceforge hosts this project for free.. Please be considerate of their resources..</source>
        <translation>Sourceforge bied hierdie projek gratis aan .. Wees asseblief bedagsaam oor hul hulpbronne.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2053"/>
        <source>days.</source>
        <translation>dae.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2118"/>
        <source>&amp;Check for Updates now</source>
        <translation>&amp;Kyk nou vir opdaterings</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2141"/>
        <source>Last Checked For Updates: </source>
        <translation>Laaste Gekyk Vir Opdaterings: </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2154"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2176"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If your interested in helping test new features and bugfixes early, click here.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;But please be warned this will sometimes mean breaky code..&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Indien u belangstel om te help om nuwe funksies en foute vroegtydig te toets, kliek hier.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Maar wees asseblief gewaarsku, dit sal soms ongetoetste kode beteken...&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2185"/>
        <source>I want to try experimental and test builds (Advanced users only please.)</source>
        <translation>Ek wil eksperimentele en toetsweergawes uitprobeer (slegs Gevorderde gebruikers asseblief)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2209"/>
        <source>&amp;Appearance</source>
        <translation>&amp;Voorkoms</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2238"/>
        <source>Graph Settings</source>
        <translation>Grafiek Instellings</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2254"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Which tab to open on loading a profile. (Note: It will default to Profile if OSCAR is set to not open a profile on startup)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Watter tab om oop te maak wanner &apos;n profiel gelaai word. (Nota: Die verstekwaarde is Profiel as OSCAR gestel is om nie &apos;n profiel oop te maak as dit begin nie)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2512"/>
        <source>Bar Tops</source>
        <translatorcomment>????</translatorcomment>
        <translation>Bar Tops</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2517"/>
        <source>Line Chart</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2607"/>
        <source>Overview Linecharts</source>
        <translation>Oorsigtelike Lyngrafieke</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2552"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This makes scrolling when zoomed in easier on sensitive bidirectional TouchPads&lt;/p&gt;&lt;p&gt;50ms is recommended value.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Dit maak scrolling makliker wanneer ingezoom is op sensitiewe tweerigting-TouchPads&lt;/p&gt;&lt;p&gt;50ms is die aanbeveelde waarde.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2446"/>
        <source>How long you want the tooltips to stay visible.</source>
        <translation>Hoe lank moet die tooltips sigbaar bly.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2434"/>
        <source>Scroll Dampening</source>
        <translation>Scroll Demping</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2424"/>
        <source>Tooltip Timeout</source>
        <translation>Tooltip Timeout</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2531"/>
        <source>Default display height of graphs in pixels</source>
        <translation>Verstek vertoon hoogte van grafieke in pixels</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2504"/>
        <source>Graph Tooltips</source>
        <translation>Grafiek Tooltips</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2380"/>
        <source>The visual method of displaying waveform overlay flags.
</source>
        <translation>Die viduele metode om golfvorm oorleg merkers te vertoon.
</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2385"/>
        <source>Standard Bars</source>
        <translation>Standaard Bars</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2390"/>
        <source>Top Markers</source>
        <translation>Boonste Merkers</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2338"/>
        <source>Graph Height</source>
        <translation>Grafiek Hoogte</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="576"/>
        <source>Auto-Launch CPAP Importer after opening profile</source>
        <translation>Outo-Laai CPAP Invoerder nadat profiel oopgemaak is</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="616"/>
        <source>Automatically load last used profile on start-up</source>
        <translation>Laai outomaties laaste profiel</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="781"/>
        <source>This calculation requires Total Leaks data to be provided by the CPAP machine. (Eg, PRS1, but not ResMed, which has these already)

The Unintentional Leak calculations used here are linear, they don&apos;t model the mask vent curve.

If you use a few different masks, pick average values instead. It should still be close enough.</source>
        <translation>Hierdie berekening vereis dat Total Lek data verskaf word deur die CPAP masjien. (Bv. PRS1, maar nie ResMed nie, wat reeds hierdie het)

Die onbedoelde lekberekeninge wat hier gebruik word, is lineêr, hulle modelleer nie die maskerventilasiekurwe nie.

As u &apos;n paar verskillende maskers gebruik, kies eerder gemiddelde waardes. Dit behoort steeds naby genoeg te wees.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="788"/>
        <source>Calculate Unintentional Leaks When Not Present</source>
        <translation>Bereken Onbedoekde Lekke Indien Nie Teenwoordig Nie</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="797"/>
        <source>Your masks vent rate at 20cmH2O pressure</source>
        <translation>U masker ventilasie tempo teen 20cmH2O druk</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="853"/>
        <source>Your masks vent rate at 4cmH2O pressure</source>
        <translation>U masker ventilasie tempo teen 4cmH2O druk</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="884"/>
        <source>4 cmH2O</source>
        <translation>4 cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="894"/>
        <source>20 cmH2O</source>
        <translation>20 cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="926"/>
        <source>Note: A linear calculation method is used. Changing these values requires a recalculation.</source>
        <translation>Nota: &apos;n Lineêre berekeningsmetode word gebruik. Verandering van die waardes vereis &apos;n herberekening.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1456"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Note: &lt;/span&gt;Due to summary design limitations, ResMed machines do not support changing these settings.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Nota: &lt;/span&gt;As gevolg van opsommende ontwerpbeperkings, ondersteun ResMed-masjiene nie die verandering van hierdie instellings nie.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1509"/>
        <source>Oximetry Settings</source>
        <translation>Oximetrie Instellings</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2244"/>
        <source>On Opening</source>
        <translation>Tydens Oopmaak</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2257"/>
        <location filename="../oscar/preferencesdialog.ui" line="2261"/>
        <source>Profile</source>
        <translation>Profiel</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2266"/>
        <location filename="../oscar/preferencesdialog.ui" line="2305"/>
        <source>Welcome</source>
        <translation>Welkom</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2271"/>
        <location filename="../oscar/preferencesdialog.ui" line="2310"/>
        <source>Daily</source>
        <translation>Daaglikse</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2281"/>
        <location filename="../oscar/preferencesdialog.ui" line="2320"/>
        <source>Statistics</source>
        <translation>Statistieke</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2289"/>
        <source>Switch Tabs</source>
        <translation>Verander Tabs</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2300"/>
        <source>No change</source>
        <translation>Geen verandering nie</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2328"/>
        <source>After Import</source>
        <translation>Na Invoering</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2354"/>
        <source>Overlay Flags</source>
        <translation>Oorleg Merkers</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2364"/>
        <source>Line Thickness</source>
        <translation>Lyndikte</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2400"/>
        <source>The pixel thickness of line plots</source>
        <translation>Die lyndikte van plots</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2626"/>
        <source>Other Visual Settings</source>
        <translation>Ander Visuele Stellings</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2632"/>
        <source>Anti-Aliasing applies smoothing to graph plots.. 
Certain plots look more attractive with this on. 
This also affects printed reports.

Try it and see if you like it.</source>
        <translation>Anti-Aliasering maak dat grafieke gladder vertoon.
Sekere grafieke lyk beter met hierdie opsie aangeskakel.
Hierdie affekteer ook gedrukte verslae.

Probeer dit en kyk of u daarvan hou.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2639"/>
        <source>Use Anti-Aliasing</source>
        <translation>Gebruik Anti-Aliasering</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2646"/>
        <source>Makes certain plots look more &quot;square waved&quot;.</source>
        <translation>Laat sekere plots meer reghoekig lyk.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2649"/>
        <source>Square Wave Plots</source>
        <translation>Reghoekige Plots</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2656"/>
        <source>Allows graphs to be &quot;screenshotted&quot; for display purposes.
The Event Breakdown PIE chart uses this method, as does
the printing code.
Unfortunately some older computers/versions of Qt can cause
this application to be unstable with this feature enabled.</source>
        <translation>Laat &quot;skermsskote&quot; van plots toe vir vertoon doelteindes.
Die Gebeurtenis Afbraak Sirkelkaart gebruik hierdie metode, asook
die drukker kode.
Ongelukkig kan sommige ouer rekenaars/weergawes van Qt veroorsaak
dat hierdie toepassing onstabiel raak met die funksie geaktiveer.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2663"/>
        <source>Show event breakdown pie chart</source>
        <translation>Vertoon gebeurtenis afbraak sirkelkaart</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2670"/>
        <source>Pixmap caching is an graphics acceleration technique. May cause problems with font drawing in graph display area on your platform.</source>
        <translation>Pixmap caching is &apos;n garfiese versnellingstegbiek. Dit mag probleme veroorsaak met lettertipe tekening in die grafiese vertoon area op u platvorm.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2673"/>
        <source>Use Pixmap Caching</source>
        <translation>Gebruik Pixmap Caching</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2680"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;These features have recently been pruned. They will come back later. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Hierdie kenmerke is onlangs gesnoei. Hulle sal later terugkom. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2683"/>
        <source>Animations &amp;&amp; Fancy Stuff</source>
        <translation>Animasies &amp;&amp; Fênsie Dinge</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2700"/>
        <source>Whether to allow changing yAxis scales by double clicking on yAxis labels</source>
        <translation>Welke verandering van Y-As skalering toegelaat word deur dubbel-kliek op Y-as waardes</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2703"/>
        <source>Allow YAxis Scaling</source>
        <translation>Laat Y-As Skalering Toe</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2726"/>
        <source>Graphics Engine (Requires Restart)</source>
        <translation>Grafiese Engine (Benodig Uitgaan)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2732"/>
        <source>Try changing this from the default setting (Desktop OpenGL) if you experience rendering problems with OSCAR&apos;s graphs.</source>
        <translation>Probeer om dit te verander van die verstekinstelling (Desktop OpenGL) as u probleme ondervind met OSCAR se grafieke.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2757"/>
        <source>Fonts (Application wide settings)</source>
        <translation>Lettertipes (Programwye instellings)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2792"/>
        <source>Font</source>
        <translation>Lettertipe</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2811"/>
        <source>Size</source>
        <translation>Grootte</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2830"/>
        <source>Bold  </source>
        <translation>Bold  </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2852"/>
        <source>Italic</source>
        <translation>Kursief</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2865"/>
        <source>Application</source>
        <translation>Program</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2929"/>
        <source>Graph Text</source>
        <translation>Grafiek Teks</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2990"/>
        <source>Graph Titles</source>
        <translation>Grafiek Titels</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3051"/>
        <source>Big  Text</source>
        <translation>Groot  Teks</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3118"/>
        <location filename="../oscar/preferencesdialog.cpp" line="428"/>
        <location filename="../oscar/preferencesdialog.cpp" line="560"/>
        <source>Details</source>
        <translation>Details</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3162"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Kanselleer</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3169"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="423"/>
        <location filename="../oscar/preferencesdialog.cpp" line="554"/>
        <source>Name</source>
        <translation>Naam</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="424"/>
        <location filename="../oscar/preferencesdialog.cpp" line="555"/>
        <source>Color</source>
        <translation>Kleur</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="426"/>
        <source>Flag Type</source>
        <translation>Merker Tipe</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="427"/>
        <location filename="../oscar/preferencesdialog.cpp" line="559"/>
        <source>Label</source>
        <translation>Label</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="444"/>
        <source>CPAP Events</source>
        <translation>CPAP Gebeurtenisse</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="445"/>
        <source>Oximeter Events</source>
        <translation>Oximeter Gebeurtenisse</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="446"/>
        <source>Positional Events</source>
        <translation>Posisionele Gebeurtenisse</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="447"/>
        <source>Sleep Stage Events</source>
        <translation>Slaap Fase Gebeurtenisse</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="448"/>
        <source>Unknown Events</source>
        <translation>Onbekende Gebeurtenisse</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="620"/>
        <source>Double click to change the descriptive name this channel.</source>
        <translation>Dubbel kliek om die beskrywende naam van hierdie kanaal te verander.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="498"/>
        <location filename="../oscar/preferencesdialog.cpp" line="627"/>
        <source>Double click to change the default color for this channel plot/flag/data.</source>
        <translation>Dubbelkliek om die verstek kleur vir hierdie kanaal se plot/merker/data te verander.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2276"/>
        <location filename="../oscar/preferencesdialog.ui" line="2315"/>
        <location filename="../oscar/preferencesdialog.cpp" line="425"/>
        <location filename="../oscar/preferencesdialog.cpp" line="556"/>
        <source>Overview</source>
        <translation>Oorsig</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="64"/>
        <source>&lt;p&gt;&lt;b&gt;Please Note:&lt;/b&gt; OSCAR&apos;s advanced session splitting capabilities are not possible with &lt;b&gt;ResMed&lt;/b&gt; machines due to a limitation in the way their settings and summary data is stored, and therefore they have been disabled for this profile.&lt;/p&gt;&lt;p&gt;On ResMed machines, days will &lt;b&gt;split at noon&lt;/b&gt; like in ResMed&apos;s commercial software.&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;Neem Asseblief Kennis:&lt;/b&gt; OSCAR se se gevorderde sessie skeiding vermoëns is nie moontlik met &lt;b&gt;ResMed&lt;/b&gt; masjiene nie weens &apos;n beperking in die manier waarop hul instellings en opsommingsdata gestoor word, en daarom is hulle vir hierdie profiel gedeaktiveer.&lt;/p&gt;&lt;p&gt;Op ResMed masjiene, sal dae &lt;b&gt;skei op middagete&lt;/b&gt; soos in ResMed se kommersiële sagteware.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="490"/>
        <source>Double click to change the descriptive name the &apos;%1&apos; channel.</source>
        <translation>Dubbelkliek om die beskrywende naam van die &apos;%1&apos;-kanaal te verander.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="503"/>
        <source>Whether this flag has a dedicated overview chart.</source>
        <translation>Of hierdie vlag &apos;n toegewyde oorsigkaart het.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="513"/>
        <source>Here you can change the type of flag shown for this event</source>
        <translation>Hier kan u die tipe merker wat vir hierdie gebeurtenis gewys word, verander</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="518"/>
        <location filename="../oscar/preferencesdialog.cpp" line="651"/>
        <source>This is the short-form label to indicate this channel on screen.</source>
        <translation>Dit is die kortvormetiket om hierdie kanaal op die skerm aan te dui.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="524"/>
        <location filename="../oscar/preferencesdialog.cpp" line="657"/>
        <source>This is a description of what this channel does.</source>
        <translation>Dit is &apos;n beskrywing van wat hierdie kanaal doen.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="557"/>
        <source>Lower</source>
        <translation>Onderste</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="558"/>
        <source>Upper</source>
        <translation>Boonste</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="577"/>
        <source>CPAP Waveforms</source>
        <translation>CPAP Golfvorms</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="578"/>
        <source>Oximeter Waveforms</source>
        <translation>Oximeter Golfvorms</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="579"/>
        <source>Positional Waveforms</source>
        <translation>Posisionele Golfvorms</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="580"/>
        <source>Sleep Stage Waveforms</source>
        <translation>Slaap Fase Golfvorms</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="636"/>
        <source>Whether a breakdown of this waveform displays in overview.</source>
        <translation>Of &apos;n afbraak van hierdie golfvorm in die oorsig vertoon.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="641"/>
        <source>Here you can set the &lt;b&gt;lower&lt;/b&gt; threshold used for certain calculations on the %1 waveform</source>
        <translation>Hier kan u die &lt;b&gt;onderste&lt;/b&gt; drempel stel wat gebruik word vir sekere berekenings op die %1 golfvorm</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="646"/>
        <source>Here you can set the &lt;b&gt;upper&lt;/b&gt; threshold used for certain calculations on the %1 waveform</source>
        <translation>Hier kan u die &lt;b&gt;boonste&lt;/b&gt; drempel stel wat gebruik word vir sekere berekenings op die %1 golfvorm</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="756"/>
        <source>Data Processing Required</source>
        <translation>Data Verwerking Benodig</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="757"/>
        <source>A data re/decompression proceedure is required to apply these changes. This operation may take a couple of minutes to complete.

Are you sure you want to make these changes?</source>
        <translation>&apos;n Data her- / dekompressie-proses is nodig om hierdie veranderinge toe te pas. Hierdie operasie kan &apos;n paar minute neem om te voltooi.

Is u seker u wil hierdie veranderinge wil maak?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="765"/>
        <source>Data Reindex Required</source>
        <translation>Data Her-indeks Benodig</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="766"/>
        <source>A data reindexing proceedure is required to apply these changes. This operation may take a couple of minutes to complete.

Are you sure you want to make these changes?</source>
        <translation>&apos;n Data her-indeksproses is nodig om hierdie veranderinge toe te pas. Hierdie operasie kan &apos;n paar minute neem om te voltooi.

Is u seker u wil hierdie veranderinge wil maak?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1132"/>
        <source> If you ever need to reimport this data again (whether in OSCAR or ResScan) this data won&apos;t come back.</source>
        <translation> As u hierdie data ooit weer moet invoer (of dit nou in OSCAR of ResScan is), sal hierdie data nie terugkom nie.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1133"/>
        <source> If you need to conserve disk space, please remember to carry out manual backups.</source>
        <translation> As u skyfspasie moet behou, onthou asb om handmatige rugsteun uit te voer.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1134"/>
        <source> Are you sure you want to disable these backups?</source>
        <translation> Is u seker u wil hierdie backups afskakel?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1178"/>
        <source>Switching off backups is not a good idea, because OSCAR needs these to rebuild the database if errors are found.

</source>
        <translation>Om rugsteun af te skakel, is nie &apos;n goeie idee nie, want OSCAR benodig dit om die databasis te herbou indien foute gevind word.

</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1179"/>
        <source>Are you really sure you want to do this?</source>
        <translation>Is u regtig seker dat u dit wil doen?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="48"/>
        <source>Flag</source>
        <translation>Merker</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="49"/>
        <source>Minor Flag</source>
        <translation>Klein Merker</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="50"/>
        <source>Span</source>
        <translation>Mate</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="51"/>
        <source>Always Minor</source>
        <translation>Altyd Klein</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="256"/>
        <source>Never</source>
        <translation>Nooit</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="283"/>
        <location filename="../oscar/preferencesdialog.cpp" line="284"/>
        <location filename="../oscar/preferencesdialog.cpp" line="1250"/>
        <location filename="../oscar/preferencesdialog.cpp" line="1255"/>
        <source>%1 %2</source>
        <translation>%1 %2</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="772"/>
        <source>Restart Required</source>
        <translation>Herbegin Vereis</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="773"/>
        <source>One or more of the changes you have made will require this application to be restarted, in order for these changes to come into effect.

Would you like do this now?</source>
        <translation>Een of meer van die veranderinge wat u gemaak het, sal vereis dat hierdie program herbegin word, sodat hierdie veranderinge in werking tree.

Wil u dit nou doen?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1130"/>
        <source>This may not be a good idea</source>
        <translation>Dit mag dalk nie &apos;n goeie idee wees nie</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1131"/>
        <source>ResMed S9 machines routinely delete certain data from your SD card older than 7 and 30 days (depending on resolution).</source>
        <translation>ResMed S9-toestelle vee gereeld sekere data uit u SD-kaart ouer as 7 en 30 dae (afhangende van die resolusie).</translation>
    </message>
</context>
<context>
    <name>ProfileSelect</name>
    <message>
        <location filename="../oscar/profileselect.ui" line="14"/>
        <source>Select Profile</source>
        <translation>Kies Profiel</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="115"/>
        <source>Search:</source>
        <translation>Soek:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="191"/>
        <source>Start with the selected user profile.</source>
        <translation>Begin met die gekose gebruikersprofiel.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="194"/>
        <source>&amp;Select User</source>
        <translation>&amp;Kies Gebruiker</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="215"/>
        <source>Create a new user profile.</source>
        <translation>Skep &apos;n nuwe gebruikerprofiel.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="218"/>
        <source>New Profile</source>
        <translation>Nuwe Profiel</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="235"/>
        <source>Choose a different OSCAR data folder.</source>
        <translation>Kies &apos;n verskillende OSCAR data vouer.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="274"/>
        <source>OSCAR</source>
        <translation>OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="310"/>
        <source>Click here if you didn&apos;t want to start OSCAR.</source>
        <translation>Kliek hier indien u nie OSCAR wou begin nie.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="383"/>
        <source>The current location of OSCAR data store.</source>
        <translation>Die huidige ligging van die OSCAR data stoor.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="241"/>
        <source>&amp;Different Folder</source>
        <translation>&amp;Verskillende Vouer</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="287"/>
        <source>[version]</source>
        <translation>[weergawe]</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="313"/>
        <source>&amp;Quit</source>
        <translation>&amp;Maak klaar</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="370"/>
        <source>Folder:</source>
        <translation>Vouer:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="389"/>
        <source>[data directory]</source>
        <translatorcomment>*??????directory??????</translatorcomment>
        <translation>[data stoorplek]</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="92"/>
        <source>Open Profile</source>
        <translation>Open Profiel</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="93"/>
        <source>Edit Profile</source>
        <translation>Wysig Profiel</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="95"/>
        <source>Delete Profile</source>
        <translation>Verwyder Profiel</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="139"/>
        <location filename="../oscar/profileselect.cpp" line="220"/>
        <source>Enter Password for %1</source>
        <translation>Verskaf Wagwoord vir %1</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="158"/>
        <location filename="../oscar/profileselect.cpp" line="342"/>
        <source>Incorrect Password</source>
        <translation>Verkeerde Wagwoord</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="160"/>
        <source>You entered the password wrong too many times.</source>
        <translation>Y het die wagwoord te veel kere verkeerd verskaf.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="182"/>
        <source>Enter the word DELETE below to confirm.</source>
        <translation>Sleutel die woord DELETE hieronder om te bevestig.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="182"/>
        <source>You are about to destroy profile &apos;%1&apos;.</source>
        <translation>U is op die punt om profiel &apos;%1&apos; te vernietig.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="201"/>
        <source>Sorry</source>
        <translation>Jammer</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="201"/>
        <source>You need to enter DELETE in capital letters.</source>
        <translation>U het nodig om DELETE in hoofletters in te sleutel.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="239"/>
        <source>You entered an incorrect password</source>
        <translation>U het &apos;n verkeerde wagwoord verskaf</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="242"/>
        <source>If you&apos;re trying to delete because you forgot the password, you need to delete it manually.</source>
        <translatorcomment>***could be better, perhaps***</translatorcomment>
        <translation>As u probeer uitvee omdat u die wagwoord vergeet het, sal u dit handmatig moet doen.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="255"/>
        <source>There was an error deleting the profile directory, you need to manually remove it.</source>
        <translation>Daar was &apos;n probleem met die uitvee van die profiel stoorplek, u sal dit handmatig moet verwyder.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="259"/>
        <source>Profile &apos;%1&apos; was succesfully deleted</source>
        <translation>Profiel &apos;%1&apos; is suksesvol uitgevee</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="289"/>
        <source>Create new profile</source>
        <translation>Skep nuwe profiel</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="326"/>
        <source>Enter Password</source>
        <translation>Verskaf Wagwoord</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="345"/>
        <source>You entered an Incorrect Password too many times. Exiting!</source>
        <translation>U het te veel kere &apos;n verkeerde wagwoord verskaf. Verlaat!</translation>
    </message>
</context>
<context>
    <name>ProfileSelector</name>
    <message>
        <location filename="../oscar/profileselector.ui" line="14"/>
        <source>Form</source>
        <translation>Vorm</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="26"/>
        <source>Filter:</source>
        <translation>Filter:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="36"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="171"/>
        <source>OSCAR</source>
        <translation>OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="186"/>
        <source>Version</source>
        <translation>Weergawe</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="203"/>
        <source>&amp;Open Profile</source>
        <translation>&amp;Open Profiel</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="214"/>
        <source>&amp;Edit Profile</source>
        <translation>&amp;Wysig Profiel</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="228"/>
        <source>&amp;New Profile</source>
        <translation>&amp;Nuwe Profiel</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="247"/>
        <source>Profile: None</source>
        <translation>Profiel: Geen</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="268"/>
        <source>Please select or create a profile...</source>
        <translation>Kies of skep asseblief &apos;n profiel...</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="319"/>
        <source>Destroy Profile</source>
        <translation>Vernietig Profiel</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="88"/>
        <source>Profile</source>
        <translation>Profiel</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="89"/>
        <source>Ventilator Brand</source>
        <translation>Ventilator Maak</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="90"/>
        <source>Ventilator Model</source>
        <translation>Ventilator Model</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="91"/>
        <source>Other Data</source>
        <translation>Ander Data</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="92"/>
        <source>Last Imported</source>
        <translation>Laaste Ingevoer</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="93"/>
        <source>Name</source>
        <translation>Naam</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="133"/>
        <location filename="../oscar/profileselector.cpp" line="304"/>
        <source>%1, %2</source>
        <translation>%1, %2</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="168"/>
        <source>You must create a profile</source>
        <translation>U moet &apos;n profiel skep</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="231"/>
        <location filename="../oscar/profileselector.cpp" line="355"/>
        <source>Enter Password for %1</source>
        <translation>Verskaf Wagwoord vir %1</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="247"/>
        <location filename="../oscar/profileselector.cpp" line="374"/>
        <source>You entered an incorrect password</source>
        <translation>U het &apos;n verkeerde wagwoord verskaf</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="250"/>
        <source>Forgot your password?</source>
        <translation>Het u u wagwoord vergeet?</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="250"/>
        <source>Ask on the forums how to reset it, it&apos;s actually pretty easy.</source>
        <translation>Vra op die forums hoe om dit te herstel, dit is baie maklik.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="314"/>
        <source>Select a profile first</source>
        <translation>Kies eers &apos;n profiel</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="377"/>
        <source>If you&apos;re trying to delete because you forgot the password, you need to either reset it or delete the profile folder manually.</source>
        <translation>Indien u probeer uitvee omdat u die wagwoord vergeet het, moet u dit of herstel of die profiel vouer handrolies uitvee.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="387"/>
        <source>You are about to destroy profile &apos;&lt;b&gt;%1&lt;/b&gt;&apos;.</source>
        <translation>U is op die punt om die profiel te vernietig &apos;&lt;b&gt;%1&lt;/b&gt;&apos;.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="387"/>
        <source>Think carefully, as this will irretrievably delete the profile along with all &lt;b&gt;backup data&lt;/b&gt; stored under&lt;br/&gt;%2.</source>
        <translation>Dink noukeurig, aangesien dit die profiel met alle &lt;b&gt;rugsteun data&lt;/b&gt; gestoor onder&lt;br/&gt;%2. onherroeplik sal uitvee.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="387"/>
        <source>Enter the word &lt;b&gt;DELETE&lt;/b&gt; below (exactly as shown) to confirm.</source>
        <translation>Skryf die woord &lt;b&gt;DELETE&lt;/b&gt; hieronder (presies soos vertoon) om te bevestig.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="405"/>
        <source>DELETE</source>
        <translation>DELETE</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="406"/>
        <source>Sorry</source>
        <translation>Jammer</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="406"/>
        <source>You need to enter DELETE in capital letters.</source>
        <translation>U het nodig om DELETE in hoofletters in te sleutel.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="419"/>
        <source>There was an error deleting the profile directory, you need to manually remove it.</source>
        <translation>Daar was &apos;n probleem met die uitvee van die profiel stoorplek, u sal dit handmatig moet verwyder.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="423"/>
        <source>Profile &apos;%1&apos; was succesfully deleted</source>
        <translation>Profiel &apos;%1&apos; is suksesvol uitgevee</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="433"/>
        <source>Bytes</source>
        <translation>Grepe</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="433"/>
        <source>KB</source>
        <translation>KB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="433"/>
        <source>MB</source>
        <translation>MB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="433"/>
        <source>GB</source>
        <translation>GB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="433"/>
        <source>TB</source>
        <translation>TB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="433"/>
        <source>PB</source>
        <translation>PB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="453"/>
        <source>Summaries:</source>
        <translation>Opsommings:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="454"/>
        <source>Events:</source>
        <translation>Gebeurtenisse:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="455"/>
        <source>Backups:</source>
        <translation>Rugsteun:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="467"/>
        <location filename="../oscar/profileselector.cpp" line="507"/>
        <source>Hide disk usage information</source>
        <translation>Versteek skyfgebruiksinligting</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="470"/>
        <source>Show disk usage information</source>
        <translation>Wys skyfgebruiksinligting</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="488"/>
        <source>Name: %1, %2</source>
        <translation>Naam: %1, %2</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="491"/>
        <source>Phone: %1</source>
        <translation>Tel: %1</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="494"/>
        <source>Email: &lt;a href=&apos;mailto:%1&apos;&gt;%1&lt;/a&gt;</source>
        <translation>Epos: &lt;a href=&apos;mailto:%1&apos;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="497"/>
        <source>Address:</source>
        <translation>Adres:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="500"/>
        <source>No profile information given</source>
        <translation>Geen profiel informasie verskaf</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="503"/>
        <source>Profile: %1</source>
        <translation>Profiel: %1</translation>
    </message>
</context>
<context>
    <name>ProgressDialog</name>
    <message>
        <location filename="../oscar/SleepLib/progressdialog.cpp" line="56"/>
        <source>Abort</source>
        <translation>Gaan uit</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1246"/>
        <source>No Data</source>
        <translation>Geen Data</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="366"/>
        <source>Events</source>
        <translation>Gebeurtenisse</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="364"/>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="367"/>
        <source>Duration</source>
        <translation>Tydsduur</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="381"/>
        <source>(% %1 in events)</source>
        <translation>(% %1 in gebeurtenisse)</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="64"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="395"/>
        <source>Jan</source>
        <translation>Jan</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="64"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="395"/>
        <source>Feb</source>
        <translation>F11</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="64"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="395"/>
        <source>Mar</source>
        <translation>Maart</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="64"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="395"/>
        <source>Apr</source>
        <translation>Apr</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="64"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="395"/>
        <source>May</source>
        <translation>Mei</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="64"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="395"/>
        <source>Jun</source>
        <translation>Jun</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="396"/>
        <source>Jul</source>
        <translation>Jul</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="396"/>
        <source>Aug</source>
        <translation>Aug</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="396"/>
        <source>Sep</source>
        <translation>Sep</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="396"/>
        <source>Oct</source>
        <translation>Okt</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="396"/>
        <source>Nov</source>
        <translation>Nov</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="396"/>
        <source>Dec</source>
        <translation>Des</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="496"/>
        <source>&quot;</source>
        <translation>&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="497"/>
        <source>ft</source>
        <translation>vt</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="498"/>
        <source>lb</source>
        <translation>lb</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="499"/>
        <source>oz</source>
        <translation>oz</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="500"/>
        <source>Kg</source>
        <translation>kg</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="501"/>
        <source>cmH2O</source>
        <translation>cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="198"/>
        <source>Med.</source>
        <translation>Med.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="216"/>
        <source>Min: %1</source>
        <translation>Min: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="247"/>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="257"/>
        <source>Min: </source>
        <translation>Min: </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="252"/>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="262"/>
        <source>Max: </source>
        <translation>Maks: </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="266"/>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="270"/>
        <source>%1: </source>
        <translation>%1: </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="274"/>
        <source>???: </source>
        <translation>???: </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="281"/>
        <source>Max: %1</source>
        <translation>Maks: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="287"/>
        <source>%1 (%2 days): </source>
        <translation>%1 (%2 dae): </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="289"/>
        <source>%1 (%2 day): </source>
        <translation>%1 (%2 dag): </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="347"/>
        <source>% in %1</source>
        <translation>% in %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="353"/>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="664"/>
        <location filename="../oscar/SleepLib/common.cpp" line="502"/>
        <source>Hours</source>
        <translation>Ure</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="359"/>
        <source>Min %1</source>
        <translation>Min %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="649"/>
        <source>
Hours: %1</source>
        <translation>
Ure: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="715"/>
        <source>%1 low usage, %2 no usage, out of %3 days (%4% compliant.) Length: %5 / %6 / %7</source>
        <translation>%1 lae gebruik, %2 geen gebruik, uit %3 dae (%4% voldoen.) Lengte: %5 / %6 / %7</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="796"/>
        <source>Sessions: %1 / %2 / %3 Length: %4 / %5 / %6 Longest: %7 / %8 / %9</source>
        <translation>Sessies: %1 / %2 / %3 Lengte: %4 / %5 / %6 Langste: %7 / %8 / %9</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="900"/>
        <source>%1
Length: %3
Start: %2
</source>
        <translation>%1
Lengte: %3
Begin: %2
</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="902"/>
        <source>Mask On</source>
        <translation>Masker Op</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="902"/>
        <source>Mask Off</source>
        <translation>Masker Af</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="913"/>
        <source>%1
Length: %3
Start: %2</source>
        <translation>%1
Lengte: %3
Begin: %2</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="1083"/>
        <source>TTIA:</source>
        <translation>TTIA:</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="1096"/>
        <source>
TTIA: %1</source>
        <translation>
TTIA: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="1196"/>
        <source>%1 %2 / %3 / %4</source>
        <translation>%1 %2 / %3 / %4</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="503"/>
        <source>Minutes</source>
        <translation>Minute</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="504"/>
        <source>Seconds</source>
        <translation>Sekondes</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="505"/>
        <source>h</source>
        <translation>h</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="506"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="507"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="508"/>
        <source>ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="509"/>
        <source>Events/hr</source>
        <translation>Gebeurtenisse/uur</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="511"/>
        <source>Hz</source>
        <translation>Hz</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="512"/>
        <source>bpm</source>
        <translation>bpm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="514"/>
        <source>Litres</source>
        <translation>Liters</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="515"/>
        <source>ml</source>
        <translation>ml</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="516"/>
        <source>Breaths/min</source>
        <translation>Asemteue/min</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="517"/>
        <source>?</source>
        <translation>?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="519"/>
        <source>Severity (0-1)</source>
        <translation>Erns (0-1)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="520"/>
        <source>Degrees</source>
        <translation>Grade</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="523"/>
        <source>Error</source>
        <translation>Fout</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="524"/>
        <source>Warning</source>
        <translation>Waarskuwing</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="525"/>
        <source>Information</source>
        <translation>Informasie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="526"/>
        <source>Busy</source>
        <translation>Besig</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="527"/>
        <source>Please Note</source>
        <translation>Neem Asseblief Kennis</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="530"/>
        <source>Compliance Only :(</source>
        <translation>Slegs Voldoening :(</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="531"/>
        <source>Graphs Switched Off</source>
        <translation>Grafieke Afgeskakel</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="532"/>
        <source>Summary Only :(</source>
        <translation>Slegs Opsomming :(</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="533"/>
        <source>Sessions Switched Off</source>
        <translation>Sessies Afgeskakel</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="537"/>
        <source>&amp;Yes</source>
        <translation>&amp;Ja</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="538"/>
        <source>&amp;No</source>
        <translation>&amp;Nee</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="539"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Kanselleer</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="540"/>
        <source>&amp;Destroy</source>
        <translation>&amp;Vernietig</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="541"/>
        <source>&amp;Save</source>
        <translation>&amp;Stoor</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="543"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="303"/>
        <source>BMI</source>
        <translation>BMI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="544"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="299"/>
        <source>Weight</source>
        <translation>Massa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="545"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="304"/>
        <source>Zombie</source>
        <translation>Zombie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="546"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="185"/>
        <source>Pulse Rate</source>
        <translation>Polstempo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="547"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="190"/>
        <source>SpO2</source>
        <translation>SpO2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="548"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="194"/>
        <source>Plethy</source>
        <translatorcomment>**???</translatorcomment>
        <translation>Plethy</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="549"/>
        <source>Pressure</source>
        <translation>Druk</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="551"/>
        <source>Daily</source>
        <translation>Daagliks</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="552"/>
        <source>Profile</source>
        <translation>Profiel</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="553"/>
        <source>Overview</source>
        <translation>Oorsig</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="554"/>
        <source>Oximetry</source>
        <translation>Oximetrie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="556"/>
        <source>Oximeter</source>
        <translation>Oximeter</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="557"/>
        <source>Event Flags</source>
        <translation>Gebeurtenis Merkers</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="560"/>
        <source>Default</source>
        <translation>Verstek</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="563"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3462"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3004"/>
        <source>CPAP</source>
        <translation>CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="564"/>
        <source>BiPAP</source>
        <translation>BiPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="565"/>
        <source>Bi-Level</source>
        <translation>Bi-Level</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="566"/>
        <source>EPAP</source>
        <translation>EPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="567"/>
        <source>Min EPAP</source>
        <translation>Min EPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="568"/>
        <source>Max EPAP</source>
        <translation>Maks EPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="569"/>
        <source>IPAP</source>
        <translation>IPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="570"/>
        <source>Min IPAP</source>
        <translation>Min IPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="571"/>
        <source>Max IPAP</source>
        <translation>Maks IPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="572"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3005"/>
        <source>APAP</source>
        <translation>APAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="573"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3011"/>
        <source>ASV</source>
        <translation>ASV</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="574"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="292"/>
        <source>AVAPS</source>
        <translation>AVAPS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="575"/>
        <source>ST/ASV</source>
        <translation>ST/ASV</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="577"/>
        <source>Humidifier</source>
        <translation>Bevogtiger</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="579"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="146"/>
        <source>H</source>
        <translation>H</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="580"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="144"/>
        <source>OA</source>
        <translation>OA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="581"/>
        <source>A</source>
        <translation>A</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="582"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="142"/>
        <source>CA</source>
        <translation>CA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="583"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="150"/>
        <source>FL</source>
        <translation>FL</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="584"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="172"/>
        <source>SA</source>
        <translation>SA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="585"/>
        <source>LE</source>
        <translation>LE</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="586"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="169"/>
        <source>EP</source>
        <translation>EP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="587"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="154"/>
        <source>VS</source>
        <translation>VS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="589"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="156"/>
        <source>VS2</source>
        <translation>VS2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="590"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="152"/>
        <source>RERA</source>
        <translation>RERA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="591"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3483"/>
        <source>PP</source>
        <translation>PP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="592"/>
        <source>P</source>
        <translation>P</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="593"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="152"/>
        <source>RE</source>
        <translation>RE</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="594"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="166"/>
        <source>NR</source>
        <translation>NR</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="595"/>
        <source>NRI</source>
        <translation>NRI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="596"/>
        <source>O2</source>
        <translation>O2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="597"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="200"/>
        <source>PC</source>
        <translation>PC</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="598"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="175"/>
        <source>UF1</source>
        <translation>UF1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="599"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="178"/>
        <source>UF2</source>
        <translation>UF2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="600"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="181"/>
        <source>UF3</source>
        <translation>UF3</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="602"/>
        <source>PS</source>
        <translation>PS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="603"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="256"/>
        <source>AHI</source>
        <translation>AHI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="604"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="265"/>
        <source>RDI</source>
        <translation>RDI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="605"/>
        <source>AI</source>
        <translation>AI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="606"/>
        <source>HI</source>
        <translation>HI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="607"/>
        <source>UAI</source>
        <translation>UAI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="608"/>
        <source>CAI</source>
        <translation>CAI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="609"/>
        <source>FLI</source>
        <translation>FLI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="611"/>
        <source>REI</source>
        <translation>REI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="612"/>
        <source>EPI</source>
        <translation>EPI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="613"/>
        <source>ÇSR</source>
        <translation>ÇSR</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="614"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="140"/>
        <source>PB</source>
        <translation>PB</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="618"/>
        <source>IE</source>
        <translation>IE</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="619"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="241"/>
        <source>Insp. Time</source>
        <translation>Inasemtyd</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="620"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="238"/>
        <source>Exp. Time</source>
        <translation>Uitasemtyd</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="621"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="244"/>
        <source>Resp. Event</source>
        <translatorcomment>*???</translatorcomment>
        <translation>Resp. Gebeurtenis</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="622"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="150"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="247"/>
        <source>Flow Limitation</source>
        <translatorcomment>*???</translatorcomment>
        <translation>Vloeibeperking</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="623"/>
        <source>Flow Limit</source>
        <translation>Vloeilimiet</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="624"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="172"/>
        <source>SensAwake</source>
        <translation>SensAwake</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="625"/>
        <source>Pat. Trig. Breath</source>
        <translatorcomment>????</translatorcomment>
        <translation>Pat. Trig. Breath</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="626"/>
        <source>Tgt. Min. Vent</source>
        <translatorcomment>??????</translatorcomment>
        <translation>Tgt. Min. Vent</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="627"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="250"/>
        <source>Target Vent.</source>
        <translatorcomment>????</translatorcomment>
        <translation>Target Vent.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="628"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="222"/>
        <source>Minute Vent.</source>
        <translatorcomment>????</translatorcomment>
        <translation>Minuut vent.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="629"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="216"/>
        <source>Tidal Volume</source>
        <translatorcomment>*???</translatorcomment>
        <translation>Gety Volume</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="630"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="225"/>
        <source>Resp. Rate</source>
        <translation>Resp. Tempo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="631"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="219"/>
        <source>Snore</source>
        <translation>Snork</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="632"/>
        <source>Leak</source>
        <translation>Lek</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="633"/>
        <source>Leaks</source>
        <translation>Lekke</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="634"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="163"/>
        <source>Large Leak</source>
        <translation>Groot Lek</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="635"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="163"/>
        <source>LL</source>
        <translation>LL</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="636"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="259"/>
        <source>Total Leaks</source>
        <translation>Totale Lekke</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="637"/>
        <source>Unintentional Leaks</source>
        <translatorcomment>*?</translatorcomment>
        <translation>Onbedoelde Lekke</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="638"/>
        <source>MaskPressure</source>
        <translation>Maskerdruk</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="639"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="207"/>
        <source>Flow Rate</source>
        <translation>Vloeitempo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="640"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="315"/>
        <source>Sleep Stage</source>
        <translation>Slaap Stadium</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="641"/>
        <source>Usage</source>
        <translation>Gebruik</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="642"/>
        <source>Sessions</source>
        <translation>Sessies</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="643"/>
        <source>Pr. Relief</source>
        <translatorcomment>*?</translatorcomment>
        <translation>Druk Verligting</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="646"/>
        <location filename="../oscar/SleepLib/journal.cpp" line="25"/>
        <source>OSCAR</source>
        <translation>OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="529"/>
        <source>No Data Available</source>
        <translation>Geen Data Beskikbaar Nie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="490"/>
        <source>Software Engine</source>
        <translation>Sagteware Engine</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="491"/>
        <source>ANGLE / OpenGLES</source>
        <translation>ANGLE / OpenGLES</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="492"/>
        <source>Desktop OpenGL</source>
        <translation>Desktop OpenGL</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="494"/>
        <source> m</source>
        <translation> m</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="495"/>
        <source> cm</source>
        <translation> cm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="645"/>
        <source>Bookmarks</source>
        <translation>Boekmerke</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="647"/>
        <source>v%1</source>
        <translation>v%1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="649"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="2999"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3001"/>
        <source>Mode</source>
        <translation>Mode</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="650"/>
        <source>Model</source>
        <translation>Model</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="651"/>
        <source>Brand</source>
        <translation>Maak</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="652"/>
        <source>Serial</source>
        <translation>Serie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="653"/>
        <source>Series</source>
        <translation>Reeks</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="654"/>
        <source>Machine</source>
        <translation>Masjien</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="655"/>
        <source>Channel</source>
        <translation>Kanaal</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="656"/>
        <source>Settings</source>
        <translation>Instellings</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="658"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="272"/>
        <source>Inclination</source>
        <translatorcomment>**??? Depends on use. Can also be &quot;geneigdheid&quot; or &quot;hoek&quot;</translatorcomment>
        <translation>Gradiënt</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="659"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="269"/>
        <source>Orientation</source>
        <translation>Oriëntasie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="661"/>
        <source>Name</source>
        <translation>Naam</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="662"/>
        <source>DOB</source>
        <translation>Geb.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="663"/>
        <source>Phone</source>
        <translation>Foon</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="664"/>
        <source>Address</source>
        <translation>Adres</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="665"/>
        <source>Email</source>
        <translation>Epos</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="666"/>
        <source>Patient ID</source>
        <translation>Pasiënt ID</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="667"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="669"/>
        <source>Bedtime</source>
        <translation>Slaaptyd</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="670"/>
        <source>Wake-up</source>
        <translation>Opstaantyd</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="671"/>
        <source>Mask Time</source>
        <translation>Masker Tyd</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="672"/>
        <location filename="../oscar/SleepLib/loader_plugins/weinmann_loader.h" line="124"/>
        <source>Unknown</source>
        <translation>Onbekend</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="673"/>
        <source>None</source>
        <translation>Geen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="674"/>
        <source>Ready</source>
        <translation>Gereed</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="676"/>
        <source>First</source>
        <translation>Eerste</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="677"/>
        <source>Last</source>
        <translation>Laaste</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="678"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="305"/>
        <source>Start</source>
        <translation>Begin</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="679"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="306"/>
        <source>End</source>
        <translation>Einde</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="680"/>
        <source>On</source>
        <translation>Aan</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="681"/>
        <source>Off</source>
        <translation>Af</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="682"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3529"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="683"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3530"/>
        <source>No</source>
        <translation>Nee</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="685"/>
        <source>Min</source>
        <translation>Min</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="686"/>
        <source>Max</source>
        <translation>Maks</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="687"/>
        <source>Med</source>
        <translation>Med</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="689"/>
        <source>Average</source>
        <translation>Gemiddeld</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="690"/>
        <source>Median</source>
        <translation>Mediaan</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="202"/>
        <location filename="../oscar/SleepLib/common.cpp" line="691"/>
        <source>Avg</source>
        <translation>Gem</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="200"/>
        <location filename="../oscar/SleepLib/common.cpp" line="692"/>
        <source>W-Avg</source>
        <translation>W-Gem</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="576"/>
        <source>Non Data Capable Machine</source>
        <translation>Masjien het Geen Data Vermoë</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="577"/>
        <source>Your Philips Respironics CPAP machine (Model %1) is unfortunately not a data capable model.</source>
        <translation>U Philips Respironics CPAP masjien (Model %1) is kan ongelukkig nie data verskaf nie.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="246"/>
        <source>RemStar Plus Compliance Only</source>
        <translation>RemStar Plus Voldoening Alleenlik</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="249"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3454"/>
        <source>RemStar Pro with C-Flex+</source>
        <translation>RemStar Pro met C-Flex+</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="252"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3455"/>
        <source>RemStar Auto with A-Flex</source>
        <translation>RemStar Auto met A-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="255"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3456"/>
        <source>RemStar BiPAP Pro with Bi-Flex</source>
        <translation>RemStar BiPAP Pro met Bi-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="258"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3457"/>
        <source>RemStar BiPAP Auto with Bi-Flex</source>
        <translation>RemStar BiPAP Auto met Bi-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="261"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3459"/>
        <source>BiPAP autoSV Advanced</source>
        <translation>BiPAP autoSV Advanced</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="264"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3461"/>
        <source>BiPAP AVAPS</source>
        <translation>BiPAP AVAPS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="267"/>
        <source>Unknown Model</source>
        <translation>Onbekende Model</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="275"/>
        <source>System One (60 Series)</source>
        <translation>System One (60 Reeks)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="278"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="345"/>
        <source>DreamStation</source>
        <translation>DreamStation</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="281"/>
        <source>unknown</source>
        <translation>onbekend</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="515"/>
        <source>Getting Ready...</source>
        <translation>Maak Gereed...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="588"/>
        <source>Machine Unsupported</source>
        <translation>Masjie nie ge-ondersteun nie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="589"/>
        <source>Sorry, your Philips Respironics CPAP machine (Model %1) is not supported yet.</source>
        <translation>Jammer, u Philips Respironics CPAP masjien (Model %1) word nog nie ondersteun nie.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="578"/>
        <source>I&apos;m sorry to report that OSCAR can only track hours of use and very basic settings for this machine.</source>
        <translation>Ongelukkig kan OSCAR slegs gebruiksure en basiese instellings van hierdie toestel rapporteer.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="590"/>
        <source>The developers needs a .zip copy of this machines&apos; SD card and matching Encore .pdf reports to make it work with OSCAR.</source>
        <translation>Die ontwikkelaars benodig &apos;n .zip afskrif van hierdie masjien se SD kaart en ooreenstemmende Encore .pdf verslae om dit te laat werk met OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="631"/>
        <source>Scanning Files...</source>
        <translation>Skandeer Lêers...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="763"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="1980"/>
        <source>Importing Sessions...</source>
        <translation>Sessie Invoer...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="768"/>
        <source>Finishing up...</source>
        <translation>Voltooiing...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2240"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3574"/>
        <source>15mm</source>
        <translation>15 mm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2240"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3573"/>
        <source>22mm</source>
        <translation>22 mm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3458"/>
        <source>RemStar Plus</source>
        <translation>RemStar Plus</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3460"/>
        <source>BiPAP autoSV Advanced 60 Series</source>
        <translation>BiPAP autoSV Advanced 60 Series</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3463"/>
        <source>CPAP Pro</source>
        <translation>CPAP Pro</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3464"/>
        <source>Auto CPAP</source>
        <translation>Auto CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3465"/>
        <source>BiPAP Pro</source>
        <translation>BiPAP Pro</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3466"/>
        <source>Auto BiPAP</source>
        <translation>Auto BiPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3487"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3489"/>
        <source>Flex Mode</source>
        <translation>Flex Mode</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3488"/>
        <source>PRS1 pressure relief mode.</source>
        <translation>PRS1 drukverligting mode.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3494"/>
        <source>C-Flex</source>
        <translation>C-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3495"/>
        <source>C-Flex+</source>
        <translation>C-Flex+</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3496"/>
        <source>A-Flex</source>
        <translation>A-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3497"/>
        <source>Rise Time</source>
        <translation>Stygtyd</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3498"/>
        <source>Bi-Flex</source>
        <translation>Bi-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3502"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3504"/>
        <source>Flex Level</source>
        <translation>Flex Vlak</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3503"/>
        <source>PRS1 pressure relief setting.</source>
        <translation>PRS1 drukverligting instelling.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3508"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3539"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3561"/>
        <source>x1</source>
        <translation>x1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3509"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3540"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3562"/>
        <source>x2</source>
        <translation>x2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3510"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3541"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3563"/>
        <source>x3</source>
        <translation>x3</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3511"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3542"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3564"/>
        <source>x4</source>
        <translation>x4</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3512"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3543"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3565"/>
        <source>x5</source>
        <translation>x5</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3516"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3518"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3056"/>
        <source>Humidifier Status</source>
        <translation>Bevogtiger Status</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3517"/>
        <source>PRS1 humidifier connected?</source>
        <translation>Is die PRS1 bevogtiger gekoppel?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3520"/>
        <source>Disconnected</source>
        <translation>Ontkoppel</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3521"/>
        <source>Connected</source>
        <translation>Gekoppel</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3525"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3527"/>
        <source>Heated Tubing</source>
        <translation>Verhitte Pyp</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3526"/>
        <source>Heated Tubing Connected</source>
        <translation>Verhitte Pyp Gekoppel</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3534"/>
        <source>Humidification Level</source>
        <translation>Bevogtigingsvlak</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3535"/>
        <source>PRS1 Humidification level</source>
        <translation>PRS1 Bevogtigingsvlak</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3536"/>
        <source>Humid. Lvl.</source>
        <translation>Bev. niv.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3547"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3548"/>
        <source>System One Resistance Status</source>
        <translation>System One Resistance Status</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3549"/>
        <source>Sys1 Resist. Status</source>
        <translation>Sys1 Resist. Status</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3556"/>
        <source>System One Resistance Setting</source>
        <translation>System One Resistance Setting</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3557"/>
        <source>System One Mask Resistance Setting</source>
        <translation>System One Mask Resistance Setting</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3558"/>
        <source>Sys1 Resist. Set</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3569"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3571"/>
        <source>Hose Diameter</source>
        <translation>Pyp Deursneë</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3570"/>
        <source>Diameter of primary CPAP hose</source>
        <translation>Deursneë van primêre CPAP pyp</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3578"/>
        <source>System One Resistance Lock</source>
        <translation>System One Resistance Lock</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3579"/>
        <source>Whether System One resistance settings are available to you.</source>
        <translation>Of System One weerstand stellins beskikbaar is aan u.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3580"/>
        <source>Sys1 Resist. Lock</source>
        <translation>Sys1 Resist. Lock</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3587"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3589"/>
        <source>Auto On</source>
        <translation>Outo Aan</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3588"/>
        <source>A few breaths automatically starts machine</source>
        <translation>&apos;n Paar asemteue begin outomaties die masjien</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3596"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3598"/>
        <source>Auto Off</source>
        <translation>Outo af</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3597"/>
        <source>Machine automatically switches off</source>
        <translation>Masjien skakel outomaties af</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3605"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3607"/>
        <source>Mask Alert</source>
        <translation>Masker Waarskuwing</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3606"/>
        <source>Whether or not machine allows Mask checking.</source>
        <translation>Of die masjien toelaat dat die Masker nagegaan word.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3614"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3616"/>
        <source>Show AHI</source>
        <translation>Vertoon AHI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3615"/>
        <source>Whether or not machine shows AHI via LCD panel.</source>
        <translation>Of die majien die AHI op die LCD paneel vertoon.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3628"/>
        <source>Unknown PRS1 Code %1</source>
        <translation>Onbekende PRS1 Kode %1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3629"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3630"/>
        <source>PRS1_%1</source>
        <translation>PRS1_%1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3686"/>
        <source>Breathing Not Detected</source>
        <translation>Asemhaling Nie Waargeneem Nie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3687"/>
        <source>A period during a session where the machine could not detect flow.</source>
        <translation>&apos;n Periode gedurende &apos;n sessie waartydens die masjien die vloei kon waarneem nie.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3688"/>
        <source>BND</source>
        <translation>BND</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3703"/>
        <source>Timed Breath</source>
        <translation>Gemete Asemhaling</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3704"/>
        <source>Machine Initiated Breath</source>
        <translation>Masjien Heinisieerde Asemhaling</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3705"/>
        <source>TB</source>
        <translation>TB</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="36"/>
        <source>Windows User</source>
        <translation>Windows Gebruiker</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="200"/>
        <source>Using </source>
        <translation>Gebruik </translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="200"/>
        <source>, found SleepyHead -
</source>
        <translation>, het SleepyHead gevind -
</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="201"/>
        <source>You must run the OSCAR Migration Tool</source>
        <translation>U moet die OSCAR Migrasie Hulpmiddel gebruik</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="510"/>
        <source>&lt;i&gt;Your old machine data should be regenerated provided this backup feature has not been disabled in preferences during a previous data import.&lt;/i&gt;</source>
        <translation>&lt;i&gt;U ou masjien data moet hergenereer word gegewe dat die rugsteun funksie nie afgeskakel was tydens vorige data intrek sessies nie.&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="443"/>
        <source>Launching Windows Explorer failed</source>
        <translation>Kon nie Windows Explorer oopmaak nie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="444"/>
        <source>Could not find explorer.exe in path to launch Windows Explorer.</source>
        <translation>Kon nie explorer.exe vind om Windows Explorer oop te maak nie.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="496"/>
        <source>OSCAR (%1) needs to upgrade its database for %2 %3 %4</source>
        <translation>OSCAR (%1) het nodig om die databasis op te gradeer vir %2 %3 %4</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="509"/>
        <source>&lt;b&gt;OSCAR maintains a backup of your devices data card that it uses for this purpose.&lt;/b&gt;</source>
        <translation>&lt;b&gt;OSCAR behou &apos;n rugsteun van u toestel se data kaart vir hierdie doel.&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="513"/>
        <source>OSCAR does not yet have any automatic card backups stored for this device.</source>
        <translation>OSCAR het nog geen outomatiese kaart rugsteun vir hierdie toestel nie.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="514"/>
        <source>This means you will need to import this machine data again afterwards from your own backups or data card.</source>
        <translation>Dit beteken dat u die masjien se data agterna weer sal moet intrek van u eie rugsteun of data kaart af.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="517"/>
        <source>Important:</source>
        <translation>Belangrik:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="517"/>
        <source>Once you upgrade, you &lt;font size=+1&gt;can not&lt;/font&gt; use this profile with the previous version anymore.</source>
        <translation>As u opgradeer, &lt;font size=+1&gt;kan u nie&lt;/font&gt; hierdie profiel meer met die vorige profiel gebruik nie.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="518"/>
        <source>If you are concerned, click No to exit, and backup your profile manually, before starting OSCAR again.</source>
        <translation>As u bekommerd is, kies Nee om uit te gaan en rugsteun u profiel handrolies, voordat u OSCAR weer begin.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="519"/>
        <source>Are you ready to upgrade, so you can run the new version of OSCAR?</source>
        <translation>Is u gereed om te opgradeer, sodat u die nuwe weergawe van OSCAR kan begin?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="533"/>
        <source>Sorry, the purge operation failed, which means this version of OSCAR can&apos;t start.</source>
        <translation>Jammer, die uitvee van die profiel was onsuksesvol, wat beteken dat hierdie weergawe van OSCAR nie kan begin nie.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="546"/>
        <source>Would you like to switch on automatic backups, so next time a new version of OSCAR needs to do so, it can rebuild from these?</source>
        <translation>Wil u outomatiese rugsteun aanskakel, sodat wanneer &apos;n nuwe weergawe van OSCAR nodig het om dit te doen, dit hiervandaan kan herbou?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="553"/>
        <source>OSCAR will now start the import wizard so you can reinstall your %1 data.</source>
        <translation>OSCAR sal nou die invoer helper begin dat u u %1 data kan invoer.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="563"/>
        <source>OSCAR will now exit, then (attempt to) launch your computers file manager so you can manually back your profile up:</source>
        <translation>OSCAR sal nou stop, waarna dit sal probeer om u rekenaar se file manager te begin dat u handrolies u profiel kan rugsteun:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="565"/>
        <source>Use your file manager to make a copy of your profile directory, then afterwards, restart OSCAR and complete the upgrade process.</source>
        <translation>Gebruik u file manager om &apos;n afskrif te maak van u profiel directory. Daarna kan u OSCAR weer begin om die opgraderingsproses te voltooi.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="522"/>
        <source>Machine Database Changes</source>
        <translation>Masjien Databasis Veranderinge</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="534"/>
        <source>The machine data folder needs to be removed manually.</source>
        <translation>The masjien se data vouer moet handmatig deur uself verwyder word.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="535"/>
        <source>This folder currently resides at the following location:</source>
        <translation>Hierdie lêer is tans teenwoordig in die volgende plek:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="541"/>
        <source>Rebuilding from %1 Backup</source>
        <translation>Herbou van %1 Rugsteun</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="110"/>
        <source>Couldn&apos;t parse Channels.xml, this build is seriously borked, no choice but to abort!!</source>
        <translation>Kon nie Channels.xml ontleed nie. Ernstige foute lei tot stop van proses!</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="121"/>
        <source>Therapy Pressure</source>
        <translation>Terapie Druk</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="122"/>
        <source>Inspiratory Pressure</source>
        <translation>Inaseming Druk</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="123"/>
        <source>Lower Inspiratory Pressure</source>
        <translation>Laer Inaseming Druk</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="124"/>
        <source>Higher Inspiratory Pressure</source>
        <translation>Hoër Inaseming Druk</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="125"/>
        <source>Expiratory Pressure</source>
        <translation>Uitaseming Druk</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="126"/>
        <source>Lower Expiratory Pressure</source>
        <translation>Laer Uitaseming Druk</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="127"/>
        <source>Higher Expiratory Pressure</source>
        <translation>Hoër Uitaseming Druk</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="128"/>
        <source>Pressure Support</source>
        <translation>Druk Ondersteuning</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="129"/>
        <source>PS Min</source>
        <translation>PS Min</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="129"/>
        <source>Pressure Support Minimum</source>
        <translation>Druk Ondersteuning Minimum</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="130"/>
        <source>PS Max</source>
        <translation>PS Maks</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="130"/>
        <source>Pressure Support Maximum</source>
        <translation>Druk Ondersteuning Maksimum</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="131"/>
        <source>Min Pressure</source>
        <translation>Min Druk</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="131"/>
        <source>Minimum Therapy Pressure</source>
        <translation>Minimum Terapie Druk</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="132"/>
        <source>Max Pressure</source>
        <translation>Maks Druk</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="132"/>
        <source>Maximum Therapy Pressure</source>
        <translation>Maksimum Terapie Druk</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="133"/>
        <source>Ramp Time</source>
        <translation>Helling Tyd</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="133"/>
        <source>Ramp Delay Period</source>
        <translation>Helling Vertraging Periode</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="134"/>
        <source>Ramp Pressure</source>
        <translation>Helling Druk</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="134"/>
        <source>Starting Ramp Pressure</source>
        <translation>Begin Helling Druk</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="135"/>
        <source>Ramp Event</source>
        <translation>Helling Gebeurtenis</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3134"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3136"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="135"/>
        <source>Ramp</source>
        <translation>Helling</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="156"/>
        <source>Vibratory Snore (VS2) </source>
        <translation>Vibratory Snore (VS2) </translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="275"/>
        <source>Mask On Time</source>
        <translation>Masker Op Tyd</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="275"/>
        <source>Time started according to str.edf</source>
        <translation>Tyd begin volgens str.edf</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="278"/>
        <source>Summary Only</source>
        <translation>Opsomming Alleenlik</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="510"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="142"/>
        <source>An apnea where the airway is open</source>
        <translation>&apos;n Apneë waar die lugweg oop is</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="144"/>
        <source>An apnea caused by airway obstruction</source>
        <translation>&apos;n Apneë veroorsaak deur lugweg obstruksie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="146"/>
        <source>Hypopnea</source>
        <translation>Hypopneë</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="146"/>
        <source>A partially obstructed airway</source>
        <translation>&apos;n Gedeeltelik-geblokte lugweg</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="148"/>
        <source>Unclassified Apnea</source>
        <translation>Ongeklassifiseerde Apneë</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="148"/>
        <source>UA</source>
        <translation>UA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="154"/>
        <source>Vibratory Snore</source>
        <translation>Vibrerende Snork</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="154"/>
        <source>A vibratory snore</source>
        <translation>&apos;n Vibrerende Snork</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="156"/>
        <source>A vibratory snore as detcted by a System One machine</source>
        <translation>&apos;n Vibrerende snork soos waargeneem deur &apos;n System One masjien</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3481"/>
        <source>Pressure Pulse</source>
        <translation>Druk Puls</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3482"/>
        <source>A pulse of pressure &apos;pinged&apos; to detect a closed airway.</source>
        <translation>&apos;n Druk puls wat gebruik word om &apos;n gelote lugweg waar te neem.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="159"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="163"/>
        <source>A large mask leak affecting machine performance.</source>
        <translation>&apos;n Groot maskerlek wat masjienverrigting beinvloed.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="166"/>
        <source>Non Responding Event</source>
        <translation>Nie-reaksie Gebeurtenis</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="166"/>
        <source>A type of respiratory event that won&apos;t respond to a pressure increase.</source>
        <translation>&apos;n Tipe respiratoriese gebeurtenis wat nie op &apos;n toename in druk reageer nie.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="169"/>
        <source>Expiratory Puff</source>
        <translation>Uitgaande Teug</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="169"/>
        <source>Intellipap event where you breathe out your mouth.</source>
        <translation>Intellipap gebeurtenis waar u deur u mond uitasem.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="172"/>
        <source>SensAwake feature will reduce pressure when waking is detected.</source>
        <translation>SensAwake funksie sal druk verminder wanneer die pasiënt wakker word.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="175"/>
        <source>User Flag #1</source>
        <translation>Gebruikersvlaggie #1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="178"/>
        <source>User Flag #2</source>
        <translation>Gebruikersvlaggie #2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="181"/>
        <source>User Flag #3</source>
        <translation>Gebruikersvlaggie #3</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="185"/>
        <source>Heart rate in beats per minute</source>
        <translation>Harttempo in slae per minuut</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="190"/>
        <source>SpO2 %</source>
        <translation>SpO2 %</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="190"/>
        <source>Blood-oxygen saturation percentage</source>
        <translation>Bloedsuurstof versadiging persentasie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="194"/>
        <source>Plethysomogram</source>
        <translation>Plethysomogram</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="194"/>
        <source>An optical Photo-plethysomogram showing heart rhythm</source>
        <translation>&apos;n Optiese Foto-plethysomogram wat hart ritme wys</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="200"/>
        <source>Pulse Change</source>
        <translation>Polsverandering</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="200"/>
        <source>A sudden (user definable) change in heart rate</source>
        <translation>&apos;n Skielike (gebruiker definieerbare) verandering in hartklop</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="203"/>
        <source>SpO2 Drop</source>
        <translation>SpO2 Afname</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="203"/>
        <source>A sudden (user definable) drop in blood oxygen saturation</source>
        <translation>&apos;n Skielike (gebruiker definieerbare) afname in bloedsuurstof versadiging</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="203"/>
        <source>SD</source>
        <translation>SD</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="207"/>
        <source>Breathing flow rate waveform</source>
        <translation>Asemhaling vloeitempo golfvorm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="513"/>
        <source>L/min</source>
        <translation>L/min</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="210"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="213"/>
        <source>Mask Pressure</source>
        <translation>Masker Druk</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="213"/>
        <source>Mask Pressure (High resolution)</source>
        <translation>Masker Druk (Hoë resolusie)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="216"/>
        <source>Amount of air displaced per breath</source>
        <translation>Hoeveelheid lug verplaas per asemteug</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="219"/>
        <source>Graph displaying snore volume</source>
        <translation>Grafiek vertoon snork volume</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="222"/>
        <source>Minute Ventilation</source>
        <translation>Minuut Ventilasie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="222"/>
        <source>Amount of air displaced per minute</source>
        <translation>Hoeveelheid lug verplaas per minuut</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="225"/>
        <source>Respiratory Rate</source>
        <translation>Respiratoriese Tempo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="225"/>
        <source>Rate of breaths per minute</source>
        <translation>Tempo van asemteue per minuut</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="228"/>
        <source>Patient Triggered Breaths</source>
        <translation>Pasiënt-gesnellerde Asemteue</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="228"/>
        <source>Percentage of breaths triggered by patient</source>
        <translation>Persentasie asemteue gesneller deur pasiënt</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="228"/>
        <source>Pat. Trig. Breaths</source>
        <translation>Pat. Trig. Breaths</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="231"/>
        <source>Leak Rate</source>
        <translation>Lek Tempo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="231"/>
        <source>Rate of detected mask leakage</source>
        <translation>Tempo van bespeurde maskerlek</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="235"/>
        <source>I:E Ratio</source>
        <translation>I:E Verhouding</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="235"/>
        <source>Ratio between Inspiratory and Expiratory time</source>
        <translation>Verhouding tussen Inasem en Uitasem tyd</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="518"/>
        <source>ratio</source>
        <translation>verhouding</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="131"/>
        <source>Pressure Min</source>
        <translation>Druk Min</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="132"/>
        <source>Pressure Max</source>
        <translation>Druk Maks</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="138"/>
        <source>Cheyne Stokes Respiration</source>
        <translation>Cheyne Stokes Respirasie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="138"/>
        <source>An abnormal period of Cheyne Stokes Respiration</source>
        <translation>&apos;n Abnormale periode van Cheyne Stokes Respirasie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="138"/>
        <source>CSR</source>
        <translation>CSR</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="140"/>
        <source>Periodic Breathing</source>
        <translation>Ongereëlde Asemhaling</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="140"/>
        <source>An abnormal period of Periodic Breathing</source>
        <translation>&apos;n Abnormale tydperk van Ongereëlde Asemhaling</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="142"/>
        <source>Clear Airway</source>
        <translation>Oop Lugweg</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="144"/>
        <source>Obstructive</source>
        <translation>Obstruktiewe</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="148"/>
        <source>An apnea that couldn&apos;t be determined as Central or Obstructive.</source>
        <translation>&apos;n Apneë wat nie bepaal kon word as Sentraal of Obstruktief nie.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="150"/>
        <source>A restriction in breathing from normal, causing a flattening of the flow waveform.</source>
        <translation>&apos;n Beperking in asemhaling wat afplatting van die vloi golfvorm veroorsaak.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="152"/>
        <source>Respiratory Effort Related Arousal: An restriction in breathing that causes an either an awakening or sleep disturbance.</source>
        <translation>Respiratory Effort Related Arousal: &apos;n Beperking in asemhaling wat of veroorsaak dat die pasiënt wakker word of wat slaapversteuring tot gevolg het.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="159"/>
        <source>Leak Flag</source>
        <translation>Lek Vlag</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="159"/>
        <source>LF</source>
        <translation>LF</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="175"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="178"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="181"/>
        <source>A user definable event detected by OSCAR&apos;s flow waveform processor.</source>
        <translation>&apos;n Gebruiker definieerbare gebeurtenis waargeneem deru OSCAR se vloei golfvorm prosesseerder.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="197"/>
        <source>Perfusion Index</source>
        <translation>Perfusie Indeks</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="197"/>
        <source>A relative assessment of the pulse strength at the monitoring site</source>
        <translation>&apos;n Relatiewe beoordeling van die polssterkte op die moniteringsplek</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="197"/>
        <source>Perf. Index %</source>
        <translation>Perf. Indeks %</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="238"/>
        <source>Expiratory Time</source>
        <translation>Uitasem Tyd</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="238"/>
        <source>Time taken to breathe out</source>
        <translation>Tyd wat dit neem om uit te asem</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="241"/>
        <source>Inspiratory Time</source>
        <translation>Inasem Tyd</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="241"/>
        <source>Time taken to breathe in</source>
        <translation>Tyd wat dit neem om in te asem</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="244"/>
        <source>Respiratory Event</source>
        <translation>Respiratoriese Gebeurtenis</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="244"/>
        <source>A ResMed data source showing Respiratory Events</source>
        <translation>&apos;n Resmed data bron wat respiratoriese gebeurtenisse wys</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="247"/>
        <source>Graph showing severity of flow limitations</source>
        <translation>Grafiek wat die omvang van vloeibeperkings toon</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="247"/>
        <source>Flow Limit.</source>
        <translation>Vloei Lim.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="250"/>
        <source>Target Minute Ventilation</source>
        <translation>Minuut Ventilasie Teiken</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="253"/>
        <source>Maximum Leak</source>
        <translation>Maksimum Lek</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="253"/>
        <source>The maximum rate of mask leakage</source>
        <translation>Die maksimum tempo van maskerlek</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="253"/>
        <source>Max Leaks</source>
        <translation>Maks Lek</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="256"/>
        <source>Apnea Hypopnea Index</source>
        <translation>Apneë Hypopneë Indeks</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="256"/>
        <source>Graph showing running AHI for the past hour</source>
        <translation>Grafiek wat die lopende AHI vir die laaste uur wys</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="259"/>
        <source>Total Leak Rate</source>
        <translation>Totale Lek Tempo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="259"/>
        <source>Detected mask leakage including natural Mask leakages</source>
        <translation>Waargenome maskerlek, insluitend natuurlike Maskerlekkasies</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="262"/>
        <source>Median Leak Rate</source>
        <translation>Median Lek Tempo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="262"/>
        <source>Median rate of detected mask leakage</source>
        <translation>Median tempo van waargenome maskerlek</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="262"/>
        <source>Median Leaks</source>
        <translation>Median Lek</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="265"/>
        <source>Respiratory Disturbance Index</source>
        <translation>Respiratoriese Versteuring Indeks</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="265"/>
        <source>Graph showing running RDI for the past hour</source>
        <translation>Grafiek wat RDI vir die afgelope uur vertoon</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="269"/>
        <source>Sleep position in degrees</source>
        <translation>Slaap posisie in grade</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="272"/>
        <source>Upright angle in degrees</source>
        <translation>Regop hoek in grade</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="278"/>
        <source>CPAP Session contains summary data only</source>
        <translation>CPAP Sessie bevat slegs opsommende data</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="282"/>
        <source>PAP Mode</source>
        <translation>PAP Mode</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="282"/>
        <source>PAP Device Mode</source>
        <translation>PAP Toestel Mode</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="286"/>
        <source>APAP (Variable)</source>
        <translation>APAP (Veranderbaar)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="290"/>
        <source>ASV (Fixed EPAP)</source>
        <translation>ASV (Vaste EPAP)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="291"/>
        <source>ASV (Variable EPAP)</source>
        <translation>ASV (Veranderbare EPAP)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="300"/>
        <source>Height</source>
        <translation>Lengte</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="300"/>
        <source>Physical Height</source>
        <translation>Fisiese Lengte</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="301"/>
        <source>Notes</source>
        <translation>Notas</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="301"/>
        <source>Bookmark Notes</source>
        <translation>Boekmerk Notas</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="303"/>
        <source>Body Mass Index</source>
        <translation>Ligaams Massa Indeks</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="304"/>
        <source>How you feel (0 = like crap, 10 = unstoppable)</source>
        <translation>Hoe u voel (0 = sleg, 10 = wonderlik)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="305"/>
        <source>Bookmark Start</source>
        <translation>Boekmerk Begin</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="306"/>
        <source>Bookmark End</source>
        <translation>Boekmerk Einde</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="307"/>
        <source>Last Updated</source>
        <translation>Laaste Opgedateer</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="308"/>
        <source>Journal Notes</source>
        <translation>Joernaal Notas</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="308"/>
        <source>Journal</source>
        <translation>Joernaal</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="315"/>
        <source>1=Awake 2=REM 3=Light Sleep 4=Deep Sleep</source>
        <translation>1=Wakker 2=REM 3=Ligte Slaap 4=Diep Slaap</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="317"/>
        <source>Brain Wave</source>
        <translation>Breingolf</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="317"/>
        <source>BrainWave</source>
        <translation>Breingolf</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="318"/>
        <source>Awakenings</source>
        <translation>Ontwakings</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="318"/>
        <source>Number of Awakenings</source>
        <translation>Aantal Ontwakings</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="319"/>
        <source>Morning Feel</source>
        <translation>Oggend Gevoel</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="319"/>
        <source>How you felt in the morning</source>
        <translation>Hoe u gevoel het in die oggend</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="320"/>
        <source>Time Awake</source>
        <translation>Tyd Wakker</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="320"/>
        <source>Time spent awake</source>
        <translation>Tyd wakker gespandeer</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="321"/>
        <source>Time In REM Sleep</source>
        <translation>Tyd In REM Slaap</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="321"/>
        <source>Time spent in REM Sleep</source>
        <translation>Tyd gespandeer in REM Slaap</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="321"/>
        <source>Time in REM Sleep</source>
        <translation>Tyd in REM Slaap</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="322"/>
        <source>Time In Light Sleep</source>
        <translation>Tyd in Ligte Slaap</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="322"/>
        <source>Time spent in light sleep</source>
        <translation>Tyd gespandeer in ligte slaap</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="322"/>
        <source>Time in Light Sleep</source>
        <translation>Tyd in Ligte Slaap</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="323"/>
        <source>Time In Deep Sleep</source>
        <translation>Tyd In Diep Slaap</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="323"/>
        <source>Time spent in deep sleep</source>
        <translation>Tyd gespandeer in diep slaap</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="323"/>
        <source>Time in Deep Sleep</source>
        <translation>Tyd in Diep Slaap</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="324"/>
        <source>Time to Sleep</source>
        <translation>Tyd om te Slaap</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="324"/>
        <source>Time taken to get to sleep</source>
        <translation>Tyd geneem om te slaap</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="325"/>
        <source>Zeo ZQ</source>
        <translation>Zeo ZQ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="325"/>
        <source>Zeo sleep quality measurement</source>
        <translation>Zeo slaap kwaliteit meting</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="325"/>
        <source>ZEO ZQ</source>
        <translation>ZEO ZQ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="334"/>
        <source>Debugging channel #1</source>
        <translation>Ontfouting kanaal #1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="334"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="335"/>
        <source>Top secret internal stuff you&apos;re not supposed to see ;)</source>
        <translation>Super geheime goed wat u nie veronderstel is om te sien nie ;)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="334"/>
        <source>Test #1</source>
        <translation>Toets #1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="335"/>
        <source>Debugging channel #2</source>
        <translation>Ontfouting kanaal #2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="335"/>
        <source>Test #2</source>
        <translation>Toets #2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="730"/>
        <source>Zero</source>
        <translation>Zero</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="733"/>
        <source>Upper Threshold</source>
        <translation>Boonste Drempel</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="736"/>
        <source>Lower Threshold</source>
        <translation>Onderste Drempel</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="409"/>
        <source>As you did not select a data folder, OSCAR will exit.</source>
        <translation>Omdat u nie &apos;n data lêer gekies het nie, sal OSCAR nou uitgaan.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="179"/>
        <source>Choose the SleepyHead data folder to migrate</source>
        <translation>Kies die SleepyHead datavouer om te migreer</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="180"/>
        <source>or CANCEL to skip migration.</source>
        <translation>of CANCEL om migrasie oor te slaan.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="194"/>
        <source>The folder you chose does not contain valid SleepyHead data.</source>
        <translation>Die vouer wat u gekies het bevat nie geldige SleepyHead data nie.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="195"/>
        <source>You cannot use this folder:</source>
        <translation>U kan nie hierdie vouer gebruik nie:</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="210"/>
        <source>Migrating </source>
        <translation>Besig om te migreer </translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="210"/>
        <source> files</source>
        <translation> lêers</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="211"/>
        <source>from </source>
        <translation>vanaf </translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="211"/>
        <source>to </source>
        <translation>na </translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="396"/>
        <source>OSCAR will set up a folder for your data.</source>
        <translation>OSCAR sal &apos;n vouer skep vir u data.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="397"/>
        <source>If you have been using SleepyHead, OSCAR can copy your old data to this folder later.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="398"/>
        <source>We suggest you use this folder: </source>
        <translation>Dit word voorgestel dat u die volgende vouer gebruik: </translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="399"/>
        <source>Click Ok to accept this, or No if you want to use a different folder.</source>
        <translation>Kies OK om hierdie te aanvaar, of No indien u &apos;n verskillende vouer wil gebruik.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="405"/>
        <source>Choose or create a new folder for OSCAR data</source>
        <translation>Kies of skep &apos;n nuwe vouer vir OSCAR data</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="410"/>
        <source>Next time you run OSCAR, you will be asked again.</source>
        <translation>Die volgende keer wat u OSCAR gebruik, sal u weer gevra word.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="421"/>
        <source>The folder you chose is not empty, nor does it already contain valid OSCAR data.</source>
        <translation>Die lêer wat u gekies het, is nie leeg nie, en bevat ook nie geldige OSCAR data nie.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="443"/>
        <source>Migrate SleepyHead Data?</source>
        <translation>Moet SleepyHead Data migreer word?</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="444"/>
        <source>On the next screen OSCAR will ask you to select a folder with SleepyHead data</source>
        <translation>Op die volgende skerm sal OSCAR u vra om &apos;n vouer met SleepyHead data te kies</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="445"/>
        <source>Click [OK] to go to the next screen or [No] if you do not wish to use any SleepyHead data.</source>
        <translation>Kies [OK] om na die volgende skerm te gaan of [No] indien u nie enige SleepyHead data wil gebruik nie.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="502"/>
        <source>The version of OSCAR you just ran is OLDER than the one used to create this data (%1).</source>
        <translation>Die weergawe van OSCAR wat u pas uitgevoer het, is OUER as die een wat gebruik is om hierdie data te skep (%1).</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="504"/>
        <source>It is likely that doing this will cause data corruption, are you sure you want to do this?</source>
        <translation>Dit is moontlik dat hierdie sal lei tot data korrupsie, is u seker dat u wil voortgaan?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="522"/>
        <source>Question</source>
        <translation>Vraag</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="408"/>
        <source>Exiting</source>
        <translation>Gaan uit</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="422"/>
        <source>Are you sure you want to use this folder?</source>
        <translation>Is u seker dat u hierdie vouer wil gebruik?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="253"/>
        <source>Don&apos;t forget to place your datacard back in your CPAP machine</source>
        <translation>Moenie vergeet om u data kaart terug te sit in u CPAP masjien nie</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="253"/>
        <source>OSCAR Reminder</source>
        <translation>OSCAR Herinnering</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="424"/>
        <source>You can only work with one instance of an individual OSCAR profile at a time.</source>
        <translation>U kan slegs met een instansie van &apos;n individuele OSCAR profiel werk op &apos;n keer.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="425"/>
        <source>If you are using cloud storage, make sure OSCAR is closed and syncing has completed first on the other computer before proceeding.</source>
        <translation>Indien u gebruik maak van cloud berging, maak seker dat OSCAR toegemaak is en dat sinkronisasie afgehandel is op die rekenaar voordat u voortgaan.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="438"/>
        <source>Loading profile &quot;%1&quot;...</source>
        <translation>Laai profiel &quot;%1&quot;...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2142"/>
        <source>Sorry, your %1 %2 machine is not currently supported.</source>
        <translation>Jammer, u %1 %2 masjien is nie tans ondersteun nie.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1167"/>
        <source>Are you sure you want to reset all your channel colors and settings to defaults?</source>
        <translation>Is u seker dat u alle kanaal kleure en instellings wil terugstel na die verstekwaardes?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1220"/>
        <source>Are you sure you want to reset all your waveform channel colors and settings to defaults?</source>
        <translation>Is u seker u wil al u golfvormkanaalkleure en -instellings na standaardinstellings herstel?</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="38"/>
        <source>There are no graphs visible to print</source>
        <translation>Daar is geen grafieke sigbaar om te druk nie</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="55"/>
        <source>Would you like to show bookmarked areas in this report?</source>
        <translation>Wil u graag boekmerk areas in hierdie verslag wys?</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="98"/>
        <source>Printing %1 Report</source>
        <translation>Druk %1 Verslag</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="132"/>
        <source>%1 Report</source>
        <translation>%1 Verslag</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="190"/>
        <source>: %1 hours, %2 minutes, %3 seconds
</source>
        <translation>: %1 ure, %2 minute, %3 sekondes
</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="236"/>
        <source>RDI	%1
</source>
        <translation>RDI	%1
</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="238"/>
        <source>AHI	%1
</source>
        <translation>AHI	%1
</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="271"/>
        <source>AI=%1 HI=%2 CAI=%3 </source>
        <translation>AI=%1 HI=%2 CAI=%3 </translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="277"/>
        <source>REI=%1 VSI=%2 FLI=%3 PB/CSR=%4%%</source>
        <translation>REI=%1 VSI=%2 FLI=%3 PB/CSR=%4%%</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="281"/>
        <source>UAI=%1 </source>
        <translation>UAI=%1 </translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="283"/>
        <source>NRI=%1 LKI=%2 EPI=%3</source>
        <translation>NRI=%1 LKI=%2 EPI=%3</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="352"/>
        <source>Reporting from %1 to %2</source>
        <translation>Verslaggewing van %1 tot %2</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="424"/>
        <source>Entire Day&apos;s Flow Waveform</source>
        <translation>Volle Dag se Vloei Golfvorm</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="430"/>
        <source>Current Selection</source>
        <translation>Huidige Keuse</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="440"/>
        <source>Entire Day</source>
        <translation>Volle Dag</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="555"/>
        <source>OSCAR v%1</source>
        <translation>OSCAR v%1</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="561"/>
        <source>Page %1 of %2</source>
        <translation>Bladsy %1 van %2</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="997"/>
        <source>Days: %1</source>
        <translation>Dae: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1001"/>
        <source>Low Usage Days: %1</source>
        <translation>Lae Gebruik Dae: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1002"/>
        <source>(%1% compliant, defined as &gt; %2 hours)</source>
        <translation>(%1% voldoening, gedefinieer as &gt; %2 ure)</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1118"/>
        <source>(Sess: %1)</source>
        <translation>(Sess: %1)</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1126"/>
        <source>Bedtime: %1</source>
        <translation>Bedtyd: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1128"/>
        <source>Waketime: %1</source>
        <translation>Wakkerwordtyd: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1168"/>
        <source>90%</source>
        <translation>90%</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1240"/>
        <source>(Summary Only)</source>
        <translation>(Slegs Opsomming)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="423"/>
        <source>There is a lockfile already present for this profile &apos;%1&apos;, claimed on &apos;%2&apos;.</source>
        <translation>Daar is &apos;n uitsluiting reeds teenwoordig vir hierdie profiel &apos;%1&apos;, aangevra op &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="154"/>
        <source>Peak</source>
        <translation>Piek</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="158"/>
        <source>%1% %2</source>
        <translation>%1% %2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="287"/>
        <source>Fixed Bi-Level</source>
        <translation>Vaste Bi-Level</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="288"/>
        <source>Auto Bi-Level (Fixed PS)</source>
        <translation>Outo Bi-Level (Vaste PS)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="289"/>
        <source>Auto Bi-Level (Variable PS)</source>
        <translation>Outo Bi-Level (Veranderbare PS)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1373"/>
        <source>%1%2</source>
        <translation>%1%2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1396"/>
        <source>Fixed %1 (%2)</source>
        <translation>Vaste %1 (%2)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1398"/>
        <source>Min %1 Max %2 (%3)</source>
        <translation>Min %1 Maks %2 (%3)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1400"/>
        <location filename="../oscar/SleepLib/day.cpp" line="1415"/>
        <source>EPAP %1 IPAP %2 (%3)</source>
        <translation>EPAP %1 IPAP %2 (%3)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1402"/>
        <source>PS %1 over %2-%3 (%4)</source>
        <translation>PS %1 oor %2-%3 (%4)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1404"/>
        <location filename="../oscar/SleepLib/day.cpp" line="1408"/>
        <source>Min EPAP %1 Max IPAP %2 PS %3-%4 (%5)</source>
        <translation>Min EPAP %1 Maks IPAP %2 PS %3-%4 (%5)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1406"/>
        <source>EPAP %1 PS %2-%3 (%6)</source>
        <translation>EPAP %1 PS %2-%3 (%6)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="145"/>
        <location filename="../oscar/SleepLib/day.cpp" line="147"/>
        <location filename="../oscar/SleepLib/day.cpp" line="149"/>
        <location filename="../oscar/SleepLib/day.cpp" line="154"/>
        <source>%1 %2</source>
        <translation>%1 %2</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="297"/>
        <source>Most recent Oximetry data: &lt;a onclick=&apos;alert(&quot;daily=%2&quot;);&apos;&gt;%1&lt;/a&gt; </source>
        <translation>Mees onlangse Oximetrie data: &lt;a onclick=&apos;alert(&quot;daily=%2&quot;);&apos;&gt;%1&lt;/a&gt; </translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="298"/>
        <source>(last night)</source>
        <translation>(laasnag)</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="299"/>
        <source>(yesterday)</source>
        <translation>(gister)</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="300"/>
        <source>(%2 day ago)</source>
        <translation>(%2 dae gelede)</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="305"/>
        <source>No oximetry data has been imported yet.</source>
        <translation>Geen oximetrie data is nog ingevoer ne.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.h" line="39"/>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.h" line="41"/>
        <source>Contec</source>
        <translation>Contec</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.h" line="39"/>
        <source>CMS50</source>
        <translation>CMS50</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.h" line="77"/>
        <source>Fisher &amp; Paykel</source>
        <translation>Fisher &amp; Paykel</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.h" line="77"/>
        <source>ICON</source>
        <translation>ICON</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.h" line="77"/>
        <source>DeVilbiss</source>
        <translation>DeVilbiss</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.h" line="77"/>
        <source>Intellipap</source>
        <translation>Intellipap</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.h" line="85"/>
        <source>SmartFlex Settings</source>
        <translation>SmartFlex Settings</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.h" line="39"/>
        <source>ChoiceMMed</source>
        <translation>ChoiceMMed</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.h" line="39"/>
        <source>MD300</source>
        <translation>MD300</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/mseries_loader.h" line="66"/>
        <source>Respironics</source>
        <translation>Respironics</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/mseries_loader.h" line="66"/>
        <source>M-Series</source>
        <translation>M-Series</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.h" line="221"/>
        <source>Philips Respironics</source>
        <translation>Philips Respironics</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="272"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.h" line="221"/>
        <source>System One</source>
        <translation>System One</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.h" line="446"/>
        <source>ResMed</source>
        <translation>ResMed</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.h" line="446"/>
        <source>S9</source>
        <translation>S9</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.h" line="455"/>
        <source>EPR: </source>
        <translation>EPR: </translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/somnopose_loader.h" line="37"/>
        <source>Somnopose</source>
        <translation>Somnopose</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/somnopose_loader.h" line="37"/>
        <source>Somnopose Software</source>
        <translation>Somnopose Software</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/zeo_loader.h" line="38"/>
        <source>Zeo</source>
        <translation>Zeo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/zeo_loader.h" line="38"/>
        <source>Personal Sleep Coach</source>
        <translation>Personal Sleep Coach</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="196"/>
        <source>Database Outdated
Please Rebuild CPAP Data</source>
        <translation>Databasis Verouderd
Herbou Asseblief Die CPAP Data</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="373"/>
        <source> (%2 min, %3 sec)</source>
        <translation> (%2 min, %3 s)</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="375"/>
        <source> (%3 sec)</source>
        <translation> (%3 s)</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="386"/>
        <source>Pop out Graph</source>
        <translation>Pop Grafiek Uit</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1406"/>
        <source>I&apos;m very sorry your machine doesn&apos;t record useful data to graph in Daily View :(</source>
        <translation>U masjien neem ongelukkig nie bruikbare data op om te wys in Daaglike Vertoon nie :(</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1409"/>
        <source>There is no data to graph</source>
        <translation>Daar is geen data om te vertoon nie</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1590"/>
        <source>d MMM [ %1 - %2 ]</source>
        <translation>d MMM [ %1 - %2 ]</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2111"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2154"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2225"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2242"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2206"/>
        <source>Hide All Events</source>
        <translation>Versteek Alle Gebeurtenisse</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2207"/>
        <source>Show All Events</source>
        <translation>Vertoon Alle Gebeurtenisse</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2548"/>
        <source>Unpin %1 Graph</source>
        <translation>Ontpin%1 Grafiek</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2550"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2625"/>
        <source>Popout %1 Graph</source>
        <translation>Popout %1 Grafiek</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2627"/>
        <source>Pin %1 Graph</source>
        <translation>Pin %1 Grafiek</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineChart.cpp" line="1036"/>
        <source>Plots Disabled</source>
        <translation>Plots Afgeskakel</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineChart.cpp" line="1110"/>
        <source>Duration %1:%2:%3</source>
        <translation>Tydsduur %1:%2:%3</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineChart.cpp" line="1111"/>
        <source>AHI %1</source>
        <translation>AHI %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="107"/>
        <source>%1: %2</source>
        <translation>%1: %2</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="150"/>
        <source>Relief: %1</source>
        <translation>Verligting: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="156"/>
        <source>Hours: %1h, %2m, %3s</source>
        <translation>Ure: %1h, %2m, %3s</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="259"/>
        <source>Machine Information</source>
        <translation>Masjien Inligting</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="25"/>
        <source>Journal Data</source>
        <translation>Joernaal Data</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="43"/>
        <source>OSCAR found an old Journal folder, but it looks like it&apos;s been renamed:</source>
        <translation>OSCAR het &apos;n ou Joernaal lêer gevind, maar dit lyk of die naam verander is:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="45"/>
        <source>OSCAR will not touch this folder, and will create a new one instead.</source>
        <translation>OSCAR sal nie aan die lêer raak nie en sal eerder &apos;n nuwe een skep.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="46"/>
        <source>Please be careful when playing in OSCAR&apos;s profile folders :-P</source>
        <translation>Wees asseblief versigtig wanneer u met OSCAR se profiel lêers speel :-P</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="53"/>
        <source>For some reason, OSCAR couldn&apos;t find a journal object record in your profile, but did find multiple Journal data folders.

</source>
        <translatorcomment>OSCAR kon nie &apos;n joernaal objek rekord in u profiel vind nie, maar het veelvuldige Joernaal data vouers gevind.</translatorcomment>
        <translation>OSCAR kon nie &apos;n joernaal objek rekord in u profiel vind nie, maar het veelvuldige Joernaal data vouers gevind.

</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="54"/>
        <source>OSCAR picked only the first one of these, and will use it in future:

</source>
        <translation>OSCARhet slegs die eerste hiervan gekies en sal dit in die toekoms gebruik:

</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="56"/>
        <source>If your old data is missing, copy the contents of all the other Journal_XXXXXXX folders to this one manually.</source>
        <translation>Indien u ou data weg is, verskuif die inhoud van al die ander Journal_XXXXXXX vouers handmatig na hierdie een toe.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.h" line="41"/>
        <source>CMS50F3.7</source>
        <translation>CMS50F3.7</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.h" line="41"/>
        <source>CMS50F</source>
        <translation>CMS50F</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1543"/>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1545"/>
        <source>SmartFlex Mode</source>
        <translation>SmartFlex Mode</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1544"/>
        <source>Intellipap pressure relief mode.</source>
        <translation>Intellipap drukverligtingsmode.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1550"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3025"/>
        <source>Ramp Only</source>
        <translation>Slegs Helling</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1551"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3026"/>
        <source>Full Time</source>
        <translation>Voltyds</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1554"/>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1556"/>
        <source>SmartFlex Level</source>
        <translation>SmartFlex Vlak</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1555"/>
        <source>Intellipap pressure relief level.</source>
        <translation>Intellipap drukverligting vlak.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="657"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="1676"/>
        <source>VPAP Adapt</source>
        <translation>VPAP Aanpas</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="1644"/>
        <source>Parsing Identification File</source>
        <translation>Ontleed identifikasie lêer</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="1743"/>
        <source>Locating STR.edf File(s)...</source>
        <translation>Soek na STR.edf Lêer(s)...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="1918"/>
        <source>Cataloguing EDF Files...</source>
        <translation>Katalogisering van EDF-lêers ...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="1929"/>
        <source>Queueing Import Tasks...</source>
        <translation>Invoeropdragte in afwagting ...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="1989"/>
        <source>Finishing Up...</source>
        <translation>Voltooiing ...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3000"/>
        <source>CPAP Mode</source>
        <translation>CPAP Mode</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3006"/>
        <source>VPAP-T</source>
        <translation>VPAP-T</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3007"/>
        <source>VPAP-S</source>
        <translation>VPAP-S</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3008"/>
        <source>VPAP-S/T</source>
        <translation>VPAP-S/T</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3009"/>
        <source>??</source>
        <translation>??</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3010"/>
        <source>VPAPauto</source>
        <translation>VPAPauto</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3012"/>
        <source>ASVAuto</source>
        <translation>ASVAuto</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3013"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3014"/>
        <source>???</source>
        <translation>???</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3015"/>
        <source>Auto for Her</source>
        <translation>Auto for Her</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3018"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3020"/>
        <source>EPR</source>
        <translation>EPR</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3019"/>
        <source>ResMed Exhale Pressure Relief</source>
        <translation>ResMed Uitasem Druk Verligting</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3027"/>
        <source>Patient???</source>
        <translation>Pasiënt???</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3030"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3032"/>
        <source>EPR Level</source>
        <translation>EPR Vlak</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3031"/>
        <source>Exhale Pressure Relief Level</source>
        <translation>Uitasem Druk Verligting Vlak</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3035"/>
        <source>0cmH2O</source>
        <translation>0cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3036"/>
        <source>1cmH2O</source>
        <translation>1cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3037"/>
        <source>2cmH2O</source>
        <translation>2cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3038"/>
        <source>3cmH2O</source>
        <translation>3cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3045"/>
        <source>SmartStart</source>
        <translation>SmartStart</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3046"/>
        <source>Machine auto starts by breathing</source>
        <translation>Masien begin outomaties deur asemhaling</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3047"/>
        <source>Smart Start</source>
        <translation>Smart Start</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3054"/>
        <source>Humid. Status</source>
        <translation>Humid. Status</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3055"/>
        <source>Humidifier Enabled Status</source>
        <translation>Humidifier Enabled Status</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3063"/>
        <source>Humid. Level</source>
        <translation>Humid. Level</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3064"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3065"/>
        <source>Humidity Level</source>
        <translation>Bevogtiger Vlak</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3079"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3081"/>
        <source>Temperature</source>
        <translation>Temperatuur</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3080"/>
        <source>ClimateLine Temperature</source>
        <translation>ClimateLine Temperatuur</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3086"/>
        <source>Temp. Enable</source>
        <translation>Temp. Enable</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3087"/>
        <source>ClimateLine Temperature Enable</source>
        <translation>ClimateLine Temperature Enable</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3088"/>
        <source>Temperature Enable</source>
        <translation>Temperature Enable</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3097"/>
        <source>AB Filter</source>
        <translation>AB Filter</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3098"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3099"/>
        <source>Antibacterial Filter</source>
        <translation>Antibakteriese Filter</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3106"/>
        <source>Pt. Access</source>
        <translation>Pt. Access</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3107"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3108"/>
        <source>Patient Access</source>
        <translation>Pasiënt Toegang</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3115"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3116"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3117"/>
        <source>Climate Control</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3120"/>
        <source>Manual</source>
        <translatorcomment>Of handleiding???</translatorcomment>
        <translation>Handrolies</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3121"/>
        <source>Auto</source>
        <translation>Outo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3124"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3126"/>
        <source>Mask</source>
        <translation>Masker</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3125"/>
        <source>ResMed Mask Setting</source>
        <translation>ResMed Masker Stelling</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3129"/>
        <source>Pillows</source>
        <translation>Kussings</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3130"/>
        <source>Full Face</source>
        <translation>Volgesig</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3131"/>
        <source>Nasal</source>
        <translation>Neus</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3135"/>
        <source>Ramp Enable</source>
        <translation>Helling Aktiveer</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/weinmann_loader.h" line="116"/>
        <source>Weinmann</source>
        <translation>Weinmann</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/weinmann_loader.h" line="116"/>
        <source>SOMNOsoft2</source>
        <translation>SOMNOsoft2</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="351"/>
        <source>Snapshot %1</source>
        <translation>Snapshot %1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="266"/>
        <source>CMS50D+</source>
        <translation>CMS50D+</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="266"/>
        <source>CMS50E/F</source>
        <translation>CMS50E/F</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="612"/>
        <source>Loading %1 data for %2...</source>
        <translation>Laai %1 data vir %2...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="622"/>
        <source>Scanning Files</source>
        <translation>Lêers skandeer</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="658"/>
        <source>Migrating Summary File Location</source>
        <translation>Migreer Opsomming Lêer Plek</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="961"/>
        <source>Loading Summaries.xml.gz</source>
        <translation>Laai Summaries.xml.gz</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="1088"/>
        <source>Loading Summary Data</source>
        <translation>Laai Opsomming Data</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/progressdialog.cpp" line="14"/>
        <source>Please Wait...</source>
        <translation>Wag Asseblief...</translation>
    </message>
    <message>
        <location filename="../oscar/updateparser.cpp" line="228"/>
        <source>%1
Line %2, column %3</source>
        <translation>%1
Lyn %2, kolom %3</translation>
    </message>
    <message>
        <location filename="../oscar/updateparser.cpp" line="241"/>
        <source>Could not parse Updates.xml file.</source>
        <translation>Kon nie Updates.xml lêer ontleed nie.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/MinutesAtPressure.cpp" line="312"/>
        <source>Peak %1</source>
        <translation>Piek %1</translation>
    </message>
</context>
<context>
    <name>Report</name>
    <message>
        <location filename="../oscar/reports.ui" line="14"/>
        <source>Form</source>
        <translation>Vorm</translation>
    </message>
    <message>
        <location filename="../oscar/reports.ui" line="27"/>
        <source>about:blank</source>
        <translatorcomment>***???</translatorcomment>
        <translation>about:blank</translation>
    </message>
</context>
<context>
    <name>SessionBar</name>
    <message>
        <location filename="../oscar/sessionbar.cpp" line="246"/>
        <source>%1h %2m</source>
        <translation>%1h %2m</translation>
    </message>
    <message>
        <location filename="../oscar/sessionbar.cpp" line="289"/>
        <source>No Sessions Present</source>
        <translation>Geen Sessies Teenwoordig nie</translation>
    </message>
</context>
<context>
    <name>Statistics</name>
    <message>
        <location filename="../oscar/statistics.cpp" line="502"/>
        <source>CPAP Statistics</source>
        <translation>CPAP Statistieke</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="505"/>
        <location filename="../oscar/statistics.cpp" line="1216"/>
        <source>CPAP Usage</source>
        <translation>CPAP Gebruik</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="506"/>
        <source>Average Hours per Night</source>
        <translation>Gemiddelde Ure per Nag</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="507"/>
        <source>Compliance</source>
        <translation>Voldoening</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="509"/>
        <source>Therapy Efficacy</source>
        <translation>Terapie Doeltreffendheid</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="519"/>
        <source>Leak Statistics</source>
        <translation>Lek Statistieke</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="524"/>
        <source>Pressure Statistics</source>
        <translation>Druk Statistieke</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="537"/>
        <source>Oximeter Statistics</source>
        <translation>Oximeter Statistieke</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="541"/>
        <source>Blood Oxygen Saturation</source>
        <translation>Bloed Suurstofversadiging</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="546"/>
        <source>Pulse Rate</source>
        <translation>Polstempo</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="557"/>
        <source>%1 Median</source>
        <translation>%1 Mediaan</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="558"/>
        <location filename="../oscar/statistics.cpp" line="559"/>
        <source>Average %1</source>
        <translation>Gemiddelde %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="561"/>
        <source>Min %1</source>
        <translation>Min %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="562"/>
        <source>Max %1</source>
        <translation>Maks %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="563"/>
        <source>%1 Index</source>
        <translation>%1 Indeks</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="564"/>
        <source>% of time in %1</source>
        <translation>% tyd in %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="565"/>
        <source>% of time above %1 threshold</source>
        <translation>% tyd bo die %1 drempelwaarde</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="566"/>
        <source>% of time below %1 threshold</source>
        <translation>% tyd onder die %1 drempelwaarde</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="587"/>
        <source>Name: %1, %2</source>
        <translation>Naam: %1, %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="589"/>
        <source>DOB: %1</source>
        <translation>Geb: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="592"/>
        <source>Phone: %1</source>
        <translation>Tel: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="595"/>
        <source>Email: %1</source>
        <translation>Epos: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="598"/>
        <source>Address:</source>
        <translation>Adres:</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="641"/>
        <source>Usage Statistics</source>
        <translation>Gebruik Statistiek</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="657"/>
        <source>This report was generated by OSCAR v%1</source>
        <translation>Hierdie verslag is gegenereer deur OSCAR v%1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="658"/>
        <source>OSCAR is free open-source CPAP report software</source>
        <translation>OSCAR is gratis oop-bronkode CPAP verslaggewing sagteware</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="993"/>
        <source>I can haz data?!?</source>
        <translation>Ek data kry kan?!?</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="995"/>
        <source>Oscar has no data to report :(</source>
        <translation>OSCAR het geen data om te rapporteer nie :(</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1217"/>
        <source>Days Used: %1</source>
        <translation>Dae Gebruik: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1218"/>
        <source>Low Use Days: %1</source>
        <translation>Lae Gebruik Dae: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1219"/>
        <source>Compliance: %1%</source>
        <translation>Voldoening: %1%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1243"/>
        <source>Days AHI of 5 or greater: %1</source>
        <translation>Dae AHI van 5 of hoër: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1250"/>
        <source>Best AHI</source>
        <translation>Beste AHI</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1254"/>
        <location filename="../oscar/statistics.cpp" line="1266"/>
        <source>Date: %1 AHI: %2</source>
        <translation>Datum: %1 AHI: %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1260"/>
        <source>Worst AHI</source>
        <translation>Slegste AHI</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1297"/>
        <source>Best Flow Limitation</source>
        <translation>Beste Vloeibeperking</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1301"/>
        <location filename="../oscar/statistics.cpp" line="1314"/>
        <source>Date: %1 FL: %2</source>
        <translation>Datum: %1 FL: %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1307"/>
        <source>Worst Flow Limtation</source>
        <translation>Slegste Vloeibeperking</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1319"/>
        <source>No Flow Limitation on record</source>
        <translation>Geen Vloeibeperking op rekord</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1340"/>
        <source>Worst Large Leaks</source>
        <translation>Slegste Groot Lekke</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1348"/>
        <source>Date: %1 Leak: %2%</source>
        <translation>Datum: %1 Lek: %2%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1354"/>
        <source>No Large Leaks on record</source>
        <translation>Geen Groot Lekke op rekord</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1377"/>
        <source>Worst CSR</source>
        <translation>Slegste CSR</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1385"/>
        <source>Date: %1 CSR: %2%</source>
        <translation>Datum: %1 CSR: %2%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1390"/>
        <source>No CSR on record</source>
        <translation>Geen CSR op rekord</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1407"/>
        <source>Worst PB</source>
        <translation>Slegste PB</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1415"/>
        <source>Date: %1 PB: %2%</source>
        <translation>Datum: %1 PB: %2%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1420"/>
        <source>No PB on record</source>
        <translation>Geen PB op rekord</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1428"/>
        <source>Want more information?</source>
        <translation>Benodig u meer inligting?</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1429"/>
        <source>OSCAR needs all summary data loaded to calculate best/worst data for individual days.</source>
        <translation>OSCAR benodig alle opsommende data ingelaai om die beste/slegste data vir individuele dae te kan bereken.</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1430"/>
        <source>Please enable Pre-Load Summaries checkbox in preferences to make sure this data is available.</source>
        <translation>Aktiveer asseblief die vooraf laai van opsommings in die kieslys om te verseker dat hierdie data beskikbaar is.</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1450"/>
        <source>Best RX Setting</source>
        <translation>Beste RX Stelling</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1453"/>
        <location filename="../oscar/statistics.cpp" line="1465"/>
        <source>Date: %1 - %2</source>
        <translation>Datum: %1 - %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1456"/>
        <location filename="../oscar/statistics.cpp" line="1468"/>
        <source>Culminative AHI: %1</source>
        <translatorcomment>?????</translatorcomment>
        <translation>Kumulatiewe AHI:%1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1457"/>
        <location filename="../oscar/statistics.cpp" line="1469"/>
        <source>Culminative Hours: %1</source>
        <translatorcomment>????</translatorcomment>
        <translation>Kumulatiewe Ure: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1462"/>
        <source>Worst RX Setting</source>
        <translation>Slegste RX Stelling</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1051"/>
        <source>Most Recent</source>
        <translation>Mees Onlangse</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1052"/>
        <source>Last Week</source>
        <translation>Laaste Week</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1053"/>
        <source>Last 30 Days</source>
        <translation>Laaste 30 Dae</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1054"/>
        <source>Last 6 Months</source>
        <translation>Laaste 6 Maande</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1055"/>
        <source>Last Year</source>
        <translation>Laasdte Jaar</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1059"/>
        <source>Last Session</source>
        <translation>Laaste Sessie</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1111"/>
        <source>Details</source>
        <translation>Details</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1125"/>
        <source>No %1 data available.</source>
        <translation>Geen %1 data beskikbaar.</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1128"/>
        <source>%1 day of %2 Data on %3</source>
        <translation>%1 dag van %2 Data op %3</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1134"/>
        <source>%1 days of %2 Data, between %3 and %4</source>
        <translation>%1 dae van %2 Data, tussen %3 en %4</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="874"/>
        <source>Changes to Prescription Settings</source>
        <translation>Veranderinge van Voorgeskrewe Instellings</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="882"/>
        <source>Days</source>
        <translation>Dae</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="886"/>
        <source>Pressure Relief</source>
        <translation>Druk Verligting</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="888"/>
        <source>Pressure Settings</source>
        <translation>Druk Instellings</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="818"/>
        <source>Machine Information</source>
        <translation>Masjien Inligting</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="825"/>
        <source>First Use</source>
        <translation>Eerste Gebruik</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="826"/>
        <source>Last Use</source>
        <translation>Laaste Gebruik</translation>
    </message>
</context>
<context>
    <name>UpdaterWindow</name>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="14"/>
        <source>OSCAR Updater</source>
        <translation>OSCAR Opdateerder</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="60"/>
        <source>A new version of $APP is available</source>
        <translation>&apos;n Nuwe weergawe van $APP is beskikbaar</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="106"/>
        <source>Version Information</source>
        <translation>Weergawe Inligting</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="117"/>
        <source>Release Notes</source>
        <translation>Vrystelling Notas</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="142"/>
        <source>Build Notes</source>
        <translation>Bou Notas</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="195"/>
        <source>Maybe &amp;Later</source>
        <translation>Miskien &amp;Later</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="215"/>
        <source>&amp;Upgrade Now</source>
        <translation>&amp;Upgradeer Nou</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="250"/>
        <source>Please wait while updates are downloaded and installed...</source>
        <translation>Wag asseblief terwyl opdaterings afgelaai en geïnstalleer is...</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="264"/>
        <source>Updates</source>
        <translation>Opdaterings</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="304"/>
        <source>Component</source>
        <translation>Komponent</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="309"/>
        <source>Version</source>
        <translation>Weergawe</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="314"/>
        <source>Size</source>
        <translation>Grootte</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="319"/>
        <source>Progress</source>
        <translation>Vordering</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="328"/>
        <source>Log</source>
        <translatorcomment>?????</translatorcomment>
        <translation>Log</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="374"/>
        <source>Downloading &amp; Installing Updates</source>
        <translation>Laai &amp; Installeer Opdaterings</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="394"/>
        <source>&amp;Finished</source>
        <translation>&amp;Finished</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="93"/>
        <source>Updates are not yet implemented</source>
        <translation>Opdaterings is nog nie voltooi nie</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="127"/>
        <source>Checking for OSCAR Updates</source>
        <translation>Kontroleer vir OSCAR opdaterings</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="146"/>
        <source>Requesting </source>
        <translation>Versoek </translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="158"/>
        <source>OSCAR Updates are currently unvailable for this platform</source>
        <translation>OSCAR Opdaterings is tans nie beskikbaar vier hierdie platvorm nie</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="158"/>
        <location filename="../oscar/UpdaterWindow.cpp" line="504"/>
        <location filename="../oscar/UpdaterWindow.cpp" line="510"/>
        <source>OSCAR Updates</source>
        <translation>OSCAR Opdaterings</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="459"/>
        <source>Version %1 of OSCAR is available, opening link to download site.</source>
        <translation>Weergawe %1 van OSCAR is beskikbaar, skakel na webwerf maak oop.</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="511"/>
        <source>New OSCAR Updates are avilable:</source>
        <translation>Nuwe OSCAR Opdaterings is beskikbaar:</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="462"/>
        <source>You are already running the latest version.</source>
        <translation>U het reeds die nuutste weergawe.</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="512"/>
        <source>Would you like to download and install them now?</source>
        <translation>Wil u dit nou aflaai en installeer?</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="504"/>
        <source>No updates were found for your platform.</source>
        <translation>Geen opdaterings is gevind vir u platvorm nie.</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="171"/>
        <source>%1 bytes received</source>
        <translation>%1 grepe ontvang</translation>
    </message>
</context>
<context>
    <name>Welcome</name>
    <message>
        <location filename="../oscar/welcome.ui" line="14"/>
        <source>Form</source>
        <translation>Vorm</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="128"/>
        <source>Welcome to the Open Source CPAP Analysis Reporter</source>
        <translation>Welkom by die Open Source CPAP Analysis Reporter</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="143"/>
        <source>What would you like to do?</source>
        <translation>Wat wil u doen?</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="186"/>
        <source>CPAP Importer</source>
        <translation>CPAP Invoerder</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="224"/>
        <source>Oximetry Wizard</source>
        <translation>Oximetrie Helper</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="262"/>
        <source>Daily View</source>
        <translation>Daaglikse Vertoon</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="300"/>
        <source>Overview</source>
        <translation>Oorsig</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="338"/>
        <source>Statistics</source>
        <translation>Statistieke</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="581"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Warning: &lt;/span&gt;&lt;span style=&quot; font-size:10pt; color:#ff0000;&quot;&gt;ResMed S9 SDCards &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; color:#ff0000;&quot;&gt;need &lt;/span&gt;&lt;span style=&quot; font-size:10pt; color:#ff0000;&quot;&gt;to be locked &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; color:#ff0000;&quot;&gt;before &lt;/span&gt;&lt;span style=&quot; font-size:10pt; color:#ff0000;&quot;&gt;inserting into your computer&lt;br/&gt;&lt;/span&gt;&lt;span style=&quot; font-size:10pt; color:#000000;&quot;&gt;Some operating systems write cache files which break their special filesystem Journal&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Waarskuwing: &lt;/span&gt;&lt;span style=&quot; font-size:10pt; color:#ff0000;&quot;&gt;ResMed S9 SDKaarte &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; color:#ff0000;&quot;&gt;benodig &lt;/span&gt;&lt;span style=&quot; font-size:10pt; color:#ff0000;&quot;&gt;om gesluit te wees &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; color:#ff0000;&quot;&gt;alvorens &lt;/span&gt;&lt;span style=&quot; font-size:10pt; color:#ff0000;&quot;&gt;dit in die rekenaar ingesit word&lt;br/&gt;&lt;/span&gt;&lt;span style=&quot; font-size:10pt; color:#000000;&quot;&gt;Sommige bedryfstelsels skryf lêers wat die spesiale Joernaal onbruikbaar maak&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="139"/>
        <source>It would be a good idea to check File-&gt;Preferences first,</source>
        <translation>Dit is &apos;n goeie idee om eers na File-&gt;Preferences te kyk,</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="140"/>
        <source>as there are some options that affect import.</source>
        <translation>want daar is keuses wat invoering affekteer.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="141"/>
        <source>Note that some preferences are forced when a ResMed machine is detected</source>
        <translation>Let daarop dat sommige voorkeure afgedwing word wanneer &apos;n ResMed-masjien bespeur word</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="142"/>
        <source>First import can take a few minutes.</source>
        <translation>Die eerste invoer kan &apos;n paar minute neem.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="155"/>
        <source>The last time you used your %1...</source>
        <translation>Die laaste keer wat u u %1 gebruik het...</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="159"/>
        <source>last night</source>
        <translation>laasnag</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="160"/>
        <source>yesterday</source>
        <translation>gister</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="161"/>
        <source>%2 days ago</source>
        <translation>%2 dae gelede</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="163"/>
        <source>was %1 (on %2)</source>
        <translation>was %1 (op %2)</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="171"/>
        <source>%1 hours, %2 minutes and %3 seconds</source>
        <translation>%1 ure, %2 minute en %3 sekondes</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="174"/>
        <source>Your machine was on for %1.</source>
        <translation>U masjien was aan vir %1.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="175"/>
        <source>&lt;font color = red&gt;You only had the mask on for %1.&lt;/font&gt;</source>
        <translation>&lt;font color = red&gt;U het slegs die masker opgehad vir %1.&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="186"/>
        <source>under</source>
        <translation>onder</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="187"/>
        <source>over</source>
        <translation>bo</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="188"/>
        <source>reasonably close to</source>
        <translation>redelik naby aan</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="189"/>
        <source>equal to</source>
        <translation>gelyk aan</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="203"/>
        <source>You had an AHI of %1, which is %2 your %3 day average of %4.</source>
        <translation>U het &apos;n AHI gehad van %1, wat %2 u %3 dag gemiddeld van %4 is.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="212"/>
        <source>Your CPAP machine used a constant %1 %2 of air</source>
        <translation>U CPAP masjien het &apos;n konstante %1 %2 lug gebruik</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="215"/>
        <source>Your pressure was under %1 %2 for %3% of the time.</source>
        <translation>U druk was onder %1 %2 vir %3% van die tyd.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="219"/>
        <source>Your machine used a constant %1-%2 %3 of air.</source>
        <translation>U masjien het &apos;n konstante %1-%2 %3 lug gebruik.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="228"/>
        <source>Your EPAP pressure fixed at %1 %2.</source>
        <translation>U EPAP druk was vas op %1 %2.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="229"/>
        <location filename="../oscar/welcome.cpp" line="235"/>
        <source>Your IPAP pressure was under %1 %2 for %3% of the time.</source>
        <translation>U IPAP druk was onder %1 %2 vir %3% van die tyd.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="234"/>
        <source>Your EPAP pressure was under %1 %2 for %3% of the time.</source>
        <translation>U EPAP druk was onder %1 %2 vir %3% van die tyd.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="223"/>
        <source>Your machine was under %1-%2 %3 for %4% of the time.</source>
        <translation>U masjien was onder %1-%2 %3 vir %4% van die tyd.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="255"/>
        <source>Your average leaks were %1 %2, which is %3 your %4 day average of %5.</source>
        <translation>U gemiddelde lekke was %1 %2, wat %3 u %4 dag gemiddeld van %5 is.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="261"/>
        <source>No CPAP data has been imported yet.</source>
        <translation>Geen CPAP data is nog ingevoer nie.</translation>
    </message>
</context>
<context>
    <name>gGraph</name>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="793"/>
        <source>%1 days</source>
        <translation>%1 dae</translation>
    </message>
</context>
<context>
    <name>gGraphView</name>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="392"/>
        <source>100% zoom level</source>
        <translation>100% zoom vlak</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="393"/>
        <source>Restore X-axis zoom too 100% to view entire days data.</source>
        <translation>Herstel X-as zoom na 100% om volle dag se data te sien.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="395"/>
        <source>Reset Graph Layout</source>
        <translation>Herstel Grafiek Uitleg</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="396"/>
        <source>Resets all graphs to a uniform height and default order.</source>
        <translation>Herstel alle grafieke na univorme hoogte en verstek volgorde.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="399"/>
        <source>Y-Axis</source>
        <translation>Y-As</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="400"/>
        <source>Plots</source>
        <translation>Plots</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="405"/>
        <source>CPAP Overlays</source>
        <translation>CPAP Oorlê</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="408"/>
        <source>Oximeter Overlays</source>
        <translation>Oximeter Oorlê</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="411"/>
        <source>Dotted Lines</source>
        <translation>Stippellyne</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1746"/>
        <source>Double click title to pin / unpin
Click and drag to reorder graphs</source>
        <translation>Dubbelkliek die titel om te pin / ontpin
Kliek en sleep om grafieke te herrangskik</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2044"/>
        <source>Remove Clone</source>
        <translation>Verwyder Kloon</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2048"/>
        <source>Clone %1 Graph</source>
        <translation>Kloon %1 Grafiek</translation>
    </message>
</context>
</TS>
